/**
	@file cuda_errorcheck.cuh
	@brief Implements functions to check for errors with kernel invocation.
	Avoids the use of cutil.

	Put @verbatim -DCUDA_ERROR_CHECK @endverbatim in the @a LDFLAGS of the Makefile to turn on error checking
*/
#pragma once
#ifndef CUDA_ERRORCHECK_CUH
/** @cond */
#define CUDA_ERRORCHECK_CUH
/** @endcond */

#include <cstdlib> //for EXIT_FAILURE
#include <cstdio>

/** 
	@brief nice wrapper for ::__myCudaSafeCall
	@param[in] err the error that is thrown
*/
#define myCudaSafeCall( err ) __myCudaSafeCall( err, __FILE__, __LINE__ )

/** 
	@brief nice wrapper for ::__myCudaCheckError
*/
#define myCudaCheckError()    __myCudaCheckError( __FILE__, __LINE__ )

/**
	My version of cudaSafeCall.
*/
inline void __myCudaSafeCall( cudaError err, const char *file, const int line ) {
	#ifdef CUDA_ERROR_CHECK
	if ( cudaSuccess != err ) {
		fprintf( stderr, "myCudaSafeCall() failed at %s:%i : %s\n", file, line, cudaGetErrorString( err ) );
		exit(EXIT_FAILURE);
	}
	#endif
	return;
}

/**
	My version of cudaCheckError
*/
inline void __myCudaCheckError( const char *file, const int line ) {
	#ifdef CUDA_ERROR_CHECK
	cudaError err = cudaGetLastError();
	if ( cudaSuccess != err ) {
		fprintf( stderr, "myCudaCheckError() failed at %s:%i : %s\n",
		file, line, cudaGetErrorString( err ) );
		exit(EXIT_FAILURE);
	}

	// More careful checking. However, this will affect performance.
	// Comment away if needed.
	err = cudaDeviceSynchronize();
	if( cudaSuccess != err ) {
		fprintf( stderr, "cudaCheckError() with sync failed at %s:%i : %s\n", file, line, cudaGetErrorString( err ) );
		exit(EXIT_FAILURE);
	}
	#endif
	return;
}

#endif
