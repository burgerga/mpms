#include "mt19937ar.h"
#include <cmath>

double gauss(double sigma){
	static bool first = true;
	static double u, v, s;
	if(first){
		do {
			u = 2.0 * genrand() - 1.0;
			v = 2.0 * genrand() - 1.0;
			s = u * u + v * v;
		} while (s >= 1.0 || s == 0);
		s = sqrt((-2.0 * log(s))/s);
		first = false;
		return u*s*sigma;
	} else {
		first = true;
		return v*s*sigma;
	}
}
		
