#include <boost/program_options.hpp>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;
namespace po = boost::program_options;

#include "boost_po.hpp"

int get_opts(int argc, char **argv, Parameters& p){
	try {

		po::variables_map opts;
		// allowed only on command line
		po::options_description cli("Command line options");
		cli.add_options()
    			("help", "produce help message")
   	 		("paramfile", po::value<string>()->default_value("params.cfg"), "name of parameter file")
			("resume,r", po::value<string>()->implicit_value("./resume.txt"),"resume from a previous run")
   	 		("config,c", po::value<string>( &(p.configfile) ), "name of config file with starting configuration")
			("device", po::value<int>( &(p.device) )->default_value(0), "device to use in case of multiple devices")
    			;

		// allowed both on command line and in config file
		po::options_description config_options("Configuration (both command line and file)");
		config_options.add_options()
			("frank",po::value<bool>( &(p.output_frank) )->implicit_value(true),"output files suitable for franks opengl")
			("outdir,o", po::value<string>( &(p.outdir) )->default_value("out"),"directory for output files")
    			("n", po::value<int>( &(p.n) )->required(), "number of particles")		
			//("delta",po::value<double>( &(p.delta) )->required(), "maximum change in position")
			//("deltaV",po::value<double>( &(p.deltaV) )->required(), "maximum change in volume")
			//("deltaR",po::value<double>( &(p.deltaR) )->required(), "maximum change in rotation")
			//("deltaPe",po::value<double>( &(p.deltaPe) )->required(), "maximum perturbation of box")
			("iter,i",po::value<int>( &(p.iter) )->required(), "number of measuring iterations")
			("nreq,q",po::value<int>( &(p.nreq) )->required(), "number of equilibration iterations")
			("density,d",po::value<double>( &(p.density) )->required(), "starting density")
    			//("pressure,p", po::value<double>( &(p.pressure) )->required(), "pressure")
    			("temp,t", po::value<double>( &(p.temp) )->required(), "temperature")
    			("collision", po::value<double>( &(p.nu) )->required(), "collisionrate")
    			//("melt_start_p", po::value<double>( &(p.melt_start_p) )->required(), "starting pressure for melting")
    			//("melt_stop_p", po::value<double>( &(p.melt_stop_p) )->required(), "stopping pressure for melting")
    			//("melt_dp", po::value<double>( &(p.melt_dp) )->required(), "pressurestep for melting")
			//("sides",po::value<int>( &(p.sides) )->required(), "number of sides for regular convex polygon"
    			("dt", po::value<double>( &(p.dt) )->required(), "timestep")
			;
		
		
		po::options_description cmdline_options;
		cmdline_options.add(cli).add(config_options);

		store(po::parse_command_line(argc, argv,cmdline_options),opts);
		//check for help
		if(opts.count("help")) {
			cout << cmdline_options << endl;
			exit(EXIT_SUCCESS);
		}
		if(opts.count("resume")) {
			p.resumefile = opts["resume"].as<string>();
			return 2;
		}
		ifstream ifs( opts["paramfile"].as<string>().c_str() );
		if (!ifs) {
    			cerr << "can not open parameter file: " << opts["paramfile"].as<string>() << endl;
			exit(EXIT_FAILURE);
		}
		else {
    			store(parse_config_file(ifs, config_options), opts);
			notify(opts);
		}
		
		if(opts.count("frank")) p.output_frank = opts["frank"].as<bool>() ;

		if(opts.count("config"))
			return 1;
	}
	catch(exception& e) {
    		cerr << e.what() << endl;
    		exit(EXIT_FAILURE);
	}    
	return 0;
}
	
