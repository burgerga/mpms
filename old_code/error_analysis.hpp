#ifndef ERROR_ANALYSIS_H
#define ERROR_ANALYSIS_H

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <vector>

using namespace boost::accumulators;

class ErrorAnalysis {
private:
	bool done;
	int nrSamples;
	int nrBins;
	int samplesPerBin;
	int count;
	std::vector<double> bins;
	accumulator_set< double, features< tag::mean, tag::variance > > acc;
	void process();
public:
	ErrorAnalysis(int _nrSamples, int _nrBins = 10);
	void push(double value);
	double mean();
	double variance();
	double error();
};
	
#endif
