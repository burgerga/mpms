#ifndef MD_HPP
#define MD_HPP

class Variables {
   public:
	int i; 		//iteration
	bool cont;
	float t; 	//time
	float avgpressure;	//Average volume
	float en;	//energy
	float virial;
	float ke;
	float box[3];
	float corr;
	float etot;
	int nrfiles;
	Variables();
};

#endif
