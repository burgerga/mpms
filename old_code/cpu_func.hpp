#ifndef CPU_FUNC_HPP
#define CPU_FUNC_HPP

#include "boost_po.hpp"
#include "md_cuda.cuh"
#include <cmath>

template <typename T> void integrate1(T *h_x, T *h_v, T *h_a, Parameters par, Variables var, int offset = 4);
template <typename T> void integrate2(T *h_x, T *h_v, Parameters par, Variables var, int offset = 4);
template <typename T> void calcAcceleration(T *h_x, T *h_a, Parameters par, Variables *var, int offset = 4);
template <typename T> double calcKinEnergy( T *h_v, Parameters par, int offset = 4);
template <typename T> void scaleMomentum( T *h_v, T kinEnergy, Parameters par, int offset = 4);

template <typename T>
void integrate1(T *h_x, T *h_v, T *h_a, Parameters par, Variables var, int offset ){
	static double dt2;
	static bool set = false;
	if(!set){
		dt2 = par.dt*par.dt;
		set = true;
	}
	for( int i = 0; i < par.n; ++i ){
		int ii = offset*i;
		//calculate new positions
		h_x[ii]   += h_v[ii]  *par.dt+0.5*h_a[ii]  *dt2;
		h_x[ii+1] += h_v[ii+1]*par.dt+0.5*h_a[ii+1]*dt2;
		h_x[ii+2] += h_v[ii+2]*par.dt+0.5*h_a[ii+2]*dt2;
		//periodic boundary conditions
		if      (h_x[ii]   >= var.box[0]) h_x[ii]   -= var.box[0];
		else if (h_x[ii]   <  0         ) h_x[ii]   += var.box[0];
		if      (h_x[ii+1] >= var.box[1]) h_x[ii+1] -= var.box[1];
		else if (h_x[ii+1] <  0         ) h_x[ii+1] += var.box[1];
		if      (h_x[ii+2] >= var.box[2]) h_x[ii+2] -= var.box[2];
		else if (h_x[ii+2] <  0         ) h_x[ii+2] += var.box[2];
		//calculate velocities
		h_v[ii]   += 0.5*h_a[ii]*par.dt; 
		h_v[ii+1] += 0.5*h_a[ii+1]*par.dt; 
		h_v[ii+2] += 0.5*h_a[ii+2]*par.dt; 
	}
}

template <typename T>
void integrate2(T *h_v, T *h_a, Parameters par, Variables var, int offset ){
	for(int i = 0; i < par.n; ++i){
		int ii = offset*i;
		h_v[ii]   += 0.5*h_a[ii]*par.dt; 
		h_v[ii+1] += 0.5*h_a[ii+1]*par.dt; 
		h_v[ii+2] += 0.5*h_a[ii+2]*par.dt; 
		
	}

}

template <typename T> 
void calcAcceleration(T *h_x, T *h_a, Parameters par, Variables *var, int offset ){
	var->en = 0.0;
	var->virial = 0.0;
	//TODO cutoff energy once
	static bool set = false;
	static double hl;
	if(!set){
		hl = var->box[0]/2;
		set = true;
	}

	memset(h_a,0, offset*par.n*sizeof(T));

	for(int i = 0; i < par.n - 1; ++i){
		for(int j = i+1; j < par.n; ++j){
			int ii = offset*i;
			int jj = offset*j;
			double xr[3];
			xr[0] = h_x[ii]  -h_x[jj];	
			xr[1] = h_x[ii+1]-h_x[jj+1];	
			xr[2] = h_x[ii+2]-h_x[jj+2];	
			//nearest image
			if     (xr[0] < -hl) xr[0] += var->box[0];
			else if(xr[0] >  hl) xr[0] -= var->box[0];
			if     (xr[1] < -hl) xr[1] += var->box[1];
			else if(xr[1] >  hl) xr[1] -= var->box[1];
			if     (xr[2] < -hl) xr[2] += var->box[2];
			else if(xr[2] >  hl) xr[2] -= var->box[2];
			//squared distance
			double r2 = xr[0]*xr[0] + xr[1]*xr[1] + xr[2]*xr[2];
			double r2i=1/r2;
			double r6i=r2i*r2i*r2i;
			//lennard jones potential
			var->en += 4*r6i*(r6i-1);	
			double ff=48*r6i*(r6i-0.5);
			var->virial += ff;
			//update forces
			h_a[ii]   += ff*r2i*xr[0]; 
			h_a[ii+1] += ff*r2i*xr[1]; 
			h_a[ii+2] += ff*r2i*xr[2]; 
			h_a[jj]   -= ff*r2i*xr[0]; 
			h_a[jj+1] -= ff*r2i*xr[1]; 
			h_a[jj+2] -= ff*r2i*xr[2]; 
		}
	}
}

template <typename T>
double calcKinEnergy( T *h_v, Parameters par, int offset ){
	double sum = 0.0;
	for( int i = 0; i < par.n; ++i){
		int ii = offset*i;
		sum += h_v[ii]*h_v[ii] + h_v[ii+1]*h_v[ii+1] + h_v[ii+2] * h_v[ii+2];
	}
	return 0.5*sum;
}

template <typename T>
void scaleMomentum(T *h_v, T kinEnergy, Parameters par, int offset ) {
	double instatemp = kinEnergy*2/(3*par.n);
	double scalefactor = sqrt(par.temp/instatemp);
	for(int i = 0; i < offset*par.n; ++i) {
	 	if(i%(offset-1) != 0) h_v[i] *= scalefactor;
	}
	return;
}
#endif
