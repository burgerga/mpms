/**
	@file particles_kernel_impl.cuh
	@brief Cuda function and functor definitions
*/

#pragma once
#ifndef _PARTICLES_KERNEL_H_
#define _PARTICLES_KERNEL_H_

#include <stdio.h>
#include <math.h>
#include "particles_kernel.cuh"
#include "assert.h"

#include "../cuda_helper/cutil_math.h"

/// simulation parameters in constant memory
__constant__ SimParams params;


/**
 *	@brief functor for the first step of the Verlet integrator.
 * 	Executes @f$ x = x+ v \Delta t + 0.5 a {\Delta t}^2 @f$, and @f$ v = v 0.5 a \Delta t @f$.

 *	Functors are function objects to be used in (thrust) algorithms.
 * 	Although they have many purposes, here they are used because you can pass them as arguments to other functions.
 *	@see <a href="https://code.google.com/p/thrust/wiki/QuickStartGuide#Algorithms">Thrust QuickStartGuide: Algorithms</a>
 */
struct integrate1_functor{
	float _dt; ///< timestep
	float3 _boxSize; ///< size of the simulation box
	///Constructor
	__host__ __device__ integrate1_functor(float dt, float3 boxSize) : _dt(dt), _boxSize(boxSize) {}
	/**
	 *	@brief overloaded operator(), actual Verlet integration.
	 *	@param[in, out] t templated parameter, in this case a @c thrust::tuple is passed.
		@note See ::integrateSystem1 to see how these tuples are made.
	 *	@see <a href="http://docs.thrust.googlecode.com/hg/classthrust_1_1tuple.html">thrust::tuple</a>
	 *	@todo check if the volatile is really needed
	 */
	template <typename tuple> 
	__host__ __device__ void operator()(tuple t){
		//get position from tuple
		volatile float4 x4 = thrust::get<0>(t);
		//get velocity from tuple
		volatile float4 v4 = thrust::get<1>(t);
		//get acceleration from tuple
		volatile float4 a4 = thrust::get<2>(t);
		//convert float4 to float3
		float3 x3 = make_float3(x4.x, x4.y, x4.z);
		float3 v3 = make_float3(v4.x, v4.y, v4.z);
		float3 a3 = make_float3(a4.x, a4.y, a4.z);
		//first velocity verlet integration step:  x = x + v*dt + 0.5*a*dt^2
		x3 += v3 * _dt + 0.5f * a3 * _dt * _dt;
		//apply periodic boundary conditions
		if(x3.x < 0.0f)		x3.x += _boxSize.x;
		if(x3.x > _boxSize.x)	x3.x -= _boxSize.x;
		if(x3.y < 0.0f)		x3.y += _boxSize.y;
		if(x3.y > _boxSize.y)	x3.y -= _boxSize.y;
		if(x3.z < 0.0f)		x3.z += _boxSize.z;
		if(x3.z > _boxSize.z)	x3.z -= _boxSize.z;
		//update velocity:  v = v + 0.5*a*dt
		v3 += 0.5f * a3 * _dt;
		//store results back in tuple
		thrust::get<0>(t) = make_float4( x3, x4.w);	
		thrust::get<1>(t) = make_float4( v3, v4.w);	
	}
};

/**
 *	@brief functor for the second step of the Verlet integrator.
 * 	Executes @f$ v = v + 0.5 a \Delta t @f$ after the new acceleration is computed by ::accelerateCelllists or ::accelerateNBody.

	See ::integrate1_functor for more information.
	@see ::integrate1_functor
 */
struct integrate2_functor{
	float _dt; ///< timestep
	///Constructor
	__host__ __device__ integrate2_functor(float dt) : _dt(dt) {}
	/**
	 *	@brief overloaded operator(), actual Verlet integration.
	 *	@param[in, out] t templated parameter, in this case a @c thrust::tuple is passed.
		@todo volatile
	 */
	template <typename Tuple>
	__host__ __device__ void operator()(Tuple t){
		//extract tuple
		volatile float4 v4 = thrust::get<0>(t);
		volatile float4 a4 = thrust::get<1>(t);
		float3 v3 = make_float3(v4.x, v4.y, v4.z);
		float3 a3 = make_float3(a4.x, a4.y, a4.z);
		// v += 0.5*a*dt
		v3 += 0.5f * a3 * _dt;
		//store results
		thrust::get<0>(t) = make_float4( v3, 0.0f);	
		thrust::get<2>(t) = dot(v3,v3);	
	}
};

/**
	@name Device functions
 	Device functions are parts of one or more kernels that have their own definition.
 	You can recognize device functions by the @c __device__ keyword in front of them. 
 	These can only be called by kernels or other device functions.
 	Some functions can be executed both on the host and the device, they have the keywords <tt> __host__ __device__ </tt>.
	@{
*/
/**
 *	@brief Converts the position of a particle to the corresponding cell in the grid
 *	@param[in] p xyz-coordinates of the particle
 *	@returns the coordinates of the cell in which the particle is
 */
__device__ int3 calcGridPos(float3 p) {
    int3 gridPos;
    gridPos.x = floor((p.x - params.worldOrigin.x) / params.cellSize.x);
    gridPos.y = floor((p.y - params.worldOrigin.y) / params.cellSize.y);
    gridPos.z = floor((p.z - params.worldOrigin.z) / params.cellSize.z);
    return gridPos;
}

/**
 * 	@brief Calculates memory addres for cell using the position in the grid.
 *	Currently uses bit operations, grid sizes need to be powers of 2
 * 	@param[in] gridPos position in the grid, can be obtained from ::calcGridPos
 * 	@returns memory addres for cell
	@todo don't use linear cell id, maybe something like z order curve
 */
__device__ uint calcGridHash(int3 gridPos) {
	// wrap grid, assumes size is power of 2
	//gridPos.x = gridPos.x & (params.gridSize.x-1);  
	//gridPos.y = gridPos.y & (params.gridSize.y-1);
	//gridPos.z = gridPos.z & (params.gridSize.z-1);
	gridPos.x = gridPos.x < 0 ? gridPos.x + params.gridSize.x : gridPos.x;  
	gridPos.x = gridPos.x >= params.gridSize.x ? gridPos.x - params.gridSize.x : gridPos.x;  
	gridPos.y = gridPos.y < 0 ? gridPos.y + params.gridSize.y : gridPos.y;  
	gridPos.y = gridPos.y >= params.gridSize.y ? gridPos.y - params.gridSize.y : gridPos.y;  
	gridPos.z = gridPos.z < 0 ? gridPos.z + params.gridSize.z : gridPos.z;  
	gridPos.z = gridPos.z >= params.gridSize.z ? gridPos.z - params.gridSize.z : gridPos.z;  
	return gridPos.z * params.gridSize.y * params.gridSize.x + gridPos.y * params.gridSize.x + gridPos.x;
}

/**
 *	@brief Computes interaction between two particles.
 *	First applies periodic boundary conditions, then checks if \n
 *	-# particles are the same (zero distance), \n
 *	-# distance is greater than the cutoff value (also catches dummy particles at infinite distance  when using nbody approach).
 *
 *	If all are false, returns the interaction (and virial), else returns 0.
 *	@note Since the acceleration only needs a @c float3 we use the @c w value of the @c float4 to store the contribution to the virial.
 * 	See implementation for details
 * 	@param[in] x4_pi position of first particle
 * 	@param[in] x4_pj position of second particle
 *	@returns The interaction between (and virial contribution of) the two particles
 *	@warning calculation assumes cubic box
 */
__device__ float4 ppInteraction(float4 x4_pi, float4 x4_pj){
	float3 xr3;
	//half the box size
	float hl = params.boxSize.x/ 2.0f;
	xr3.x = x4_pi.x - x4_pj.x;
	xr3.y = x4_pi.y - x4_pj.y;
	xr3.z = x4_pi.z - x4_pj.z;
	//apply periodic boundary conditions
	if(xr3.x < - hl) xr3.x += 2*hl;
	if(xr3.x >   hl) xr3.x -= 2*hl;
	if(xr3.y < - hl) xr3.y += 2*hl;
	if(xr3.y >   hl) xr3.y -= 2*hl;
	if(xr3.z < - hl) xr3.z += 2*hl;
	if(xr3.z >   hl) xr3.z -= 2*hl;
	//compute distance squared
	float r2 = dot(xr3,xr3);
	//checks if the particles are the same (distance == 0) or if one particle is dummy (distance == inf)
	if(r2 > params.cutoff*params.cutoff || r2==0.0f ) return make_float4(0.0f); 
	float r6i = 1/(r2*r2*r2);
	//compute virial contribution
	float ff = 48*r6i*(r6i-0.5f);
	//return acceleration (ff/r2*xr3) and the virial
	return make_float4(ff/r2*xr3,ff);
}

/**
 *	@brief Computes potential between two particles.
 *	First applies periodic boundary conditions, then checks if \n
 *	-# particles are the same (zero distance), \n
 *	-# distance is greater than the cutoff value (also catches dummy particles at infinite distance  when using nbody approach).
 *
 *	If all are false, returns the potential, else returns 0.
 * 	@param[in] x4_pi position of first particle
 * 	@param[in] x4_pj position of second particle
 *	@returns The potential between the two particles
 *	@warning calculation assumes cubic box
 */
__device__ float ppPotential(float4 x4_pi, float4 x4_pj){
	float3 xr3;
	/// @todo assumes cubic box
	float hl = params.boxSize.x/ 2.0f;
	xr3.x = x4_pi.x - x4_pj.x;
	xr3.y = x4_pi.y - x4_pj.y;
	xr3.z = x4_pi.z - x4_pj.z;
	if(xr3.x < - hl) xr3.x += 2*hl;
	if(xr3.x >   hl) xr3.x -= 2*hl;
	if(xr3.y < - hl) xr3.y += 2*hl;
	if(xr3.y >   hl) xr3.y -= 2*hl;
	if(xr3.z < - hl) xr3.z += 2*hl;
	if(xr3.z >   hl) xr3.z -= 2*hl;
	float r2 = dot(xr3,xr3);
	//checks if the particles are the same (distance == 0) or if one particle is dummy (distance == inf)
	if(r2 > params.cutoff*params.cutoff || r2==0.0f ) return 0.0f; 
	//if(r2==0.0f || isinf(r2)) return make_float4(0.0f); 
	float r6i = 1/(r2*r2*r2);
	return 4*r6i*(r6i-1.0f)-params.potentialCorrection;
}

/**
 *	@brief Calculate particle-particle interaction for a tile in the n x n acceleration matrix.
 *	Calls ::ppInteraction and for every entry in the row. 
 *	Remember that this function is called from ::calculate_forces_nbody,
 *	so @a myPos is dependend on the thread which calls this function. 
 *	@c shPos (see source) on the other hand, is in shared memory and is the same for all threads in the block.
 *	Since all threads in the block access the same entry in shared memory at the same time, 
 *	broadcasting is used (meaning: no serialization penalty).	
 *	@param[in] myPos the position of the particle the thread is responsible for	
 *	@param[in,out] accel the acceleration so far
 *	@returns the acceleration added by this tile plus the acceleration already computed in previous tiles.
 * 	@see ::ppInteraction
 * 	@see ::calculate_forces_nbody
 */
__device__ float4 tile_calculation(float4 myPos, float4 accel){
	extern __shared__ float4 shPos[];
	for( int i = 0; i < blockDim.x; ++i){
		accel += ppInteraction(myPos, shPos[i]);
	}
	return accel;
}

/**
 *	@brief Device function that calculates the contribution of a neighboring cell for one particle
 *	Is called from ::calculate_forces_celllist for one particle and one of its neighboring cells.
 *	Iterates over all the particles in the cell and computes the acceleration and virial contribution by calling ::ppInteraction.
 *	@param[in] gridPos the position of the cell in the grid
 *	@param[in] index the index of the particle that we want to calculate the contribution for
 *	@param[in] pos the position of the particles with index @p index
 *	@param[in] sortedPos array with positions of all particles
 *	@param[in] cellStart array with start of each cell
 *	@param[in] cellEnd array with end of each cell
 *	@returns the contribution of this cell to the acceleration and the virial of the particle with index @p index
 *	@see ::calculate_forces_celllist
 *	@see ::ppInteraction
 */
__device__ float4 calculateForcesCell(int3 gridPos, uint index, float4 pos, float4 *sortedPos, uint *cellStart, uint *cellEnd) {
	uint gridHash = calcGridHash(gridPos);

	//get start of bucket for this cell
	uint startIndex = cellStart[gridHash];

	float4 acc = make_float4(0.0f);
	
	if(startIndex != 0xffffffff) { //cell is not empty
		//get end of bucket for this cell
		uint endIndex = cellEnd[gridHash];
		//iterate over particles in this cell
		for( uint j = startIndex; j < endIndex; ++j){
			if( j != index ) { //avoid self
				float4 pos2 = sortedPos[j];
				acc += ppInteraction(pos, pos2);
			}
		}
	}
	return acc;
}
				
/**
 *	@brief Device function that calculates the contribution of a neighboring cell for one particle
 *	Is called from ::calculate_potential_celllist for one particle and one of its neighboring cells.
 *	Iterates over all the particles in the cell and computes the potential contribution by calling ::ppPotential.
 *	@param[in] gridPos the position of the cell in the grid
 *	@param[in] index the index of the particle that we want to calculate the contribution for
 *	@param[in] pos the position of the particles with index @p index
 *	@param[in] sortedPos array with positions of all particles
 *	@param[in] cellStart array with start of each cell
 *	@param[in] cellEnd array with end of each cell
 *	@returns the contribution of this cell to the potential of the particle with index @p index
 *	@see ::calculate_potential_celllist
 *	@see ::ppPotential
 */
__device__ float calculatePotentialCell(int3 gridPos, uint index, float4 pos, float4 *sortedPos, uint *cellStart, uint *cellEnd) {
	uint gridHash = calcGridHash(gridPos);

	//get start of bucket for this cell
	uint startIndex = cellStart[gridHash];

	float potential = 0.0f;
	
	if(startIndex != 0xffffffff) { //cell is not empty
		//iterate over particles in this cell
		//get end of bucket for this cell
		uint endIndex = cellEnd[gridHash];
		for( uint j = startIndex; j < endIndex; ++j){
			if( j != index ) { //avoid self
				//printf("particle %d\n", j);
				float4 pos2 = sortedPos[j];
				potential += ppPotential(pos, pos2);
				//printf("%f %f %f %f\n",acc.x, acc.y, acc.z, acc.w);
			}
		}
	}
	return potential;
}


/** @}*/

/**
	@name Kernels
 	Kernels are a parallel portion of an application launched on the GPU.
 	Can be recognized by a @c __global__ in front. 
 	Kernels need to be launched with a launch configuration, 
 	e.g. <tt> <<< nb, tpb, shmemsize>>>Kernel() </tt>, where @c nb is the number of blocks, @c tpb the threads per block, 
 	and @c shmemsize (optional) the size of the shared memory. \n
	@{
*/

/**
 *	@brief Calculates grid hash value for each particle.
 *	Calls ::calcGridHash for every particle and restore the indices to the unsorted positions
 *	@param[in] pos particle position
 * 	@param[in] numParticles number of particles
 * 	@param[out] gridParticleHash array containing the particles hashes
 *	@param[out] gridParticleIndex array containing the order of the particles
 *	@todo check volatile
 	@see ::calcGridPos
	@see ::calcGridHash
 */
__global__
void calcHashD(uint   *gridParticleHash,  
           uint   *gridParticleIndex,
           float4 *pos,               
           uint    numParticles) {
	uint index = blockIdx.x * blockDim.x + threadIdx.x;

	if (index >= numParticles) return;

	volatile float4 p = pos[index];

	// get address in grid
	int3 gridPos = calcGridPos(make_float3(p.x, p.y, p.z));
	uint hash = calcGridHash(gridPos);

	// store grid hash and particle index
	gridParticleHash[index] = hash;
	gridParticleIndex[index] = index;
}

/**
 *	@brief Reorders sorted particles and determine cells.
 *	Particles are sorted by ::sortParticles, which sorts the hashes in @p gridParticleHash.
 * 	The sort permutation is stored in @p gridParticleIndex.
 *	Determines start and stop of each cell by checking if neighboring hashes differ, if they differ we have a new cell.
 *	Finally puts the particle positions in @p oldPos in sorted order in @p sortedPos. 
 *	@param[out] cellStart array with start of each cell
 *	@param[out] cellEnd array with end of each cell
 *	@param[out] sortedPos sorted positions
 *	@param[in] gridParticleHash sorted grid hashes
 *	@param[in] gridParticleIndex sorted particle indices
 *	@param[in] oldPos unsorted particle positions
 *	@param[in] numParticles number of particles
 *	@todo See if <a href="http://docs.thrust.googlecode.com/hg/group__transformations.html#gaecdeb69a74548f924e3a9de420a84516">thrust::adjacent_difference()</a> can be used.
 */
__global__
void reorderDataAndFindCellStartD(uint   *cellStart,        
                                  uint   *cellEnd,          
                                  float4 *sortedPos,        
                                  uint   *gridParticleHash, 
                                  uint   *gridParticleIndex,
                                  float4 *oldPos,           
                                  uint    numParticles) {
	// blockSize + 1 elements
	extern __shared__ uint sharedHash[];    
	uint index = blockIdx.x * blockDim.x + threadIdx.x;

	uint hash;
	// handle case when no. of particles not multiple of block size
	if (index < numParticles)
	{
	    hash = gridParticleHash[index];

	    // Load hash data into shared memory so that we can look
	    // at neighboring particle's hash value without loading
	    // two hash values per thread
	    sharedHash[threadIdx.x+1] = hash;

	    if (index > 0 && threadIdx.x == 0)
	    {
		// first thread in block must load neighbor particle hash
		sharedHash[0] = gridParticleHash[index-1];
	    }
	}

	__syncthreads();

	if (index < numParticles)
	{
	    // If this particle has a different cell index to the previous
	    // particle then it must be the first particle in the cell,
	    // so store the index of this particle in the cell.
	    // As it isn't the first particle, it must also be the cell end of
	    // the previous particle's cell

	    if (index == 0 || hash != sharedHash[threadIdx.x])
	    {
		cellStart[hash] = index;

		if (index > 0)
		    cellEnd[sharedHash[threadIdx.x]] = index;
	    }

	    //last hash is always the end of a cell
	    if (index == numParticles - 1)
	    {
		cellEnd[hash] = index + 1;
	    }

	    // Now use the sorted index to reorder the pos data
	    uint sortedIndex = gridParticleIndex[index];
	    float4 pos = oldPos[sortedIndex];

	    sortedPos[index] = pos;
}


}

/**
 *	@brief Kernel that calculates acceleration using the nbody approach.
 *	Divides the acceleration matrix into @c n by @c n tiles, where @c n is the block dimension.
 * 	If the block dimension is not a divisor of the number of particles, append dummy particles at infinite distance.
 *	Then run ::tile_calculation on each tile, and return the acceleration and the virial for each particle.
 *	@param[in] d_x4 array with position of each particle
 *	@param[out] d_a4 array with acceleration of each particle
 *	@param[out] virial array with virial contribution of each particle
 *	@param[in] numParticles number of particles
 *	@see ::tile_calculation
 *	@todo see if ternary operator makes a difference on possible warp divergence
 */
__global__ void calculate_forces_nbody(float4 *d_x4, float4 *d_a4, float *virial, int numParticles){
	//compute global thread id	
	int gtid = blockIdx.x * blockDim.x + threadIdx.x; 
	//reserve some space in shared memory to store information that must be accessed by all threads at the same time
	extern __shared__ float4 shPos[];
	//reserve space to store own position
	float4 myPos;
	//set acceleration to zero
	float4 acc = make_float4(0.0f);
	//if the thread is responsible for a real (not dummy) particle load my position
	if(gtid < numParticles) myPos = d_x4[gtid];
	//do calculation on a tile (the size of which is determined by the launch configuration)
	for( int tile = 0; tile < gridDim.x ; ++tile){
		//see which particle I am responsible for in this tile
		int idx = tile * blockDim.x + threadIdx.x;
		//if it is a real particle store it's position in shared memory
		if(idx < numParticles)	shPos[threadIdx.x] = d_x4[idx];
		//if it is a dummy set the particles position in shared memory to inf
		else shPos[threadIdx.x] = make_float4(logf(0));
		//synchronize to make sure that we're al at the same point of execution, needed for memory broadcast
		__syncthreads();
		//if responsible for a real particle calculate acceleration due to the particles in the tile
		if(gtid < numParticles) acc = tile_calculation(myPos, acc);
		//sync again
		__syncthreads();
	}
	//if real particle store acceleration and virial
	if(gtid < numParticles) {
		d_a4[gtid] = acc;
		virial[gtid] = acc.w;
	}
}

/**
 *	@brief Kernel that calculates acceleration using the celllist approach.
 *	For every particle determine the neighboring cells, 
 *	than calculate the acceleration and virial contribution for that cell using ::calculateForcesCell.
 *	@param[out] d_a4 array with acceleration of each particle
 *	@param[out] virial array with virial contribution of each particle
	@param[in] gridParticleIndex index of sorted particles
	@param[in] cellStart array with start of each cell
	@param[in] cellEnd array with end of each cell
 *	@param[in] d_x4 array with position of each particle
 *	@param[in] numParticles number of particles
 *	@see ::calculateForcesCell
 */
__global__ void calculate_forces_celllist (	float4 *d_a4, float *virial, float4 *d_x4, 
						uint *gridParticleIndex,
						uint *cellStart, uint *cellEnd,
						uint numParticles) {
	uint index = blockIdx.x * blockDim.x + threadIdx.x;

	if(index >= numParticles) return;

	//read particle data from sorted arrays
	float4 pos = d_x4[index];
	float4 acc = make_float4(0.0f);

	int3 gridPos = calcGridPos(make_float3(pos));

	//examine neighboring cells
	for(int z = -1; z <= 1; ++z) {
		for(int y = -1; y <= 1; ++y) {
			for(int x = -1; x <= 1; ++x) {
				int3 neighborPos = gridPos + make_int3(x,y,z);
				acc += calculateForcesCell(neighborPos, index, pos, d_x4, cellStart, cellEnd);
			}
		}
	}

	// for big particles do separate routines here

	//write new acceleration back to original unsorted location
	uint originalIndex = gridParticleIndex[index];
	d_a4[originalIndex] = acc;
	virial[originalIndex] = acc.w;
}

/**
 *	@brief Kernel that calculates potential using the celllist approach.
 *	For every particle determine the neighboring cells, 
 *	than calculate the potential for that cell using ::calculatePotentialCell
 *	@param[out] pot array with potential contribution of each particle
	@param[in] gridParticleIndex index of sorted particles
	@param[in] cellStart array with start of each cell
	@param[in] cellEnd array with end of each cell
 *	@param[in] sortedPos sorted array with position of each particle
 *	@param[in] numParticles number of particles
 *	@see ::calculatePotentialCell
 */
__global__ void calculate_potential_celllist (	float *pot, float4 *sortedPos, 
						uint *gridParticleIndex,
						uint *cellStart, uint *cellEnd,
						uint numParticles) {
	//uint index = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
	uint index = blockIdx.x * blockDim.x + threadIdx.x;

	if(index >= numParticles) return;

	//read particle data from sorted arrays
	float4 pos = sortedPos[index];
	float potential = 0.0f;

	int3 gridPos = calcGridPos(make_float3(pos));
	//printf("gridpos %d %d %d\n", gridPos.x, gridPos.y, gridPos.z);

	//examine neighboring cells
	for(int z = -1; z <= 1; ++z) {
		for(int y = -1; y <= 1; ++y) {
			for(int x = -1; x <= 1; ++x) {
				int3 neighborPos = gridPos + make_int3(x,y,z);
				//printf("neighbor %d %d %d\n", neighborPos.x, neighborPos.y, neighborPos.z);
				potential += calculatePotentialCell(neighborPos, index, pos, sortedPos, cellStart, cellEnd);
				//printf("%f %f %f %f\n",acc.x, acc.y, acc.z, acc.w);
			}
		}
	}

	// for big particles do separate routines here

	//write new acceleration back to original unsorted location
	uint originalIndex = gridParticleIndex[index];
	pot[originalIndex] = potential;
}

/**@}*/

#endif
