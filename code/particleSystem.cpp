/**
	@file particleSystem.cpp
	@brief Implementation of the ParticleSystem class
	@see particleSystem.hpp
	@see ParticleSystem
	@todo use derived classes instead of SimulationMode
*/

#include "particleSystem.h"
#include "particleSystem.cuh"
#include "particles_kernel.cuh"

#include "../cuda_helper/cutil_math.h"
#include "error_analysis.hpp" //code for computing standard error
#include "../cuda_helper/cuda_timer.cuh" //code for timing

#include <fstream> 			//for ofstream
#include <boost/format.hpp>		//format io

#include <assert.h>
#include <math.h>
#include <memory.h>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <string>

#include <boost/filesystem.hpp> 	//for editing files and directories

#ifndef PI_F
/// Definition of PI as float
#define PI_F 3.141592654f
#endif

using namespace std;
namespace bf = boost::filesystem;

/**
	@warning only works for cubic box size
 */
ParticleSystem::ParticleSystem(uint numParticles, uint3 gridSize, float3 boxSize, float temperature, string outdir, float cutoff) :
	m_bInitialized(false),
	m_velscaling(false),
	m_numParticles(numParticles),
	m_hPos(NULL),
	m_hVel(NULL),
	m_hAcc(NULL),
	m_dPos(NULL),
	m_dVel(NULL),
	m_dAcc(NULL),
	m_gridSize(gridSize),
	m_boxSize(boxSize),
	m_temperature(temperature),
	m_outdir(outdir),
	m_cutoff(cutoff),
	/// @todo set seed
	//m_rng(BoostRNG(0))
	m_rng(BoostRNG(time(NULL)))
{
	//create output directory if it doesn't exist
	if(!bf::is_directory( m_outdir ))
		bf::create_directory( m_outdir );

	m_numGridCells = m_gridSize.x * m_gridSize.y * m_gridSize.z;	

	//set simulation parameters
	m_params.gridSize = m_gridSize;
	m_params.numCells = m_numGridCells;
	m_params.boxSize = m_boxSize;
	m_params.temperature = m_temperature;
	m_params.cutoff = m_cutoff;
	m_params.potentialCorrection = 4.0f * (powf(1/m_cutoff,12) - powf(1/m_cutoff,6)); //4 * (rc^-12 - rc^-6)
	
	m_params.worldOrigin = make_float3(0.0f);
	float cellSize = m_boxSize.x / m_gridSize.x;
	m_params.cellSize = make_float3(cellSize);

	m_density = m_numParticles/(m_boxSize.x*m_boxSize.y*m_boxSize.z);
	m_pressureCorrection = 16.0f/3.0f * M_PI *m_density*m_density * (2.0f/3.0f*powf(1/m_cutoff,9)-powf(1/m_cutoff,3));
	m_potentialEnergyCorrection = 8.0f/9.0f * M_PI *m_density * (powf(1/m_cutoff,9)-3.0f*powf(1/m_cutoff,3));
	m_potentialEnergyCorrection += 4.0f/3.0f*M_PI * m_cutoff * m_cutoff * m_cutoff * m_density * 0.5f * m_params.potentialCorrection;
	cout << m_potentialEnergyCorrection << endl;

	_initialize(numParticles);
}

ParticleSystem::~ParticleSystem() {
	_finalize();
	m_numParticles = 0;
}

/**
	@brief Initializes particle system.
	Seeds random number generator, allocates memory for CPU and GPU arrays 
	and copies ParticleSystem::m_params (instance of SimParams) to constant memory.
	Finally sets ParticleSystem::m_bInitialized to true
*/
void ParticleSystem::_initialize(uint numParticles) {
	assert(!m_bInitialized);

	m_numParticles = numParticles;
	unsigned int memSize = sizeof(float)*4*m_numParticles;

	//allocate host storage
	m_hPos = new float[m_numParticles*4];
	m_hVel = new float[m_numParticles*4];
	m_hAcc = new float[m_numParticles*4];
	memset(m_hPos, 0, memSize);
	memset(m_hVel, 0, memSize);
	memset(m_hAcc, 0, memSize);
	
	m_hCellStart = new uint[m_numGridCells];
	memset(m_hCellStart, 0, m_numGridCells*sizeof(uint));

	m_hCellEnd = new uint[m_numGridCells];
	memset(m_hCellEnd, 0, m_numGridCells*sizeof(uint));

	m_hParticleHash = new uint[m_numParticles];
	memset(m_hParticleHash, 0 , m_numParticles*sizeof(uint)); 
	
	//allocate GPU data
	allocateDeviceArray((void **)&m_dPos, memSize);
	allocateDeviceArray((void **)&m_dVel, memSize);
	allocateDeviceArray((void **)&m_dAcc, memSize);

	allocateDeviceArray((void **)&m_dSortedPos, memSize);

	allocateDeviceArray((void **)&m_dVirial, m_numParticles*sizeof(float));
	allocateDeviceArray((void **)&m_dKineticEnergy, m_numParticles*sizeof(float));
	allocateDeviceArray((void **)&m_dPotentialEnergy, m_numParticles*sizeof(float));

	allocateDeviceArray((void **)&m_dGridParticleHash, m_numParticles*sizeof(uint));
	allocateDeviceArray((void **)&m_dGridParticleIndex, m_numParticles*sizeof(uint));

	allocateDeviceArray((void **)&m_dCellStart, m_numGridCells*sizeof(uint));
	allocateDeviceArray((void **)&m_dCellEnd, m_numGridCells*sizeof(uint));

	setParameters(&m_params);

	m_bInitialized = true;
}

/**
	@brief frees CPU and GPU memory
	Called by the destructor ParticleSystem::~ParticleSystem().
	@see ParticleSystem::~ParticleSystem().
*/
void ParticleSystem::_finalize() {
	assert(m_bInitialized);
	
	delete [] m_hPos;
	delete [] m_hVel;
	delete [] m_hAcc;
	delete [] m_hCellStart;
	delete [] m_hCellEnd;
	delete [] m_hParticleHash;

	
	freeDeviceArray(m_dPos);	
	freeDeviceArray(m_dVel);	
	freeDeviceArray(m_dAcc);	

	freeDeviceArray(m_dSortedPos);	

	freeDeviceArray(m_dVirial);
	freeDeviceArray(m_dKineticEnergy);
	freeDeviceArray(m_dPotentialEnergy);

	freeDeviceArray(m_dGridParticleHash);
	freeDeviceArray(m_dGridParticleIndex);
	freeDeviceArray(m_dCellStart);
	freeDeviceArray(m_dCellEnd);
}

/**
	@brief The actual MC simulation.
	@param[in] equilibrationIter number of equilibration cycles
	@param[in] iterations number of measurement cycles
	@param[in] mode simulation mode (nbody or celllists)
	@param[in] deltaTime timestep to be used in the discretization
	@see ParticleSystem::MDstep
	@todo include parameter to set number of output files
	@todo maybe make the acceleration a virtual function
	@todo add simulationmode
	@warning only does celllists
*/
void ParticleSystem::startMCSimulationNVT(uint equilibrationIter, uint iterations, SimulationMode mode, float deltaTime) {
	assert(m_bInitialized);
	
	//add space to store backup of positions
	float *dPosBackup;
	int memSize = 4*m_numParticles*sizeof(float);
	allocateDeviceArray((void **)&dPosBackup, memSize);
	backupArrayDevice(dPosBackup, m_dPos, memSize);

	//calculate hash for all particles
	calcHash( m_dGridParticleHash, m_dGridParticleIndex, m_dPos, m_numParticles);

	//sort particles based on hash
	sortParticles(m_dGridParticleHash, m_dGridParticleIndex, m_numParticles);

	//reorder particle arrays into sorted order and 
	//find start and end of each cell
	reorderDataAndFindCellStart(
		m_dCellStart, m_dCellEnd,
		m_dSortedPos,
		m_dGridParticleHash, m_dGridParticleIndex,
		m_dPos,
		m_numParticles, m_numGridCells);

	//set initial potential
	potentialCelllists (m_dPotentialEnergy, m_dSortedPos, m_dGridParticleIndex, m_dCellStart, m_dCellEnd, m_numParticles);
	m_potentialEnergy = 0.5*thrustReduce(m_dPotentialEnergy, m_numParticles);
	m_potentialEnergy += m_numParticles*m_potentialEnergyCorrection;
	float oldPotential = m_potentialEnergy;

	//set initial acceleration and pressure
	accelerateCelllists (m_dAcc, m_dVirial, m_dSortedPos, m_dGridParticleIndex, m_dCellStart, m_dCellEnd, m_numParticles);
	m_virial = 0.5f*thrustReduce(m_dVirial, m_numParticles);
	m_pressure = m_density*m_kineticEnergy*2/(3*m_numParticles) + m_virial/(3*m_boxSize.x*m_boxSize.y*m_boxSize.z); 
	m_pressure += m_pressureCorrection;
	float oldPressure = m_pressure;

	//set initial total energy
	float totalEnergy = m_potentialEnergy + m_kineticEnergy;
	float initTotalEnergy = totalEnergy; 

	cout << "iter\tPE\t\tKE\t\tTE\t\tdrift" << endl;
	cout << boost::format("%3d\t%6f\t%6f\t%6f\t%+6f") % 0 % m_potentialEnergy % m_kineticEnergy % initTotalEnergy % 0.0f << endl ;

	float diffTotalEnergy;
	float diffInitTotalEnergy;
	float oldTotalEnergy;

	uint totalIter = equilibrationIter + iterations;
	ErrorAnalysis pressureData(iterations);
	for(uint i = 1; i < totalIter + 1; ++i) {

		//set new velocity here
		resetVel(); //kinetic energy should be set here
		totalEnergy = m_potentialEnergy + m_kineticEnergy;
		oldTotalEnergy = totalEnergy;
		//cout << "before move " << totalEnergy << endl; 

		for(uint j = 0; j < 10 + 1; ++j) {
			MDstep(mode, deltaTime); //potential not set here, virial set here!
		}

		potentialCelllists (m_dPotentialEnergy, m_dSortedPos, m_dGridParticleIndex, m_dCellStart, m_dCellEnd, m_numParticles);
		m_potentialEnergy = 0.5*thrustReduce(m_dPotentialEnergy, m_numParticles);
		m_potentialEnergy += m_numParticles* m_potentialEnergyCorrection;
		totalEnergy = m_potentialEnergy + m_kineticEnergy;
		diffTotalEnergy = totalEnergy - oldTotalEnergy;
		diffInitTotalEnergy = totalEnergy - initTotalEnergy;
		//cout << boost::format("%3d\t%6f\t%6f\t%6f\t%+6f") % i % m_potentialEnergy % m_kineticEnergy % totalEnergy % diffInitTotalEnergy << endl ;
		int accept = 1;
		if(m_rng.uniform() >= expf(-(1/m_temperature)*diffTotalEnergy)) accept=0;
		//cout << boost::format("move: %+6f\t\t%3f\t%d\n") % diffTotalEnergy % (expf(-(1/m_temperature)*diffTotalEnergy)) % accept;

		if(accept == 1){
			//store new positions, pressure, potential in backup
			backupArrayDevice(dPosBackup,m_dPos, memSize);
			oldPressure = m_pressure;
			oldPotential = m_potentialEnergy;
		} else {
			//restore particle positions
			backupArrayDevice(m_dPos, dPosBackup, memSize);
			m_potentialEnergy = oldPotential;
			m_pressure = oldPressure;

			//calculate hash for all particles
			calcHash( m_dGridParticleHash, m_dGridParticleIndex, m_dPos, m_numParticles);

			//sort particles based on hash
			sortParticles(m_dGridParticleHash, m_dGridParticleIndex, m_numParticles);

			//reorder particle arrays into sorted order and 
			//find start and end of each cell
			reorderDataAndFindCellStart(
				m_dCellStart, m_dCellEnd,
				m_dSortedPos,
				m_dGridParticleHash, m_dGridParticleIndex,
				m_dPos,
				m_numParticles, m_numGridCells);

			accelerateCelllists (m_dAcc, m_dVirial, m_dSortedPos, m_dGridParticleIndex, m_dCellStart, m_dCellEnd, m_numParticles);

		}
		if(i > equilibrationIter) pressureData.push(m_pressure);
		if(i % 1000 == 0) {
			cout << i << endl;
			cout << boost::format("pressure: %f\n") % (m_pressure);
		}
		
	//writeForGL();

	}
	cout << boost::format("average pressure = %6f with error %6f \n") % (pressureData.mean()) % (pressureData.error()) ;

}

/**
	@brief The actual MC simulation in NPT
	@param[in] equilibrationIter number of equilibration cycles
	@param[in] equilibrationIter2 number of equilibration cycles for subsequent pressures (compression)
	@param[in] iterations number of measurement cycles
	@param[in] mode simulation mode (nbody or celllists)
	@param[in] deltaTime timestep to be used in the discretization
	@param[in] pressure the desired pressure 
	@param[in] dlnV log volume displacement to be used in trial volume moves
	@see ParticleSystem::MDstep
	@todo see if timestep can be included in SimParam
	@todo include parameter to set number of output files
	@todo add simulationmode
	@todo see what needs to be done about timing code
*/
void ParticleSystem::startMCSimulationNPT(uint equilibrationIter, uint equilibrationIter2, uint iterations, SimulationMode mode, float deltaTime, float pressure, float dlnV) {
	assert(m_bInitialized);

	//EventTimer *cet = new EventTimer(); 

	//add space to store backup of positions
	float *dPosBackup;
	int memSize = 4*m_numParticles*sizeof(float);
	allocateDeviceArray((void **)&dPosBackup, memSize);

	//cet->start();

	//calculate hash for all particles
	calcHash( m_dGridParticleHash, m_dGridParticleIndex, m_dPos, m_numParticles);

	//sort particles based on hash
	sortParticles(m_dGridParticleHash, m_dGridParticleIndex, m_numParticles);

	//reorder particle arrays into sorted order and 
	//find start and end of each cell
	reorderDataAndFindCellStart(
		m_dCellStart, m_dCellEnd,
		m_dSortedPos,
		m_dGridParticleHash, m_dGridParticleIndex,
		m_dPos,
		m_numParticles, m_numGridCells);

	//set initial potential energy
	potentialCelllists (m_dPotentialEnergy, m_dSortedPos, m_dGridParticleIndex, m_dCellStart, m_dCellEnd, m_numParticles);
	m_potentialEnergy = 0.5*thrustReduce(m_dPotentialEnergy, m_numParticles);
	m_potentialEnergy += m_numParticles*m_potentialEnergyCorrection;

	//set initial acceleration and pressure
	accelerateCelllists (m_dAcc, m_dVirial, m_dSortedPos, m_dGridParticleIndex, m_dCellStart, m_dCellEnd, m_numParticles);
	m_virial = 0.5f*thrustReduce(m_dVirial, m_numParticles);
	m_pressure = m_density*m_kineticEnergy*2/(3*m_numParticles) + m_virial/(3*m_boxSize.x*m_boxSize.y*m_boxSize.z); 
	m_pressure += m_pressureCorrection;

	//set initial total energy
	//float totalEnergy = m_potentialEnergy + m_kineticEnergy;
	//float initTotalEnergy = totalEnergy; 

	//cet->stop();
	//printf("Initialization:\n\t %f s\n",cet->elapsed()/1000);


	//cout << "iter\tPE\t\tKE\t\tTE\t\tdrift" << endl;
	//cout << boost::format("%3d\t%6f\t%6f\t%6f\t%+6f") % 0 % m_potentialEnergy % m_kineticEnergy % initTotalEnergy % 0.0f << endl ;


	int nMovAtt = 0, nMovAcc = 0, nVolAtt = 0, nVolAcc = 0;
	int  niVolAtt = 0, niVolAcc = 0;
	double totalmcmove = 0, totalmcvol = 0;

	//set file for density log
	
	string ofname = "density.log";
	ofstream os;
	os.open(ofname.c_str(),ios::trunc);
	if( !os.is_open() ){
		cerr << "error: could not open file " << ofname << endl ;
	}
#ifdef COMP
float pressure_arr [9] = { 0.1776, 0.3290, 0.4890, 0.7000, 1.0710, 1.7500, 3.0280, 5.2850, 9.1200 };

string eqnstofname = "eqnst.txt";
ofstream eqnstos;
eqnstos.open(eqnstofname.c_str(),ios::trunc);
if(!eqnstos.is_open()){
	cerr << "error: could not open file " << eqnstofname << endl ;
}

eqnstos << "#n=" << m_numParticles << " equi=" << equilibrationIter << " equi2=" << equilibrationIter2 << " iter=" << iterations << "\n" ; 
eqnstos << "#pressure\tdensity\tmpressure\n" ;

for(uint pstep = 0; pstep < 9; pstep++){
	pressure = pressure_arr[pstep];
	printf("\npressure = %f\n",pressure);

#endif

	//instantiate error analysis for pressure and density
	ErrorAnalysis pressureData(iterations);
	ErrorAnalysis densityData(iterations);

	uint totalIter = equilibrationIter + iterations;

	//main loop
	for(uint i = 1; i < totalIter + 1; ++i) {
		
		//test ratio particle/volume moves
		float randnr = m_rng.uniform();
		//int randnr = (int) (m_rng.uniform()*(m_numParticles+1));
		//if(randnr < m_numParticles) { /*trial move*/
		if(randnr < 0.9 ) { /*trial move*/
			
			nMovAtt++;
			//cet->start();
			
			//backup old values
			float oldPE = m_potentialEnergy;
			float oldKE = m_kineticEnergy;
			float oldPressure = m_pressure;
			float oldVirial = m_virial;

			//backup positions
			backupArrayDevice(dPosBackup, m_dPos, memSize);

			//reset velocities according to maxwell boltzman
			resetVel(); //kinetic energy should be set here
			float beforeTotalEnergy = m_potentialEnergy + m_kineticEnergy;

			//do MD simulation, replaces the traditional trial move
			for(uint j = 0; j < 10 + 1; ++j) {
				MDstep(mode, deltaTime); //potential not set here, virial set here!
			}

			//calculate new potential energy
			potentialCelllists (m_dPotentialEnergy, m_dSortedPos, m_dGridParticleIndex, m_dCellStart, m_dCellEnd, m_numParticles);
			m_potentialEnergy = 0.5*thrustReduce(m_dPotentialEnergy, m_numParticles);
			m_potentialEnergy += m_numParticles* m_potentialEnergyCorrection;

			//compute difference in energy
			float afterTotalEnergy = m_potentialEnergy + m_kineticEnergy;
			float diffTotalEnergy = afterTotalEnergy - beforeTotalEnergy;

			//calculate acceptance
			int accept = 1;
			float acceptprob = expf(-(1/m_temperature)*diffTotalEnergy);
			if(m_rng.uniform() >= acceptprob) accept=0;
			//cout << boost::format("move: %+6f\t\t%3f\t%d\n") % diffTotalEnergy % acceptprob % accept;

			if(accept == 0) { //not accepted, revert changes
				//restore particle positions
				backupArrayDevice(m_dPos, dPosBackup, memSize);

				//calculate hash for all particles
				calcHash( m_dGridParticleHash, m_dGridParticleIndex, m_dPos, m_numParticles);

				//sort particles based on hash
				sortParticles(m_dGridParticleHash, m_dGridParticleIndex, m_numParticles);

				//reorder particle arrays into sorted order and 
				//find start and end of each cell
				reorderDataAndFindCellStart(
					m_dCellStart, m_dCellEnd,
					m_dSortedPos,
					m_dGridParticleHash, m_dGridParticleIndex,
					m_dPos,
					m_numParticles, m_numGridCells);

				accelerateCelllists (m_dAcc, m_dVirial, m_dSortedPos, m_dGridParticleIndex, m_dCellStart, m_dCellEnd, m_numParticles);

				m_potentialEnergy = oldPE ;
				m_kineticEnergy = oldKE ;
				m_virial = oldVirial ;
				m_pressure = oldPressure ;

			} else { //accepted
				nMovAcc++;
			}
			//cet->stop();
			//totalmcmove += cet->elapsed()/1000;
			//end trial move
		} else { //volume move

			nVolAtt++;
			niVolAtt++;
			//cet->start();

			//backup old values
			float oldPE = m_potentialEnergy;
			float oldPressure = m_pressure;
			float oldVirial = m_virial;
			
			//determine new box size and scalefactor
			float oldV = m_boxSize.x * m_boxSize.y * m_boxSize.z;
			float lnOldV = logf(oldV);
			float lnNewV = lnOldV + dlnV*(m_rng.uniform()-0.5);
			float newV = expf(lnNewV);
			float newBoxsize = powf(newV,1.0f/3.0f);
			float scalefactor = newBoxsize / m_boxSize.x;

			//backup old positions
			backupArrayDevice(dPosBackup, m_dPos, memSize);
			//backup old simulation parameters (used in cuda kernels, stored on device)
			SimParams backupParams = m_params;

			//backup old values
			float3 oldBoxSize = m_boxSize;
			float oldDensity = m_density;
			uint3 oldGridSize = m_gridSize;
			float oldPressureCorrection = m_pressureCorrection;
			float oldPotentialEnergyCorrection = m_potentialEnergyCorrection;

			//scale particle positions
			scaleArray(m_dPos, scalefactor, 4*m_numParticles);	

			//scale box and grid
			m_boxSize *= scalefactor;
			m_params.boxSize = m_boxSize;
			m_density = m_numParticles/(m_boxSize.x * m_boxSize.y * m_boxSize.z);

			//setup new corrections for pressure and potential
			m_pressureCorrection = 16.0f/3.0f * M_PI *m_density*m_density * (2.0f/3.0f*powf(1/m_cutoff,9)-powf(1/m_cutoff,3));
			m_potentialEnergyCorrection = 8.0f/9.0f * M_PI *m_density * (powf(1/m_cutoff,9)-3.0f*powf(1/m_cutoff,3));
			m_potentialEnergyCorrection += 4.0f/3.0f*M_PI * m_cutoff * m_cutoff * m_cutoff * m_density * 0.5f * m_params.potentialCorrection;

			//check if new grid is needed
			uint gridDim = (uint) (m_boxSize.x / m_cutoff);
                        if(gridDim < 3) {
                                printf("system too small for celllists\n");
                                exit(EXIT_FAILURE);
                        }
                        m_gridSize = make_uint3(gridDim);
			/// @todo make suitable for non-cubic boxes
			float cellSize = m_boxSize.x / m_gridSize.x;
			m_params.cellSize = make_float3(cellSize);

			bool gridChanged = (m_gridSize.x != oldGridSize.x);

			//if grid is changed allocate space
			if(gridChanged){
				m_params.gridSize = m_gridSize;
				m_numGridCells = m_gridSize.x * m_gridSize.y * m_gridSize.z;
				freeDeviceArray(m_dCellStart);
				freeDeviceArray(m_dCellEnd);
				allocateDeviceArray((void **)&m_dCellStart, m_numGridCells*sizeof(uint));
				allocateDeviceArray((void **)&m_dCellEnd, m_numGridCells*sizeof(uint));
			}

			//send simulation parameters to cuda device
			setParameters(&m_params);

			//if grid is changed calculate new hashes
			if(gridChanged){
				//calculate hash for all particles
				calcHash( m_dGridParticleHash, m_dGridParticleIndex, m_dPos, m_numParticles);

				//sort particles based on hash
				sortParticles(m_dGridParticleHash, m_dGridParticleIndex, m_numParticles);
			}

			/**
			*	@todo find a way to do this more efficiently
			*	this step needs to be done because the sortedPosData is based on the old pos
			*	maybe scale the sortedposdata itself?
			*/
			//reorder particle arrays into sorted order and 
			//find start and end of each cell
			reorderDataAndFindCellStart(
				m_dCellStart, m_dCellEnd,
				m_dSortedPos,
				m_dGridParticleHash, m_dGridParticleIndex,
				m_dPos,
				m_numParticles, m_numGridCells);

			/// @todo see if potential scaling can be used
			//calculate new potential energy
			potentialCelllists (m_dPotentialEnergy, m_dSortedPos, m_dGridParticleIndex, m_dCellStart, m_dCellEnd, m_numParticles);
			m_potentialEnergy = 0.5*thrustReduce(m_dPotentialEnergy, m_numParticles);
			m_potentialEnergy += m_numParticles*m_potentialEnergyCorrection;

			float newPE = m_potentialEnergy;

			float diffTotalEnergy = newPE - oldPE;

			//calculate acceptance
			float acceptprob = expf(-(1.0/m_temperature)*(diffTotalEnergy + pressure*(newV - oldV)) + (m_numParticles+1)*(lnNewV-lnOldV));
			int accept = 1;
			if(m_rng.uniform() >= acceptprob) accept=0;
			//cout << boost::format("volume move: %+6f\t\t%+6f\t\t%3f\t%d\n") % (newV - oldV) % diffTotalEnergy % acceptprob % accept;

			if(accept == 1){ //accepted, calculate new accelerations, virial and pressure
				accelerateCelllists (m_dAcc, m_dVirial, m_dSortedPos, m_dGridParticleIndex, m_dCellStart, m_dCellEnd, m_numParticles);
				m_virial = 0.5f*thrustReduce(m_dVirial, m_numParticles);
				m_pressure = m_density*m_kineticEnergy*2/(3*m_numParticles) + m_virial/(3*m_boxSize.x*m_boxSize.y*m_boxSize.z); 
				m_pressure += m_pressureCorrection;
				nVolAcc++;
				niVolAcc++;
			} else { //not accepted, revert to old situation
				//restore particle data
				backupArrayDevice(m_dPos, dPosBackup, memSize);
				//restore simulation parameters
				m_params = backupParams;
				//send restored simulation parameters to device
				setParameters(&m_params);

				//restore old values
				m_boxSize = oldBoxSize; 
				m_density = oldDensity;
				m_gridSize = oldGridSize;
				m_pressureCorrection = oldPressureCorrection;
				m_potentialEnergyCorrection = oldPotentialEnergyCorrection;

				if(gridChanged){
					m_numGridCells = m_gridSize.x * m_gridSize.y * m_gridSize.z;
					freeDeviceArray(m_dCellStart);
					freeDeviceArray(m_dCellEnd);
					allocateDeviceArray((void **)&m_dCellStart, m_numGridCells*sizeof(uint));
					allocateDeviceArray((void **)&m_dCellEnd, m_numGridCells*sizeof(uint));

					calcHash( m_dGridParticleHash, m_dGridParticleIndex, m_dPos, m_numParticles);
				
					//sort particles based on hash
					sortParticles(m_dGridParticleHash, m_dGridParticleIndex, m_numParticles);

				}
				//reorder particle arrays into sorted order and 
				//find start and end of each cell
				reorderDataAndFindCellStart(
					m_dCellStart, m_dCellEnd,
					m_dSortedPos,
					m_dGridParticleHash, m_dGridParticleIndex,
					m_dPos,
					m_numParticles, m_numGridCells);

				//acceleration need not be calculated because it didn't change
				m_potentialEnergy = oldPE ;
				m_virial = oldVirial ;
				m_pressure = oldPressure ;
			}

			//cet->stop();
			//totalmcvol += cet->elapsed()/1000;
		}
		
		if(i <= equilibrationIter && i % 100 == 0) {
			if(niVolAtt != 0){
				double accratio = (double) niVolAcc / niVolAtt;
				if(accratio < 0.25 && dlnV > 0.000001){
					dlnV *= 0.95;
				} else if (accratio > 0.25 && dlnV < 0.2){
					dlnV *= 1.05;
				}
				//printf("dlnv changed to %f\n",dlnV);
			}
			niVolAtt = 0;
			niVolAcc = 0;
		}

		if(i % 100 == 0) {
			cout << i << endl;
			cout << boost::format("density: %f\n") % (m_density);
			cout << boost::format("pressure: %f\n") % (m_pressure);
		}
		if(i > equilibrationIter){
			pressureData.push(m_pressure);
			densityData.push(m_density);
		}
		
		
		os << m_density << "\n";

	//writeForGL();

	}

	
	fprintf(stdout,"Trial moves\n");
	fprintf(stdout,"\tTotal moves: %d\n",nMovAtt);
	fprintf(stdout,"\tTotal time: %lf s\n",totalmcmove);
	fprintf(stdout,"\tAverage time: %lf s\n",totalmcmove/nMovAtt);
	fprintf(stdout,"\tAcceptance rate: %f\n\n",(double)nMovAcc/nMovAtt); 

	fprintf(stdout,"Volume moves\n");
	fprintf(stdout,"\tTotal moves: %d\n",nVolAtt);
	fprintf(stdout,"\tTotal time: %lf s\n",totalmcvol);
	fprintf(stdout,"\tAverage time: %lf s\n",totalmcvol/nVolAtt);
	fprintf(stdout,"\tAcceptance rate: %f\n\n",(double)nVolAcc/nVolAtt); 

	cout << boost::format("average pressure = %6f with error %6f \n") % (pressureData.mean()) % (pressureData.error()) ;
	cout << boost::format("average density = %6f with error %6f \n") % (densityData.mean()) % (densityData.error()) ;

#ifdef COMP

	eqnstos << pressure << "\t" << densityData.mean() << "\t" << pressureData.mean() << "\n" ;
	eqnstos.flush();
	equilibrationIter = equilibrationIter2;
}
	eqnstos.close();
#endif


	os.close();
	
}

/**
	@brief The actual MD simulation.
	For every iteration calls ParticleSystem::MDstep.
	Before starting calculate initial acceleration.
	@param[in] equilibrationIter number of equilibration cycles
	@param[in] iterations number of measurement cycles
	@param[in] mode simulation mode (nbody or celllists)
	@param[in] deltaTime timestep to be used in the discretization
	@see ParticleSystem::MDstep
	@todo see if timestep can be included in SimParam
	@todo remove temporary hack for timing
	@todo include parameter to set number of output files
	@todo maybe make the acceleration a virtual function
*/
void ParticleSystem::startMDSimulation(uint equilibrationIter, uint iterations, SimulationMode mode, float deltaTime) {
	assert(m_bInitialized);
	m_velscaling = true;
	//assert(iterations == 1000);
	//writeForGL();
	switch(mode) {
		case NBODY:
		{
			printf("Running MD simulation using nbody approach...\n");
			accelerateNBody(m_dPos, m_dAcc, m_dVirial, m_numParticles);
			break;
		}
		case CELLLIST:
		{
			printf("Starting MD simulation using celllist approach...\n");
			//calculate hash for all particles
			calcHash( m_dGridParticleHash, m_dGridParticleIndex, m_dPos, m_numParticles);
	
			//sort particles based on hash
			sortParticles(m_dGridParticleHash, m_dGridParticleIndex, m_numParticles);

			//reorder particle arrays into sorted order and 
			//find start and end of each cell
			reorderDataAndFindCellStart(
				m_dCellStart, m_dCellEnd,
				m_dSortedPos,
				m_dGridParticleHash, m_dGridParticleIndex,
				m_dPos,
				m_numParticles, m_numGridCells);
			
			accelerateCelllists (m_dAcc, m_dVirial, m_dSortedPos, m_dGridParticleIndex, m_dCellStart, m_dCellEnd, m_numParticles);

			break;
		}
	}

	//setup data structure to store pressure measurements (defined in error_analysis.hpp)
	uint totalIter = equilibrationIter + iterations;
	ErrorAnalysis pressureData(iterations);
	for(uint i = 1; i < totalIter + 1; ++i) {
		MDstep(mode, deltaTime);
		if(i > equilibrationIter) pressureData.push(m_pressure);
		if(totalIter <10 || i%(totalIter/10) == 0) printf("%i/%i\n", i, totalIter);
		//writeForGL();
	}
	writeForGL();
	cout << boost::format("average pressure = %6f with error %6f \n") % (pressureData.mean()) % (pressureData.error()) ;


	/// @todo maybe move fileoutput to function?
	string ofname = "eqnst.txt";
	ofstream os;
	os.open(ofname.c_str(),ios::app);
	if(os.is_open()){
		os << m_density << " " << pressureData.mean() << " " << pressureData.error() << "\n";
		os.close();
	} else {
		cerr << "error: could not open file " << ofname << endl ;
	}
}
/**
	@brief Does one MD step.
	One MD step consists of the following:
	-# First Verlet Integration step ::integrateSystem1
	-# Compute Acceleration:
		- If @p mode is @c NBODY, runs ::accelerateNBody
		- If @p mode is @c CELLLIST, runs:
			-# calculate hash ::calcHash
			-# sort particles based on hash ::sortParticles
			-# reorder data into sorted order and determine cell begin and end ::reorderDataAndFindCellStart
			-# actual acceleration computation with ::accelerateCelllists
	-# Second Verlet Integration step ::integrateSystem2
	-# Calculate virial ::thrustReduce
	-# Calculate kinetic energy ::thrustReduce
	-# Calculate pressure and apply correction
	-# Scale momenta using ::scaleArray
	.
	@param[in] mode nbody or celllist approach
	@param[in] deltaTime timestep
	@todo slim down for better performance in NPT MC (maybe remove timing code (but is already controlled by CUDA_TIME macro))
*/
void ParticleSystem::MDstep(SimulationMode mode, float deltaTime) {
	assert(m_bInitialized);

	//static EventTimer *cet = new EventTimer(); 
	//static int count = 0;
	//static double totaltime = 0.0;

	//integrate1
	integrateSystem1( m_dPos, m_dVel, m_dAcc, deltaTime, m_boxSize, m_numParticles );

	//cet->start();
	//compute acceleration
	switch(mode) {
		case NBODY:
		{
			accelerateNBody(m_dPos, m_dAcc, m_dVirial, m_numParticles);
			break;
		}
		case CELLLIST:
		{
			//calculate hash for all particles
			calcHash( m_dGridParticleHash, m_dGridParticleIndex, m_dPos, m_numParticles);
	
			//sort particles based on hash
			sortParticles(m_dGridParticleHash, m_dGridParticleIndex, m_numParticles);

			//reorder particle arrays into sorted order and 
			//find start and end of each cell
			reorderDataAndFindCellStart(
				m_dCellStart, m_dCellEnd,
				m_dSortedPos,
				m_dGridParticleHash, m_dGridParticleIndex,
				m_dPos,
				m_numParticles, m_numGridCells);
			
			accelerateCelllists (m_dAcc, m_dVirial, m_dSortedPos, m_dGridParticleIndex, m_dCellStart, m_dCellEnd, m_numParticles);

		}
	}
	//cet->stop();
	//totaltime+=cet->elapsed();
	//count++;
	//if(count == 1000) {
		//printf("mean acctime %f\n", totaltime/count);
		//delete cet;
	//}

	//integrate2
	integrateSystem2( m_dVel, m_dAcc, m_dKineticEnergy, deltaTime, m_numParticles );

	//measure virial
	m_virial = 0.5f*thrustReduce(m_dVirial, m_numParticles);

	//measuring
	m_kineticEnergy = 0.5f*thrustReduce(m_dKineticEnergy, m_numParticles);

	if(m_velscaling){
		//scale momentum for MD NVT
		float instatemp = m_kineticEnergy*2/(3*m_numParticles);
		float scalefactor = sqrtf(m_temperature/instatemp);
		scaleArray(m_dVel, scalefactor, 4*m_numParticles);
	}

	m_pressure = m_density*m_kineticEnergy*2/(3*m_numParticles) + m_virial/(3*m_boxSize.x*m_boxSize.y*m_boxSize.z); 
	m_pressure += m_pressureCorrection;
	

}

/**
	@brief Dumps grid information to CPU and prints to screen
*/
void ParticleSystem::dumpGrid() {
	//dump grid information
	copyArrayFromDevice(m_hCellStart, m_dCellStart, m_numGridCells*sizeof(uint));
	copyArrayFromDevice(m_hCellEnd, m_dCellEnd, m_numGridCells*sizeof(uint));
	uint maxCellSize = 0;

	for( uint i = 0; i < m_numGridCells; ++i) {
		if (m_hCellStart[i] != 0xffffffff) {
			uint cellSize = m_hCellEnd[i] - m_hCellStart[i];
			printf("cell: %d, %d particles\n", i, cellSize);
			//printf("\tstart: %d, end: %d\n",m_hCellStart[i],m_hCellEnd[i]);
			if(cellSize > maxCellSize) maxCellSize = cellSize;
		}
	}
	
	/*
	printf("maximum particles per cell = %d\n", maxCellSize);

	uint* hParticleIndex = new uint[m_numParticles];
	copyArrayFromDevice(hParticleIndex, m_dGridParticleIndex, m_numParticles*sizeof(uint));
	for( uint i = 0; i < m_numParticles; ++i) {
		printf("particle %d has index %d\n", i, hParticleIndex[i]);
	}
	delete [] hParticleIndex;
	*/

	/*
	copyArrayFromDevice(m_hParticleHash, m_dGridParticleHash, m_numParticles*sizeof(uint));
	for( uint i = 0; i < m_numParticles; ++i) {
		printf("particle %d in cell %d\n", i, m_hParticleHash[i]);
	}
	*/
	
}

/**
	@brief Dumps particle information to CPU and prints to screen
	@param[in] start first particle to dump
	@param[in] count number of particles to dump
*/
void ParticleSystem::dumpParticles(uint start, uint count) {
	//debug
	copyArrayFromDevice(m_hPos, m_dPos, count*4*sizeof(float));
	copyArrayFromDevice(m_hVel, m_dVel, count*4*sizeof(float));
	copyArrayFromDevice(m_hAcc, m_dAcc, count*4*sizeof(float));
	
	for( uint i = start; i < start+count; ++i) {
		//printf("%d: ", i);
		uint ii = 4*i;
		printf("pos: (%.4f, %.4f, %.4f, %.4f)\n", m_hPos[ii+0], m_hPos[ii+1], m_hPos[ii+2], m_hPos[ii+3]);
		printf("vel: (%.4f, %.4f, %.4f, %.4f)\n", m_hVel[ii+0], m_hVel[ii+1], m_hVel[ii+2], m_hVel[ii+3]);
		printf("acc: (%.4f, %.4f, %.4f, %.4f)\n", m_hAcc[ii+0], m_hAcc[ii+1], m_hAcc[ii+2], m_hAcc[ii+3]);
	}
}

/**
	@brief Copies particle positions and prints them for use in franks opengl.
*/
void ParticleSystem::writeForGL() {
	copyArrayFromDevice(m_hPos, m_dPos, m_numParticles*4*sizeof(float));
	static bool first = true;
	string ofname;
	ofname = str(boost::format("%s/config_frank.sph") % m_outdir);
	ofstream os;
	if(first) {
		//if first output overwrite any existant file
		os.open(ofname.c_str(),ios::trunc);
		first = false;
	} else {
		//else append
		os.open(ofname.c_str(),ios::app);
	}
	if(os.is_open()){
		os << "&" << m_numParticles << "\n";
		os << m_boxSize.x << " " << m_boxSize.y << " "  << m_boxSize.z << "\n";
		for( uint i = 0; i < m_numParticles; ++i){
			uint ii = 4*i;
			os << (char)((i%26)+'a') << " " << m_hPos[ii+0] << " " << m_hPos[ii+1] << " " << m_hPos[ii+2] << " " << 0.5 << "\n" ;
		}
		os.close();
	} else
		cerr << "error: could not open file " << ofname << endl ;
}

/**
	@brief Copies array from the GPU to the CPU
	@param[in] array which array to copy
	@returns pointer to data on CPU
*/
float *ParticleSystem::getArray(ParticleArray array) {
	assert(m_bInitialized);

	float *hdata = 0;
	float *ddata = 0;

	switch(array) {
		default:
		case POSITION:
			hdata = m_hPos;
			ddata = m_dPos;
			break;
		case VELOCITY:
			hdata = m_hVel;
			ddata = m_dVel;
			break;
		case ACCELERATION:
			hdata = m_hAcc;
			ddata = m_dAcc;
			break;
	}

	copyArrayFromDevice(hdata, ddata, m_numParticles*4*sizeof(float));
	return hdata;
}

/**
	@brief Copies array from the CPU to the GPU
	@param[in] array which array to copy
	@param[in] data pointer to data on CPU
	@param[in] start first particle to copy
	@param[in] count number of particles to copy
*/
void ParticleSystem::setArray(ParticleArray array, const float *data, int start, int count) {
	assert(m_bInitialized);

	switch(array) {
		default:
		case POSITION:
			copyArrayToDevice(m_dPos, data, start*4*sizeof(float), count*4*sizeof(float));
			break;
		case VELOCITY:
			copyArrayToDevice(m_dVel, data, start*4*sizeof(float), count*4*sizeof(float));
			break;
		case ACCELERATION:
			copyArrayToDevice(m_dAcc, data, start*4*sizeof(float), count*4*sizeof(float));
			break;
	}
}

/**
	@brief initializes position and velocity data
	Called from ParticleSystem::resetPos
	@param[in] size the size of the particle grid
	@param[in] spacing spacing between the particles
	@warning cubic box/grid assumed
	@see ParticleSystem::resetPos
*/
void ParticleSystem::initGrid(uint *size, float spacing) { 
	for(uint z = 0; z<size[2]; ++z) {
		for(uint y = 0; y<size[1]; ++y) {
			for(uint x = 0; x<size[0]; ++x) {
				uint i = (z*size[1]*size[0]) + (y*size[0]) + x;
				if(i < m_numParticles) {
					m_hPos[i*4+0] = spacing * (x+0.5f);	
					m_hPos[i*4+1] = spacing * (y+0.5f);	
					m_hPos[i*4+2] = spacing * (z+0.5f);	
					m_hPos[i*4+3] = 0.0f; //filler for coalescence
				}
			}
		}
	}
}

/**
	@brief Resets particle positions.
	If @p config is @c CONFIG_GRID call ParticleSystem::initGrid.
	@param[in] config random or on a grid
	@see ParticleSystem::initGrid
	@see ParticleSystem::resetVel
	@warning also resets velocity by calling ParticleSystem::resetVel
*/
void ParticleSystem::resetPos(ParticleConfig config) {
	switch(config) {
		default:
		case CONFIG_GRID:
		{
			//determine lattice size
			uint s = (uint) ceilf(powf((float) m_numParticles, 1.0f/ 3.0f));
			uint gridSize[3];
			gridSize[0] = gridSize[1] = gridSize[2] = s;
			float spacing = m_boxSize.x/gridSize[0];
			initGrid(gridSize, spacing);
			break;
		}
		case CONFIG_RANDOM:
		{
			int p = 0;
			for(uint i = 0; i < m_numParticles; ++i) {
				m_hPos[p++] = m_rng.uniform();
				m_hPos[p++] = m_rng.uniform();
				m_hPos[p++] = m_rng.uniform();
				m_hPos[p++] = 0.0f; //filler for coalescence
			}
			break;
		}
	}
	setArray(POSITION, m_hPos, 0, m_numParticles);
	resetVel();
	
}

/** 
	@brief resets velocity.
	Uses uniform random number generator, then sets velocity center of mass to zero and scales to the right temperature.
	@todo see if rescaling is necessary for large number of particles
*/
void ParticleSystem::resetVel() {
	float kinEnergy;
	float sumVel[3] = {0.0f, 0.0f, 0.0f};
	for(uint i = 0; i < m_numParticles; ++i) {	
		uint ii = 4*i;
		m_hVel[ii+0] = m_rng.normal();
		m_hVel[ii+1] = m_rng.normal();
		m_hVel[ii+2] = m_rng.normal();
		m_hVel[ii+3] = 1.0f;
		sumVel[0] += m_hVel[ii+0];
		sumVel[1] += m_hVel[ii+1];
		sumVel[2] += m_hVel[ii+2];
	}
	//calculate velocity center of mass
	sumVel[0] /= m_numParticles;
	sumVel[1] /= m_numParticles;
	sumVel[2] /= m_numParticles;

	//set velocity center of mass to zero and compute kinetic energy
	kinEnergy = 0.0f;
	for( uint i = 0; i < m_numParticles; ++i) {
		uint ii = 4*i;
		m_hVel[ii+0] -= sumVel[0];
		m_hVel[ii+1] -= sumVel[1];
		m_hVel[ii+2] -= sumVel[2];
		kinEnergy += m_hVel[ii]*m_hVel[ii]+m_hVel[ii+1]*m_hVel[ii+1]+m_hVel[ii+2]*m_hVel[ii+2];
	}
	kinEnergy /= 2.0f;
	//compute instantaneous temperature
	float instaTemp = kinEnergy/m_numParticles * 2.0f / 3.0f;
	//compute scalefactor needed to get the desired temperature
	float scalefactor = sqrtf(m_temperature/instaTemp);
	kinEnergy = 0.0f;
	for( uint i = 0; i < m_numParticles; ++i) {
		uint ii = 4*i;
		m_hVel[ii+0] *= scalefactor;
		m_hVel[ii+1] *= scalefactor;
		m_hVel[ii+2] *= scalefactor;
		/// @todo see if kin energy can be done more efficiently
		kinEnergy += m_hVel[ii]*m_hVel[ii]+m_hVel[ii+1]*m_hVel[ii+1]+m_hVel[ii+2]*m_hVel[ii+2];
	}
	kinEnergy /= 2.0f;
	m_kineticEnergy = kinEnergy;
	//copy velocity to cuda
	setArray( VELOCITY, m_hVel, 0, m_numParticles);
}


