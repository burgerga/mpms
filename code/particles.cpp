
/**
	@file particles.cpp
	@brief The main program.
	Here the ParticleSystem is instantiated and ran.
*/
#include <stdlib.h>
#include <cstdlib>
#include <cstdio>
#include <algorithm>

#include "particleSystem.h"
#include "../cuda_helper/cutil_math.h"

/**
	@name Default parameters
	@{
*/
#define NUM_PARTICLES 1024
#define NUM_EQUILIBRATION 15000
#define NUM_EQUILIBRATION_2 5000
#define NUM_ITER 15000
#define TIMESTEP 0.003f
#define DENSITY 0.5f
#define TEMP 2.0f
#define OUTDIR "out"
#define CUTOFF 2.5f
#define PRESSURE 1.75f
#define DLNV 0.1f
/**@}*/

/**
	@name Parameters used to instantiate ParticleSystem
	@{
*/
uint numParticles;
int numEquilibrations; 
int numEquilibrations2; 
int numIterations;
float deltaTime;
float density;
float temperature;
float cutoff;
std::string outdir;
float pressure;
float dlnV;
/**@}*/

///The particle system
ParticleSystem *psystem = NULL;

extern "C" void cudaInit(int cmdArgDevice);
extern "C" bool checkDeviceCapabilities(int major, int minor);

/**
 	@brief main function, what's there to say ;)
	Checks if CUDA device is present and has the right compute capabilities (>= 2.0).
	Instantiates ParticleSystem and runs simulation.
	@todo use boost::program_options for neat parameter parsing
	@warning: when running make sure you set the correct parameters, pressure for NPT, density for NVT or NVE
 */
int main(int argc, char **argv) {
	printf("%s Starting...\n",argv[0]);
	if(argc < 4) {
		printf("error: too few arguments\n");
		return EXIT_FAILURE;
	}

	numParticles = NUM_PARTICLES;
	numEquilibrations = NUM_EQUILIBRATION;
	numEquilibrations2 = NUM_EQUILIBRATION_2;
	numIterations = NUM_ITER;
	deltaTime = TIMESTEP;
	density = DENSITY;
	temperature = TEMP;
	cutoff = CUTOFF;
	outdir = OUTDIR;
	pressure = PRESSURE;
	dlnV = DLNV;
	//bool bNbody = true;
	bool bNbody = false;
	int device = -1;

	device = atoi(argv[1]);
	pressure = atof(argv[2]);
	//density = atof(argv[2]);
	numParticles = atoi(argv[3]);

	//setup cuda device
	printf("Initializing CUDA...\n");
	cudaInit(device);

        // If the GPU does not meet SM2.0 capabilities, we will quit
	int desiredCCMajor = 2;
	int desiredCCMinor = 0;
        if (!checkDeviceCapabilities(desiredCCMajor,desiredCCMinor)) {
		return EXIT_FAILURE;	
	}
	
	//initializing particle system
	printf("Initializing particle system...\n");
	//set boxSize
	float3 boxSize = make_float3(powf(numParticles/density, 1.0f/3.0f));
	uint3 gridSize;
	if(bNbody) { //nbody
		/// @todo cubic
		cutoff = boxSize.x /2.0f;
		gridSize = make_uint3(0);
	} else { //celllists
		uint gridDim = (uint) (boxSize.x / cutoff);
		printf("grid dim: %d\n", gridDim);
		if(gridDim < 3) {
			printf("system too small for celllists\n");
			return EXIT_FAILURE;
		}
		gridSize = make_uint3(gridDim);
	}

	psystem = new ParticleSystem(numParticles, gridSize, boxSize, temperature, outdir, cutoff);
	psystem->resetPos(ParticleSystem::CONFIG_GRID);

	printf("Starting simulation...\n");
	if(bNbody) { //nbody
		psystem->startMDSimulation(numEquilibrations, numIterations, ParticleSystem::NBODY, deltaTime);
	} else { //celllists
		//psystem->startMDSimulation(numEquilibrations, numIterations, ParticleSystem::CELLLIST, deltaTime);
		psystem->startMCSimulationNPT(numEquilibrations, numEquilibrations2, numIterations, ParticleSystem::CELLLIST, deltaTime, pressure, dlnV);
		//psystem->startMCSimulationNVT(numEquilibrations, numIterations, ParticleSystem::CELLLIST, deltaTime);
	}
	//clean up
	printf("Cleaning up...\n");
	if(psystem) delete psystem;

	cudaDeviceReset();
	printf("Exiting...\n");
	return EXIT_SUCCESS;
}
