/**
	@file particles_kernel.cuh
	@brief Definition of the struct SimParams that goes to constant memory
	@todo check if vector_types is necesarry
*/

#pragma once
#ifndef PARTICLES_KERNEL_H
#define PARTICLES_KERNEL_H

#include "vector_types.h"
/// Definition of @c uint as <tt> unsigned int </tt>
typedef unsigned int uint;

/**
 *	@brief Structure that stores the simulation parameter and goes in constant memory.
 *	@todo see if including number of particles is convenient
 */
struct SimParams
{
	float3 worldOrigin; ///< origin of the coordinate system
	float3 boxSize; ///< size of the simulation box
	uint numCells; ///< number of cells
	uint3 gridSize; ///< size of the grid
	float3 cellSize; ///< size of the cells
	float temperature; ///< temperature in the system
	float cutoff; ///< cutoff radius potential
	float potentialCorrection; ///< potential correction for the shift in potential
};

#endif
