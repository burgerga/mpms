/**
	@file boost_rng.cpp
	@brief Implementation for the BoostRNG class.
	A random number generator that uses the mersenne twister from the boost libraries
	@see BoostRNG
	@see boost_rng.hpp
*/
#include "boost_rng.hpp"

BoostRNG::BoostRNG(uint _seed) : 
	seed(_seed), 
	rng(boost::mt19937(_seed)), 
	dist_normal(boost::normal_distribution<float>(0.0f,1.0f)), 
	dist_uniform(boost::uniform_real<float>(0.0f,1.0f)), 
	rand_normal(boost::variate_generator<boost::mt19937&, boost::normal_distribution<float> >(rng, dist_normal)),
	rand_uniform(boost::variate_generator<boost::mt19937&, boost::uniform_real<float> >(rng, dist_uniform))
{}

float BoostRNG::normal() {
	return rand_normal();
}

float BoostRNG::uniform() {
	return rand_uniform();
}

uint BoostRNG::getSeed(){
	return seed;
}
	
	
