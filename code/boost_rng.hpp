/**
	@file boost_rng.hpp
	@brief Interface for the BoostRNG class.
	A random number generator that uses the mersenne twister from the boost libraries
	@see BoostRNG
	@see boost_rng.cpp
*/
#pragma once
#ifndef BOOST_RNG_H
#define BOOST_RNG_H

#include <boost/random.hpp>

/**
	@class BoostRNG
	@brief class that provides random number generator.
	Uses the Mersenne twister from boost which can be used to sample different distributions
	without explicitly coding transformations such as the Box-Muller transform
*/
class BoostRNG {
private:
	uint seed; ///< seed for the random number generator
	boost::mt19937 rng; ///< the Mersenne twister engine class from boost
	boost::normal_distribution<float> dist_normal; ///< the normal distribution class from boost
	boost::uniform_real<float> dist_uniform; ///< the uniform real distribution class from boost
	/** an instance of the variate generator class from boost, the template parameters specify that
		a reference to the Mersenne twister engine BoostRNG::rng is to be used,
		and the normal distribution BoostRNG::dist_normal
		@see BoostRNG::rng
		@see BoostRNG::dist_normal
	*/
	boost::variate_generator<boost::mt19937&, boost::normal_distribution<float> > rand_normal;
	/** an instance of the variate generator class from boost, the template parameters specify that
		a reference to the Mersenne twister engine BoostRNG::rng is to be used,
		and the normal distribution BoostRNG::dist_normal
		@see BoostRNG::rng
		@see BoostRNG::dist_normal
	*/
	boost::variate_generator<boost::mt19937&, boost::uniform_real<float> > rand_uniform;
public:
	/**
		Constructor, initializes the BoostRNG class. 
		Because the variate_generator does not have a empty constructor,
		and uses a reference to the BoostRNG::rng, 
		all variables are initialized using initializer lists
		@param _seed the seed for the BoostRNG::rng
	*/
	BoostRNG(uint _seed);
	/**
		Returns a number from a uniform distribution.
		Min and max of the distribution are set in the constructor.
		@see BoostRNG::BoostRNG
	*/
	float uniform();
	/**
		Returns a number from a normal distribution.
		Mean and standard deviation of the distribution are set in the constructor.
		@see BoostRNG::BoostRNG
	*/
	float normal();
	/**
		Returns the seed of the rng
	*/
	uint getSeed();
};
	
#endif
