/**
	@file error_analysis.cpp
	@brief Implementation for the ErrorAnalysis class
	@see ErrorAnalysis
	@see error_analysis.hpp
*/
#include "error_analysis.hpp"

#include <boost/bind.hpp>
#include <boost/ref.hpp>
#include <algorithm> //for_each
#include <cmath>
#include <iostream>

/**
	Constructor. Sets the correct vector size and initializes counter.
	@param _nrSamples the total number of samples
	@param _nrBins the number of bins to use, a minimum of 10 is recommended
*/
ErrorAnalysis::ErrorAnalysis(int _nrSamples, int _nrBins) : nrSamples(_nrSamples) ,nrBins(_nrBins) {
	if ( nrBins == 1)
		std::cerr << "error: will not work with 1 bin will only return the mean" << std::endl;
	else if(nrBins < 10) 
		std::cerr << "warning: number of bins might be too low for error estimate" << std::endl;
	if(nrSamples % nrBins != 0) {
		std::cerr << "error: nrSamples not a multiple of number of bins" << std::endl;
		exit(EXIT_FAILURE);
	}
	bins.resize(nrBins);
	samplesPerBin = nrSamples / nrBins;
	done = false;
	count = 0;
}
/**
	Processes the data.
	Checks first if all samples are added. 
	Then divides the total of each bin in ErrorAnalysis::bins by the ErrorAnalysis::samplesPerBin	
	Sets ErrorAnalysis::done to @c true.
*/
void ErrorAnalysis::process() {
	if(count < nrSamples) {
		std::cerr << "not all samples added" << std::endl;
		exit(EXIT_FAILURE);
	}
	std::for_each( bins.begin(), bins.end(), 
		boost::bind<void>( boost::ref(acc), boost::bind<double>(std::divides<double>(), _1, samplesPerBin)));
	done = true;
	return;
}

/**
	Pushes a value to the correct bin
*/
void ErrorAnalysis::push(double value) {
	bins[count / samplesPerBin] += value;
	count++;
}

/**
	Returns the mean. 

	First checks ErrorAnalysis::done if ErrorAnalysis::bins is processed already.
	If not, runs ErrorAnalysis::process() first. 
	@see ErrorAnalysis::process 
	@returns the average of the pushed values
*/
double ErrorAnalysis::mean() {
	if(done == false) process();
	return boost::accumulators::mean(acc);
}

/**
	Returns the unbiased estimator of the variance.

	The unbiased estimator of the variance (aka the sample variance) is given by @f$s^2 = \frac{N}{N-1} n_2@f$,
	where @f$N@f$ is equal to ErrorAnalysis::nrBins and @f$n_2 = \frac{1}{N} \sum\limits_{i=1}^{N} (x_i - \bar{x})^2@f$ is the second sample central moment.
	The second sample central moment can be obtained by calling @c boost::accumulators::variance(arg),
	where @c arg is the @c boost::accumulators::accumulator_set, in our case ErrorAnalysis::acc.

	First checks ErrorAnalysis::done if ErrorAnalysis::bins is processed already.
	If not, runs ErrorAnalysis::process() first. 
	@see ErrorAnalysis::process()
	@see ErrorAnalysis::acc
	@returns the unbiased estimator of the variance
*/
double ErrorAnalysis::variance() {
	if(done == false) process();
	//return boost::accumulators::variance(acc);
	return boost::accumulators::variance(acc)*nrBins/(nrBins-1);
}

/**
	Returns the standard error of the mean using the unbiased estimator of the variance.

	The standard error is given by @f$\sqrt{s^2/N}@f$, 
	where @f$s^2@f$ is the unbiased estimator of the variance returned by ErrorAnalysis::variance(),
	and @f$N@f$ is equal to ErrorAnalysis::nrBins.

	First checks ErrorAnalysis::done if ErrorAnalysis::bins is processed already.
	If not, runs ErrorAnalysis::process() first. 
	@see ErrorAnalysis::variance()
	@see ErrorAnalysis::process()
	@returns the standard error of the mean
	
*/
double ErrorAnalysis::error() {
	if(done == false) process();
	return sqrt(variance()/nrBins);
}

