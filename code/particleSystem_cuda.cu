/**
	@file particleSystem_cuda.cu
	@brief C wrappers around CUDA functions so they can be called from particleSystem.cpp.
	Declared extern in particleSystem.cuh.
	Many functions are enclosed by a ::myCudaSafeCall to enable error checking
	@see particleSystem.cuh
	@see ::myCudaSafeCall
*/

#include<cstdlib>
#include<cstdio>
#include<string.h>

#include<thrust/device_ptr.h>
#include<thrust/for_each.h>
#include<thrust/iterator/zip_iterator.h>
#include <thrust/iterator/constant_iterator.h>
#include<thrust/sort.h>

#include "particles_kernel_impl.cuh"

#include "../cuda_helper/select_device.h"

extern "C" {
	/**
		@name Wrappers around CUDA helper utils.
		Since the CUDA helper utils (cutil) are not a part of the SDK, 
		these functions are copied to files in the @c cuda_helper directory.
		@{
	*/
	/**
		@brief Initialize cuda environment.
		Calls ::findCudaDevice.
		@param[in] cmdArgDevice desired CUDA device, -1 if not specified
		@see ::findCudaDevice
	*/	
	void cudaInit(int cmdArgDevice) {
		int devID = cmdArgDevice;
		devID = findCudaDevice(devID);
	}

	/**
		@brief Checks if device has the given compute capability.
		Runs checkCudaCapabilities.
		@param[in] major major compute capability
		@param[in] minor minor compute capability
		@returns true if device supports desired capability, false otherwise
	*/
	bool checkDeviceCapabilities(int major, int minor) {
		return checkCudaCapabilities(major, minor);
	}
	/**@}*/ //end group wrapcutil

	/**
		@name Wrappers around the CUDA Runtime (cudart) API.
		Documentation for the API (version 4.2) is <a href="http://developer.download.nvidia.com/compute/cuda/4_2/rel/toolkit/docs/online/index.html">here</a>.
		@{
	*/
	/**
		@brief Allocates device memory.
		Wrapper around cudaMalloc
		@see <a href="http://developer.download.nvidia.com/compute/cuda/4_2/rel/toolkit/docs/online/group__CUDART__MEMORY_gc63ffd93e344b939d6399199d8b12fef.html#gc63ffd93e344b939d6399199d8b12fef">cudaMalloc</a> 
	*/
	void allocateDeviceArray(void **devPtr, int size)
	{
		//cutilSafeCall(cudaMalloc(devPtr, size));
		myCudaSafeCall(cudaMalloc(devPtr, (size_t) size));
	}

	/**
		@brief Frees device memory.
		Wrapper around cudaFree
		@see <a href="http://developer.download.nvidia.com/compute/cuda/4_2/rel/toolkit/docs/online/group__CUDART__MEMORY_gb17fef862d4d1fefb9dba35bd62a187e.html#gb17fef862d4d1fefb9dba35bd62a187e">cudaFree</a> 
	*/
	void freeDeviceArray(void *devPtr)
	{
		//cutilSafeCall(cudaFree(devPtr));
		myCudaSafeCall(cudaFree(devPtr));
	}

	/**
		@brief Copy arrays from host to device.
		Wrapper around cudaMemcpy
		@see <a href="http://developer.download.nvidia.com/compute/cuda/4_2/rel/toolkit/docs/online/group__CUDART__MEMORY_g48efa06b81cc031b2aa6fdc2e9930741.html#g48efa06b81cc031b2aa6fdc2e9930741">cudaMemcpy</a>
	*/
	void copyArrayToDevice(void* device, const void* host, int offset, int size)
	{
		myCudaSafeCall(cudaMemcpy((char *) device + offset, host, size, cudaMemcpyHostToDevice));
	}

	/**
		@brief Copy arrays from device to device.
		Wrapper around cudaMemcpy
		@see <a href="http://developer.download.nvidia.com/compute/cuda/4_2/rel/toolkit/docs/online/group__CUDART__MEMORY_g48efa06b81cc031b2aa6fdc2e9930741.html#g48efa06b81cc031b2aa6fdc2e9930741">cudaMemcpy</a>
	*/
	void backupArrayDevice(void *dest, const void *source, int size){
		myCudaSafeCall(cudaMemcpy(dest, source, size, cudaMemcpyDeviceToDevice));
	}
		
	/**
		@brief Copy arrays to host from device.
		Wrapper around cudaMemcpy
		@see <a href="http://developer.download.nvidia.com/compute/cuda/4_2/rel/toolkit/docs/online/group__CUDART__MEMORY_g48efa06b81cc031b2aa6fdc2e9930741.html#g48efa06b81cc031b2aa6fdc2e9930741">cudaMemcpy</a>
	*/
	void copyArrayFromDevice(void* host, const void* device, int size)
	{ 
		//cutilSafeCall(cudaMemcpy(host, device, size, cudaMemcpyDeviceToHost));
		myCudaSafeCall(cudaMemcpy(host, device, size, cudaMemcpyDeviceToHost));
	}

	/**
		@brief Synchronize device.
		Wrapper around cudaDeviceSynchronize
		@see <a href="http://developer.download.nvidia.com/compute/cuda/4_2/rel/toolkit/docs/online/group__CUDART__DEVICE_gb76422145b5425829597ebd1003303fe.html">cudaDeviceSynchronize</a>
	*/
	void deviceSync()
	{
		//cutilSafeCall(cudaDeviceSynchronize());
		myCudaSafeCall(cudaDeviceSynchronize());
	}

	/**
		@brief copies parameters to constant memory
		Wrapper around cudaMemcpyToSymbol. Uses globally declared ::params.
		@see <a href="http://developer.download.nvidia.com/compute/cuda/4_2/rel/toolkit/docs/online/group__CUDART__MEMORY_gf268fa2004636b6926fdcd3189152a14.html#gf268fa2004636b6926fdcd3189152a14">cudaMemcpyToSymbol</a>
	*/
	void setParameters(SimParams *hostParams)
	{
		// copy parameters to constant memory
		myCudaSafeCall(cudaMemcpyToSymbol(params, hostParams, sizeof(SimParams)));
	}


	/**@}*/ //end wrapcudart


	/**
		@brief Does integer division but rounds up insteads of down.
	*/
	uint iDivUp(uint a, uint b) {
		return (a % b != 0) ? ( a / b + 1 ) : ( a / b );
	}

	/**
		@brief Computes launch configuration based on blockSize and number of particles
		@param[in] n number of particles
		@param[in] blockSize optimal (user defined) number of threads per block
		@param[out] numBlocks number of blocks ( ::iDivUp(@p n, @p numThreads))
		@param[out] numThreads number of threads per block ( min(@p blockSize, @p n))
	*/
	void computeGridSize(uint n, uint blockSize, uint &numBlocks, uint &numThreads) {
		numThreads = min(blockSize, n);
		numBlocks = iDivUp(n, numThreads);
	}

	/**
		@name Wrappers around thrust functions
		Documentation for thrust is <a href="https://code.google.com/p/thrust/wiki/Documentation">here</a>.
		@{
	*/
	/**
		@brief First Velocity Verlet integration step.
		Executes an instance of ::integrate1_functor, which does the actual integration step, on every particle.

		This function does the following steps:
		-# creates an instance of ::integrate1_functor
		-# converts each raw pointer to @c thrust::device_ptr
		-# zips together all the @c thrust::device_ptr into tuples using @c thrust::make_zip_iterator.
		Every tuple contains the position, velocity and acceleration of one particle.
		-# for every tuple call integrate1_op, this is done using @c thrust::for_each, which makes it run in parallel on the gpu
		.
		@param[in,out] pos array with positions of particles
		@param[in,out] vel array with velocities of particles
		@param[in] acc array with accelerations of particles
		@param[in] deltaTime time step
		@param[in] boxSize size of the simulation box
		@param[in] numParticles number of particles
		@see ::integrate1_functor
		@see <a href="http://docs.thrust.googlecode.com/hg/classthrust_1_1device__ptr.html">thrust::device_ptr</a>
		@see <a href="http://docs.thrust.googlecode.com/hg/group__modifying.html#ga263741e1287daa9edbac8d56c95070ba">thrust::for_each</a>
		@see <a href="http://docs.thrust.googlecode.com/hg/classthrust_1_1zip__iterator.html">thrust::zip_iterator</a>
		
	*/
	void integrateSystem1(	float *pos, float *vel, float *acc, float deltaTime, float3 boxSize, uint numParticles) {
		//construct the functor
		integrate1_functor integrate1_op(deltaTime, boxSize);
		//wrap raw pointer to thrust::device_ptr
		thrust::device_ptr<float4> d_pos4((float4 *) pos);
		thrust::device_ptr<float4> d_vel4((float4 *) vel);
		thrust::device_ptr<float4> d_acc4((float4 *) acc);
		
		thrust::for_each(
			/*combine position, velocity and acceleration together in a tuple
			and make them accesible with zip iterator */
			//define begin of the zip iterator
			thrust::make_zip_iterator(thrust::make_tuple(d_pos4,d_vel4,d_acc4)),
			//define end of the zip iterator
			thrust::make_zip_iterator(thrust::make_tuple(d_pos4+numParticles,d_vel4+numParticles,d_acc4+numParticles)),
			//functor to be executed on the tuple
			integrate1_op
		);
	}

	/**
		@brief Second Velocity Verlet integration step.
		Works similar to ::integrateSystem1, only uses ::integrate2_functor and computes kinetic energy.
		Is executed after the acceleration is computed
		@param[in,out] vel array with velocities of particles
		@param[in] acc array with accelerations of particles
		@param[out] kin array to store the kinetic energy contributions
		@param[in] deltaTime time step
		@param[in] numParticles number of particles
		@see ::integrateSystem1
		@see ::integrate2_functor
	*/
	void integrateSystem2( float *vel, float *acc, float *kin, float deltaTime, uint numParticles) {
		//construct the functor
		integrate2_functor integrate2_op(deltaTime);
		//wrap raw pointer to thrust::device_ptr
		thrust::device_ptr<float4> d_vel4((float4 *) vel);
		thrust::device_ptr<float4> d_acc4((float4 *) acc);
		thrust::device_ptr<float> d_tkin(kin);
		thrust::for_each(
			thrust::make_zip_iterator(thrust::make_tuple(d_vel4, d_acc4, d_tkin)),
			thrust::make_zip_iterator(thrust::make_tuple(d_vel4+numParticles, d_acc4+numParticles, d_tkin+numParticles)),
			integrate2_op
		);
	}

	/**
		@brief Sorts the particles based on hash.
		Does not do the actual reordering, 
		@c thrust::sort_by_key stores the permutation which is then used for the actual reordering.
		The reordering is done by ::reorderDataAndFindCellStart
		@param[in] dGridParticleHash the hashes of the particles
		@param[out] dGridParticleIndex the permutation that will put the particles in sorted order
		@param[in] numParticles number of Particles
		@see <a href="http://docs.thrust.googlecode.com/hg/group__sorting.html#ga2bb765aeef19f6a04ca8b8ba11efff24">thrust::sort_by_key</a>
		@see ::reorderDataAndFindCellStart
	*/
	void sortParticles(uint *dGridParticleHash, uint *dGridParticleIndex, uint numParticles) {
		thrust::sort_by_key(	thrust::device_ptr<uint>(dGridParticleHash),
					thrust::device_ptr<uint>(dGridParticleHash + numParticles),
					thrust::device_ptr<uint>(dGridParticleIndex));
	}

	/**
	  	@brief Reduces an array by using adding the values
		First converts the raw pointer to @c thrust::device_ptr then uses @c thrust::reduce to sum the values.
		@param[in] array the array with values to be reduced
		@param[in] length length of the array, usually equal to the number of particles
		@returns the sum of the values in @p array
		@see <a href="http://docs.thrust.googlecode.com/hg/classthrust_1_1device__ptr.html">thrust::device_ptr</a>
		@see <a href="http://docs.thrust.googlecode.com/hg/group__reductions.html#ga69434d74f2e6117040fb38d1a28016c2">thrust::reduce</a>
	*/
	float thrustReduce(float *array, uint length) {
		thrust::device_ptr<float> thrustArray(array);
		return thrust::reduce(thrustArray,thrustArray + length);
	}


	/**
		@brief scales the array with the given scalefactor.
		Executes @c thrust::transform on the array, 
		the functor <tt> thrust::multiplies<T>() </tt> is used to multiply the values with the scaling factor.
		@note The use of @c thrust::constant_iterator in @c thrust::transform saves both bandwith and memory capacity sinds it represents an array of size @p numParticles, but is not explicitly stored.
		@param[in,out] array array with the values to be scaled
		@param[in] scalefactor scaling factor for the array
		@param[in] length length of the array
		@see <a href="http://docs.thrust.googlecode.com/hg/group__transformations.html#gacbd546527729f24f27dc44e34a5b8f73">thrust::transform</a>
		@see <a href="http://docs.thrust.googlecode.com/hg/classthrust_1_1constant__iterator.html">thrust::constant_iterator</a>
		@see <a href="http://docs.thrust.googlecode.com/hg/structthrust_1_1multiplies.html">thrust::multiplies</a>
	*/
	void scaleArray(float *array, float scalefactor, uint length) {
		thrust::device_ptr<float> tarray(array);
		//use thrust transform to multiply every particles velocity with the scalefactor in parallel
		thrust::transform(	
				//begin and end of the array to transform
				tarray, tarray + length,
				/*array to multiply with 
				(the use of constant_iterator avoids wasting space on a array filled with constant value) */
				thrust::make_constant_iterator(scalefactor),
				//the begin of the array in which to store the result (in this case overwrites the old array)
				tarray,
				//the functor to call for the transformation, in this case we want to multiply two floats
				thrust::multiplies<float>() );
	}

	/**@}*/ //end wrapthrust


	/**
		@name Wrappers around CUDA kernels
		Kernels are defined in particles_kernel_impl.cuh
		@{
	*/
	/**
		@brief Calculate grid hash for all particles.
		Wrapper around ::calcHashD
		First determine optimal launch configuration using ::computeGridSize, then launches the kernel,
		then checks for errors in kernel execution.
		@param[out] gridParticleHash array to store the particle hashes
		@param[out] gridParticleIndex array to store the particle indices
		@param[in] pos	array containing positions of particles
		@param[in] numParticles number of particles
		@see ::computeGridSize
		@see ::calcHashD
		@see ::myCudaCheckError
		@todo determine optimal threads per block
		
	*/
	void calcHash( uint *gridParticleHash, uint *gridParticleIndex, float *pos, int numParticles) {
		uint numThreads, numBlocks;
		computeGridSize(numParticles, 256, numBlocks, numThreads);
		//execute kernel
		calcHashD<<< numBlocks , numThreads >>>(gridParticleHash,gridParticleIndex, (float4 *) pos, numParticles);

		//check for errors
		myCudaCheckError();
	}

	
	/**
		@brief Reorder particle data into sorted order and determine first and last particle for each cell.
		Wrapper around ::reorderDataAndFindCellStartD
		First sets all cells to empty, then determine optimal launch configuration using ::computeGridSize, then launches the kernel,
		then checks for errors in kernel execution.
		@note the shared memory size (3rd argument in <<< ... >>> ) is defined in the kernel.
		@param[out] cellStart array with start of each cell
		@param[out] cellEnd array with end of each cell
		@param[out] sortedPos sorted positions
		@param[in] gridParticleHash sorted grid hashes
		@param[in] gridParticleIndex sorted particle indices
		@param[in] oldPos unsorted particle positions
		@param[in] numParticles number of particles
		@param[in] numCells number of cells
		
	*/
	void reorderDataAndFindCellStart(uint *cellStart, uint *cellEnd,
					float *sortedPos,
					uint *gridParticleHash, uint *gridParticleIndex, 
					float *oldPos,
					 uint numParticles, uint numCells) {
		uint numThreads, numBlocks;
		computeGridSize(numParticles, 256, numBlocks, numThreads);

		// set all cells to empty
		myCudaSafeCall(cudaMemset(cellStart, 0xffffffff, numCells*sizeof(uint)));

		uint smemSize = sizeof(uint)*(numThreads+1);
		reorderDataAndFindCellStartD<<< numBlocks, numThreads, smemSize>>>(
			cellStart, cellEnd,
			(float4 *) sortedPos,
			gridParticleHash, gridParticleIndex,
			(float4 *) oldPos,
			 numParticles);
		myCudaCheckError();
	}
	
	/**
		@brief Compute the acceleration by using the nbody approach.
		Wrapper around kernel ::calculate_forces_nbody.
		First determine optimal launch configuration using ::computeGridSize, then launches the kernel,
		then checks for errors in kernel execution.
		@note the shared memory size (3rd argument in <<< ... >>> ) is defined in the kernel.
		@param[in] pos array with positions of particles
		@param[out] acc array to store accelerations of particles
		@param[out] virial array to store virial contributions of particles
		@param[in] numParticles number of particles
		@see ::computeGridSize
		@see ::calculate_forces_nbody
		@see ::myCudaCheckError
		@todo determine optimal threads per block
	 */
	void accelerateNBody(float *pos, float *acc, float *virial, uint numParticles) {
		uint numBlocks, numThreads;
		computeGridSize(numParticles, 256, numBlocks, numThreads);
		calculate_forces_nbody <<< numBlocks, numThreads, numThreads*sizeof(float4) >>>((float4 *) pos, (float4 *) acc, virial, numParticles);

		//check if kernel invocation generated an error
		myCudaCheckError();
	}
	
	/**
		@brief Compute the acceleration by using celllists.
		Wrapper around kernel ::calculate_forces_celllist.
		First determine optimal launch configuration using ::computeGridSize, then launches the kernel,
		then checks for errors in kernel execution.
		@note the shared memory size (3rd argument in <<< ... >>> ) is defined in the kernel.
		@param[out] acc array to store accelerations of particles
		@param[out] virial array to store virial contributions of particles
		@param[in] sortedPos array with sorted positions of particles
		@param[in] gridParticleIndex indices of the particles in sorted order
		@param[in] cellStart array with starting particles of cells
		@param[in] cellEnd array with ending particles of cells
		@param[in] numParticles number of particles
		@see ::computeGridSize
		@see ::calculate_forces_celllist
		@see ::myCudaCheckError
		@todo determine optimal threads per block
	 */
	void accelerateCelllists( float *acc, float *virial, float *sortedPos, uint *gridParticleIndex, uint *cellStart, uint *cellEnd, uint numParticles) {
		uint numThreads, numBlocks;
		computeGridSize(numParticles, 256, numBlocks, numThreads);
		calculate_forces_celllist <<< numBlocks, numThreads >>> ( 	(float4 *) acc, virial, (float4 *) sortedPos, 
										gridParticleIndex, 
										cellStart, cellEnd, 
										numParticles);
		//check if kernel invocation generated an error
		myCudaCheckError();
	}

	/**
		@brief Compute the potential energy by using celllists.
		Wrapper around kernel ::calculate_potential_celllist.
		First determine optimal launch configuration using ::computeGridSize, then launches the kernel,
		then checks for errors in kernel execution.
		@note the shared memory size (3rd argument in <<< ... >>> ) is defined in the kernel.
		@param[out] potentialEnergy the potential energy
		@param[in] sortedPos array with sorted positions of particles
		@param[in] gridParticleIndex indices of the particles in sorted order
		@param[in] cellStart array with starting particles of cells
		@param[in] cellEnd array with ending particles of cells
		@param[in] numParticles number of particles
		@see ::computeGridSize
		@see ::calculate_potential_celllist
		@see ::myCudaCheckError
		@todo determine optimal threads per block
	 */
	void potentialCelllists( float *potentialEnergy, float *sortedPos, uint *gridParticleIndex, uint *cellStart, uint *cellEnd, uint numParticles) {
		uint numThreads, numBlocks;
		computeGridSize(numParticles, 256, numBlocks, numThreads);
		calculate_potential_celllist <<< numBlocks, numThreads >>> ( 	potentialEnergy, (float4 *) sortedPos, 
										gridParticleIndex, 
										cellStart, cellEnd, 
										numParticles);
		//check if kernel invocation generated an error
		myCudaCheckError();
	}

	/**@}*/ //end wrapkernel
} // extern "C"
