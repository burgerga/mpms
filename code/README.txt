README for shortrange-md

the code consists of several parts:

particles.cpp: 	main file, here you select the device, and start the simulation. 
		if you are only running simulations you probably only need to understand this file

particleSystem.hpp: 	the framework for a particle simulation system
particleSystem.cpp: 	the implementation of the framework, here the actual code is implemented on how a simulation goes
particleSystem.cuh: 	an overview of the cuda functions which are called from particleSystem.cpp, 
			the "extern C" means that these functions are to be imported from another file (particleSystem_cu.o)

particleSystem_cuda.cu: the implementation of the functions which are imported by particleSystem.cuh
			the kernels that are called (with the <<< >>>) and the functors for thrust are defined in particles_kernel_impl.cuh

particles_kernel.cuh: 	defines the structure with simulation parameters that is copied to constant memory
particles_kernel_impl.cuh:	the actual cuda kernels, and functors that are used by thrust. contain all the gory details ;)

error_analysis.hpp: interface for error analysis, uses boost. see this file if you want to know how to use it.
error_analysis.cpp: implementation
