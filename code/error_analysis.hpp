/**
	@file error_analysis.hpp
	@brief Interface for the ErrorAnalysis class
	@see ErrorAnalysis
	@see error_analysis.cpp
*/
#pragma once
#ifndef ERROR_ANALYSIS_H
#define ERROR_ANALYSIS_H

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <vector>


/**
	@class ErrorAnalysis
	@brief class for determining mean and error for a observable.
	Uses batch means to avoid autocorrelation effects.
*/
class ErrorAnalysis {
private:
	bool done; ///< set to true if all samples are added and processed
	int nrSamples; ///< total number of samples
	int nrBins; ///< number of bins to divide the samples over
	int samplesPerBin; ///< number of samples per bin
	int count; ///< counter for number of samples already pushed
	std::vector<double> bins; ///< vector of doubles to store the sum of the bins
	/**
		The boost accumulator set.
		Class that stores samples and can return features, such as mean and variance
		@see http://www.boost.org/doc/html/boost/accumulators/accumulator_set.html
	*/
	boost::accumulators::accumulator_set< double, boost::accumulators::features< boost::accumulators::tag::mean, boost::accumulators::tag::variance > > acc;
	void process();
public:
	ErrorAnalysis(int _nrSamples, int _nrBins = 10);
	void push(double value);
	double mean();
	double variance();
	double error();
};
	
#endif
