/* Matrix Multiplication example on CuDa using straightforward stepping over rows/columns
 * 
 * Performs the operation : C = A * B
 * 
 * - 'A' and 'B' are two square matrices with size dim x dim;
 * - 'C' is the resulting matrix;
 * 
 * Kees Lemmens, Nov 2008, Sept 2010
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "cuda.h"

// Nr of threads per threadblock or blocksize (MAX = 16x16 for NVS290)
#define MAXBLOCKSIZE 16

/* These are simple routines stored in a separate source file as they are not really
 * important for understanding this example. */ 

extern void fillMatrix(float *a, int n, int m, int offset);
extern void showMatrix(char *name, float *a, int n, int m);
extern void checkCudaError(cudaError_t status, char *error);
extern float *initHostMatrix(int n, int m);
extern float *initCudaMatrix(int n, int m);
extern void copytoCudaMatrix(float *d_a, float *h_a, int n, int m);
extern void copyfromCudaMatrix(float *h_a, float *d_a, int n, int m);
extern void freeCudaHost(float *a);
extern void freeCudaDevice(float *a);

__global__ void d_matrixProdCuda(float *d_A, float *d_B, float *d_C, int dim)
{
   float Cs = 0;
   long x,y,z;
   
   // Each thread computes the matrix product for only one single element !
 
   x = (blockIdx.x * blockDim.x) + threadIdx.x; // row index for this thread
   y = (blockIdx.y * blockDim.y) + threadIdx.y; // col index for this thread
   
   for(z=0; z < dim; z++) // compute product for row element in A x column element in B
   {
      Cs += d_A[y * dim + z] * d_B[z * dim + x];
   }
   
   // Store result back into global memory for C :
   d_C[y * dim + x] = Cs;
}

void matrixProdCuda(float *d_A, float *d_B, float *d_C, int dim, int threadsPerBlock)
{
   cudaError_t status;
   
   // Use "gridSize" blocks and "blockSize" threads per block :
   dim3 gridSize(dim/threadsPerBlock, dim/threadsPerBlock);
   dim3 blockSize(threadsPerBlock, threadsPerBlock);
			
   // Use 1 block with dim threads just for testing/comparison (only for small dim !):
   // dim3 gridSize(1, 1);      // total number of blocks in grid
   // dim3 blockSize(dim, dim); // total number of threads per block

   // Use n blocks with 1 thread per block just for testing/comparison :
   // dim3 gridSize(dim, dim); // total number of blocks in grid
   // dim3 blockSize(1, 1);    // total number of threads per block

   d_matrixProdCuda <<<gridSize, blockSize>>> (d_A, d_B, d_C, dim);
 
   status = cudaGetLastError();
   checkCudaError(status,"Kernel execution error !");
}

int main(int argc, char** argv)
{    
   struct timeval ti1,ti2;
   double runtime;
   int dim = 4;
   float *h_A,     *h_B,     *h_C;
   float *d_A = 0, *d_B = 0, *d_C = 0;
   int threadsPerBlock;

   if(argc >=2 )
     sscanf(argv[1],"%ld",&dim);

   threadsPerBlock = (dim > MAXBLOCKSIZE ? MAXBLOCKSIZE : (dim/4)*4);
   fprintf(stderr,"Matrix product with dim = %d, blocksize = %d*%d\n",dim,threadsPerBlock,threadsPerBlock);
      
   gettimeofday(&ti1,NULL);/* read starttime in t1 */

   /* Allocate host memory for the matrices */
   h_A = initHostMatrix(dim,dim);
   h_B = initHostMatrix(dim,dim);
   h_C = initHostMatrix(dim,dim);
   
   fillMatrix(h_A,dim,dim, 0);
   fillMatrix(h_B,dim,dim, 10);
   
   /* Allocate device memory for the matrices */
   d_A = initCudaMatrix(dim,dim);
   d_B = initCudaMatrix(dim,dim);
   d_C = initCudaMatrix(dim,dim);
   
   /* Initialize the device matrices with the host matrices */
   copytoCudaMatrix(d_A,h_A,dim,dim);
   copytoCudaMatrix(d_B,h_B,dim,dim);
   
   /* Clear last error */
   cudaGetLastError();
   
   /* Performs operation using Cuda kernel above */
   matrixProdCuda(d_A, d_B, d_C, dim, threadsPerBlock);
   
   /* Read the result back */
   copyfromCudaMatrix(h_C,d_C,dim,dim);
   
   gettimeofday(&ti2,NULL); /* read endtime in t2 */
   runtime = (ti2.tv_sec - ti1.tv_sec) + 1e-6*(ti2.tv_usec - ti1.tv_usec);

   showMatrix("C",h_C,dim,dim);
   
   /* Memory clean up */
   
   freeCudaDevice(d_A);
   freeCudaDevice(d_B);
   freeCudaDevice(d_C);
   
   freeCudaHost(h_A);
   freeCudaHost(h_B);
   freeCudaHost(h_C);
   
   fflush(stderr);
   fprintf(stderr,"\nCuda : run time = %f secs.\n",runtime);
   
   return EXIT_SUCCESS;
}
