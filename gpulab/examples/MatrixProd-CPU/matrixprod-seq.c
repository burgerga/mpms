/*
 * Sequential matrix product example for the MPI practice:
 * 
 * Performs the operation : C = A * B
 * 
 * - 'A' and 'B' are two square matrices with size NxN;
 * - 'C' is the resulting matrix;
 *
 * November 2008; Kees Lemmens; TWA-EWI TU Delft.
*/  

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

//#define DEBUG 2  // Show all columns
//#define DEBUG 1  // Show only last column

//typedef double real;
typedef float real;

real **initMatrix(int n, int m)
{
   int t;
   real *ptr;
   real **mtr;
   
   ptr = (real  *) calloc(n * m, sizeof(real)); // rows x columns
   mtr = (real **) calloc(n, sizeof(real *));   // rows 
   
   if(ptr == NULL || mtr == NULL)
   {
      fprintf(stderr,"Malloc for matrix strip failed !\n");
      exit(1);
   }
   
   for(t=0;t<m;t++)
   {
      mtr[t] = ptr;
      ptr   += n;
   }
   
   return mtr;
}

void fillMatrix(real **a, int n, int m, int offset)
{  long x,y;
   
   for(y=0; y<m; y++)
     for(x=0; x<n; x++)
       a[y][x] = (real) (x + y + offset);
}

/* Next function simply prints a matrix on the screen. */
void showMatrix(char *name, real **a, int n, int m)
{
#if (DEBUG > 0)
   int x,y;
 
   for(y=0; y<m; y++)
   {
# if (DEBUG < 2)
      x = n - 1;
# else
      for(x=0; x<n; x++)
# endif
	printf("%s[%02u][%02u]=%6.2f  ",name,y,x,a[y][x]);
      printf("\n");
   }
#endif
}

void matrixprod(real **a, real **b, real **c, int m, int n)
{
   int x,y,z;
   
   for(y=0;y<m;y++)
     for(x=0;x<n;x++)
     {
	c[y][x] = 0;
	for(z=0;z<n;z++)
	  c[y][x] += a[y][z] * b[z][x];
     }
}

int main(int argc, char *argv[])
{
   real **A, **B, **C;
   struct timeval ti1,ti2;
   long dim = 4;
   real runtime;

   if(argc >=2 )
     sscanf(argv[1],"%ld",&dim);
   
   fprintf(stderr,"Matrix product with dim = %ld\n",dim);
   
   gettimeofday(&ti1,NULL); /* read starttime in t1 */
   
   A = initMatrix(dim,dim);
   B = initMatrix(dim,dim);
   C = initMatrix(dim,dim);
   
   fillMatrix(A,dim,dim, 0);
   fillMatrix(B,dim,dim, 10);
   
   matrixprod(A,B,C,dim,dim);
   
   gettimeofday(&ti2,NULL); /* read endtime in t2 */
   
   showMatrix("A",A,dim,dim);
   showMatrix("B",B,dim,dim);
   showMatrix("C",C,dim,dim);
   
   runtime = (ti2.tv_sec - ti1.tv_sec ) + 1e-6*(ti2.tv_usec - ti1.tv_usec);
   
   fflush(stderr);
   fprintf(stderr,"\nSequential : run time = %f secs.\n",runtime);
   
   return 0;
}
