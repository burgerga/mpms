// This program contains some deliberate errors in GPU kernels that should be detected
// if this program is run through cuda-memcheck ! It was taken from the cuda-memcheck
// documentation by NVidia.
// 
// Run this program using : "cuda-memcheck --continue memcheck_demo"
// to ensure cuda-memcheck continues after the first error
// 
// Kees Lemmens, Sept. 2011

#include <stdio.h>

// __device__ int t; // only to influence the value of the pointer to x
// __device__ int y;
__device__ int x; //

__global__ void unaligned_kernel(void) 
{
   // Take the address of x, add 5 and then store the value of 47 at
   // this new pointer, assuming it points to an integer value :
   *(int*) ((char*)&x + 5) = 47; // should fail 
// *(int*) ((char*)&x    ) = 47; // should work 
}

__global__ void out_of_bounds_kernel(void) 
{
   // Store the value of 47 at a location which is outside the
   // available local memory space (in this case around 2.2 GByte) :
   *(int*) 0x87654321 = 47; // should fail
// *(int*) 0x00101000 = 47; // should work
}

int main() 
{   
   printf("Running unaligned_kernel\n");
   unaligned_kernel<<<1,1>>>();
   printf("Ran unaligned_kernel: %s\n",
	  cudaGetErrorString(cudaGetLastError()));
   printf("Sync: %s\n", cudaGetErrorString(cudaThreadSynchronize()));
   
   printf("Running out_of_bounds_kernel\n");
   out_of_bounds_kernel<<<1,1>>>();
   printf("Ran out_of_bounds_kernel: %s\n",
	  cudaGetErrorString(cudaGetLastError()));
   printf("Sync: %s\n", cudaGetErrorString(cudaThreadSynchronize()));
   
   return 0;
}

