
N=1024
s=1:N;
t=zeros(1,N);

for i=1:100:N
  for j=1:2
    t(1,i+j) = 1;
  end
end

out = fopen("pulse-1024.dat","w");
fprintf(out,"%E\n",t);
fclose(out);

f=fft(t,N);
out = fopen("pulse-1024-oct.fft","w");
for i=1:N
  fprintf(out,"%E\n",abs(f(i))/N);
end
fclose(out);

#figure(1);
#plot(s,t);
#figure(2);
#plot(s,abs(f));
#sleep(5);

exit
