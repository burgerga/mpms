
N=1024
s=1:N;
t=zeros(1,N);

t(1,512) = 5;

out = fopen("needle-1024.dat","w");
fprintf(out,"%E\n",t);
fclose(out);

f=fft(t,N);
out = fopen("needle-1024-oct.fft","w");
for i=1:N
#  fprintf(out,"%E %E\n",real(f(i)),imag(f(i)));
  fprintf(out,"%E\n",abs(f(i))/N);
end  
fclose(out);
  
#figure(1);
#plot(s,t);
#figure(2);
#plot(s,abs(f));
exit
