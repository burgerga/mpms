
N=1024
fr=256;
s=1:N;
t=zeros(1,N);

t = sin(2*pi*s/fr);

out = fopen("sine-1024.dat","w");
fprintf(out,"%E\n",t);
fclose(out);

f=fft(t,N);
out = fopen("sine-1024-oct.fft","w");
for i=1:N
   fprintf(out,"%E   \n",abs(f(i))/N);
# fprintf(out,"%E %E\n",real(f(i)),imag(f(i)));
end
fclose(out);

#figure(1);
#plot(s,t);
#figure(2);
#plot(s,abs(f));
#sleep(3);
exit
