/* Simple 1D FFT example using CuDa */
/* Kees Lemmens, Nov 2008 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

//NOTE : stdarg functions in files with .cu extension do NOT work with nvcc, they
//       must be in a separate standard .c or .cpp extension file since gcc 4.4.x !!

void ldebug(int level, char*fmt, ...);

void ldebug(int level, char *fmt, ...)
{
   va_list p;
   
   if (DEBUG < level)
     return;
   va_start(p,fmt);
   vfprintf(stderr,fmt, p);
   va_end(p);
}

