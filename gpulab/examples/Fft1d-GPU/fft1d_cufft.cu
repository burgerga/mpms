/* Simple 1D FFT example using CuDa */
/* Kees Lemmens, Nov 2008 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/time.h>

/* Includes, cuda */
#include "cuda.h"
#include "cufft.h"

/* Matrix size */
//#define N 1024
#define BATCH 1
//#define DEBUG 1

//#define N 2048
//#define DEBUG 1

// extern void ldebug(int, char*, ...);  /* for C++ code */
extern "C" void ldebug(int, char*, ...); /* for standard C code */

char *readLine(FILE *in,char *name,char *buffer,int n)
{
   if(fgets(buffer,n,in) == NULL)
   {
      fprintf(stderr,"Inputfile %s too short !!\n",name);
      exit(1);
   }
   else
     return buffer;
}

int readdim(int argc, char *argv[])
{
   ldebug(1,"readdim entered ...\n");
   
   if(argc <= 1)
   {
      printf("Please supply a dimension.\n");
      exit(1);
   }
   
   return atoi(argv[1]);
}

int fillCVector(cufftComplex *a,int n,int argc, char *argv[])
{
   FILE *in;
   char buffer[100]="";
   int i;

   ldebug(1,"fillCVector entered ...\n");
   
   if(argc <= 1)
   {
      printf("Please supply a filename.\n");
      exit(1);
   }
   
   if((in = fopen(argv[1],"r")) == NULL)
   {
      fprintf(stderr,"Open of inputfile %s failed !!\n",argv[1]);
      exit(1);
   }
   
   for(i=0;i<n;i++)
   {
      do
      {
	 readLine(in,argv[1],buffer,100);
      }
      while( strlen(buffer) < 3); // skip emtpy lines
      sscanf(buffer,"%f",&a[i].x);
      a[i].y = 0.0; // imaginary part
   }
   fclose(in);
   return 0;
}

cufftComplex *initCVector(int n)
{
   cufftComplex *ptr;
   
   ldebug(1,"initCVector entered ...\n");
   
   ptr = (cufftComplex *) calloc(n, sizeof(cufftComplex));
   
   if(ptr == NULL)
   {
      fprintf(stderr,"Malloc for complex vector on host failed !\n");
      exit(1);
   }
   return ptr;
}

void showCVector(char *name, cufftComplex *a, int n)
{  int i;
   
   for(i=0; i<n; i++)
   {
//    printf("%s[%02u]=%E +i%E \n",name,i,a[i].x,a[i].y);
//    printf("%E %E\n",a[i].x,a[i].y);
      printf("%E\n",sqrt(a[i].x * a[i].x  + a[i].y * a[i].y));
   }
}

void checkCudaError(cudaError_t status, char *error)
{
   if (status != cudaSuccess)
   {
      fprintf (stderr, "Cuda : %s\n",error);
      exit(EXIT_FAILURE);
   }
}

void checkCufftResult(cufftResult_t status, char *error)
{
   if (status != CUFFT_SUCCESS) {
      fprintf (stderr, "Cuda CUFFT : %s\n",error);
      exit(EXIT_FAILURE);
   }
}

cufftComplex *initCufftCVector(int n)
{
   cufftComplex *ptr;
   cudaError_t status;

   ldebug(1,"initCufftCVector entered ...\n");

   status = cudaMalloc( (void**)&ptr, n * sizeof(cufftComplex));
   checkCudaError(status,"Malloc for complex vector on device failed !");
   
   return ptr;
}

void copytoCufftCVector(cufftComplex *d_a, cufftComplex *h_a, int n)
{
   cudaError_t status;
   
   ldebug(1,"copytoCufftCVector entered ...\n");

   status = cudaMemcpy(d_a, h_a, n *sizeof(cufftComplex), cudaMemcpyHostToDevice);
   checkCudaError(status," Copy complex data to device failed !");
}

void copyfromCufftCVector(cufftComplex *h_a, cufftComplex *d_a, int n)
{
   cudaError_t status;
   
   ldebug(1,"copyfromCufftCVector entered ...\n");

   status = cudaMemcpy(h_a, d_a, n *sizeof(cufftComplex), cudaMemcpyDeviceToHost);
   checkCudaError(status," Copy complex data from device failed !");
}

void scaleCVector(cufftComplex *h_a, int n)
{
   int i;
   
   for(i=0;i<n;i++)
   {
      h_a[i].x /= (float)n;
      h_a[i].y /= (float)n;
   }
}

void freeCuda(cufftComplex *a)
{
   cudaError_t status;
   
   status = cudaFree(a);
   checkCudaError(status,"Memory free error !");
}

/* Main */
int main(int argc, char *argv[])
{  
   struct timeval ti1,ti2;
   double runtime;
   int dim;
   cufftHandle plan;
   cufftResult status;
   
   cufftComplex *h_At;
   cufftComplex *h_Af;
   cufftComplex *d_At = 0;
   cufftComplex *d_Af = 0;
   
   dim = readdim(argc--,argv++);
   
   fprintf(stderr,"\n");
   
   gettimeofday(&ti1,NULL);        /* read starttime in t1 */
   
   /* Allocate host memory for the vectors */
   h_At = initCVector(dim);
   h_Af = initCVector(dim);

   /* Allocate device memory for the vectors */
   d_At = initCufftCVector(dim*BATCH);
   d_Af = initCufftCVector(dim*BATCH);

   status = cufftPlan1d(&plan, dim, CUFFT_C2C, BATCH);
   checkCufftResult(status,"Plan init has failed !");
   
   fillCVector(h_At,dim,argc,argv);
   
   /* Initialize the device vector with the host vector */
   copytoCufftCVector(d_At,h_At,dim);
   
   /* Performs operation using cufft */
   ldebug(1,"cufftExecC2C starting ...\n");
   status = cufftExecC2C(plan,d_At,d_Af,CUFFT_FORWARD);
   checkCufftResult(status,"Kernel execution error !");
   ldebug(1,"cufftExecC2C finished ...\n");
   
   /* Read the result back */
   copyfromCufftCVector(h_Af,d_Af,dim);

   /* And scale with dim if wanted as this is not done inside CUDA FFT : */
   scaleCVector(h_Af,dim);
   
   gettimeofday(&ti2,NULL);        /* read endtime in t2 */
   
   showCVector("F",h_Af,dim);
   
   /* Memory clean up */
   free(h_At);
   free(h_Af);
   
   freeCuda(d_At);
   freeCuda(d_Af);
   
   status = cufftDestroy(plan);
   checkCufftResult(status,"Plan destroy has failed !");
   
   runtime = (ti2.tv_sec - ti1.tv_sec) + 1e-6*(ti2.tv_usec-ti1.tv_usec);

   fflush(stderr);
   fprintf(stderr,"\nCufft : run time = %f secs.\n",runtime);
   
   return EXIT_SUCCESS;
}
