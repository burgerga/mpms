/* Simple matrix Multiplication example using CuBlas
 * 
 * Performs the operation : C = A * B
 * 
 * - 'A' and 'B' are two square matrices with size NxN;
 * - 'C' is the resulting matrix;
 * 
 * Kees Lemmens, Nov 2008
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

/* Includes, cuda */
#include "cublas.h"

//#define DEBUG 2
//#define DEBUG 1

float *initMatrix(int n, int m)
{
   float *ptr;
   
   ptr = (float  *) calloc(n * m, sizeof(float)); // rows x columns
   
   // Note that it is not possible to use PL memory ( cudaMallocHost() ) with CuBlas 
   // This compiles but results in segmentation faults 
   // Kees Lemmens, Feb 2010 (cudua 2.3)

   if (ptr == NULL)
   {
      fprintf(stderr,"Malloc for matrix failed !\n");
      exit(1);
   }
   
   return ptr;
}

// Note that we actually fill the TRANSPOSED matrix here
// as BLAS is Fortran based !!! 
void fillMatrix(float *a, int n, int m, int offset)
{  int x,y;
   
   for(x=0; x<n; x++)
     for(y=0; y<m; y++)
       a[y*n + x] = (float) x+y + offset;
}

// Note that we actually show the TRANSPOSED matrix here
// as BLAS is Fortran based !!! 
void showMatrix(char *name, float *a, int n, int m)
{ 
#if (DEBUG > 0)
   int x,y;
   
   for(x=0; x<n; x++)
   {
# if (DEBUG < 2)
   y = m - 1;
# else
      for(y=0; y<m; y++)
# endif
	printf("%s[%02u][%02u]=%6.2f  ",name,x,y,a[y*n + x]);
      printf("\n");
   }
#endif
}

void checkCublasStatus(cublasStatus status, char *error)
{
   if (status != CUBLAS_STATUS_SUCCESS)
   {
      fprintf (stderr, "Cuda CUBLAS : %s\n",error);
      exit(EXIT_FAILURE);
   }
}

float *initCublasMatrix(int n, int m)
{
   float *ptr;
   cublasStatus status;
   
   status = cublasAlloc(n*m, sizeof(float), (void**)&ptr);
   checkCublasStatus(status,"Malloc for matrix on device failed !");
   
   return ptr;
}

void copytoCublasMatrix(float *d_a, float *h_a, int n, int m)
{
   cublasStatus status;
   
   status = cublasSetVector(n*m, sizeof(float), h_a, 1, d_a, 1);
   checkCublasStatus(status," Copy to device failed !");
}

void copyfromCublasMatrix(float *h_a, float *d_a, int n, int m)
{
   cublasStatus status;
   
   status = cublasGetVector(n*m, sizeof(float), d_a, 1, h_a, 1);
   checkCublasStatus(status,"Copy from device failed !");
}

void freeCublas(float *a)
{
   cublasStatus status;
   
   status = cublasFree(a);
   checkCublasStatus(status,"Memory free error !");
}

int main(int argc, char** argv)
{
   struct timeval ti1,ti2,ti3,ti4;
   double runtime;
   cublasStatus status;
   int dim = 4;
   
   float *h_A,     *h_B,     *h_C;
   float *d_A = 0, *d_B = 0, *d_C = 0;
   
   if(argc >=2 )
     sscanf(argv[1],"%ld",&dim);
   
   status = cublasInit();
   checkCublasStatus(status,"Init has failed !");
   
   fprintf(stderr,"Matrix product with dim = %d\n",dim);
      
   /* Allocate host memory for the matrices */
   h_A = initMatrix(dim,dim);
   h_B = initMatrix(dim,dim);
   h_C = initMatrix(dim,dim);
   
   fillMatrix(h_A,dim,dim, 0);
   fillMatrix(h_B,dim,dim, 10);
   
   /* Allocate device memory for the matrices */
   d_A = initCublasMatrix(dim,dim);
   d_B = initCublasMatrix(dim,dim);
   d_C = initCublasMatrix(dim,dim);
   
   gettimeofday(&ti1,NULL);        /* read starttime in t1 */
   
   /* Initialize the device matrices with the host matrices */
   copytoCublasMatrix(d_A,h_A,dim,dim);
   copytoCublasMatrix(d_B,h_B,dim,dim);
   
   /* Clear last error */
   cublasGetError();
   
   /* Performs operation using cublas */
   
   gettimeofday(&ti2,NULL);
   
   cublasSgemm('n', 'n', dim, dim, dim, 1.0f, d_A, dim, d_B, dim, 0.0f, d_C, dim);
   status = cublasGetError();
   checkCublasStatus(status,"Kernel execution error !");

   cudaThreadSynchronize(); /* Make sure all threads finished before stopping clock */
   
   gettimeofday(&ti3,NULL);
   
   /* Read the result back */
   copyfromCublasMatrix(h_C,d_C,dim,dim);
   
   gettimeofday(&ti4,NULL);
   
   showMatrix("A",h_A,dim,dim);
   showMatrix("B",h_B,dim,dim);
   
   showMatrix("C",h_C,dim,dim);
   
   /* Memory clean up */
   free(h_A);
   free(h_B);
   free(h_C);
   
   freeCublas(d_A);
   freeCublas(d_B);
   freeCublas(d_C);
   
   status = cublasShutdown();
   checkCublasStatus(status,"Shutdown error !");
   
   fflush(stderr);
   runtime = (ti3.tv_sec - ti2.tv_sec) + 1e-6*(ti3.tv_usec - ti2.tv_usec);
   fprintf(stderr,"\nCublas : run time only for Sgemm = %f secs.\n",runtime);
  
   // This goes wrong if using single precision floats on some platforms. Any idea why ?
   // (remember that Unix starts counting seconds from Jan 1, 1970 !)
   // runtime = (ti4.tv_sec + 1e-6*ti4.tv_usec) - (ti1.tv_sec + 1e-6*ti1.tv_usec);

   runtime = (ti4.tv_sec - ti1.tv_sec) + 1e-6*(ti4.tv_usec - ti1.tv_usec);
   fprintf(stderr,"\nCublas : run time including data transfers = %f secs.\n",runtime);
   
   return EXIT_SUCCESS;
}
