/* Some common routines for allocating matrices,
 * filling them with some data and printing.
 * 
 * Kees Lemmens, June 2009
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "cuda.h"

void fillMatrix(float *a, int n, int m, int offset)
{  long x,y;
   
   for(y=0; y<m; y++)
     for(x=0; x<n; x++)
       a[y*n + x] = (float) (x + y + offset);
}

void showMatrix(char *name, float *a, int n, int m)
{ 
#if (DEBUG > 0)
   long x,y;
   
   for(y=0; y<m; y++)
   {
# if (DEBUG < 2)
      x = n - 1;
# else
      for(x=0; x<n; x++)
# endif
        printf("%s[%02u][%02u]=%6.2f  ",name,y,x,a[y*n + x]);
      printf("\n");
   }
#endif
}

void checkCudaError(cudaError_t status, char *error)
{
   if (status != cudaSuccess)
   {
      fprintf (stderr, "Cuda : %s\n",error);
      exit(EXIT_FAILURE);
   }
}

float *initHostMatrix(int n, int m)
{
   float *ptr;
   cudaError_t status;

   status = cudaMallocHost((void**)&ptr, n * m * sizeof(float)); // rows x columns
   checkCudaError(status,"Malloc for matrix on host failed !");

   return ptr;
}

float *initCudaMatrix(int n, int m)
{
   float *ptr;
   cudaError_t status;

   status = cudaMalloc((void**)&ptr, n * m * sizeof(float));
   checkCudaError(status,"Malloc for matrix on device failed !");

   return ptr;
}

void copytoCudaMatrix(float *d_a, float *h_a, int n, int m)
{
   cudaError_t status;
   
   status = cudaMemcpy(d_a, h_a, n * m * sizeof(float), cudaMemcpyHostToDevice);
   checkCudaError(status," Copy to device failed !");
}

void copyfromCudaMatrix(float *h_a, float *d_a, int n, int m)
{
   cudaError_t status;
   
   status = cudaMemcpy(h_a, d_a, n * m * sizeof(float), cudaMemcpyDeviceToHost);
   checkCudaError(status,"Copy from device failed !");
}

void freeCudaHost(float *a)
{
   cudaError_t status;
   
   status = cudaFreeHost(a);
   checkCudaError(status,"Memory free error !");
}

void freeCudaDevice(float *a)
{
   cudaError_t status;
   
   status = cudaFree(a);
   checkCudaError(status,"Memory free error !");
}

