/* Simple matrix Multiplication example using CuDa and 1D matrices on the host
 * 
 * Performs the operation : C = A * B
 * 
 * - 'A' and 'B' are two square matrices with size NxN;
 * - 'C' is the resulting matrix;
 * 
 * Kees Lemmens, Nov 2008
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

/* Includes, cuda */
#include "cuda.h"

//#define DEBUG 2

#define MAXBLOCKSIZE 16

/* These are simple routines stored in a separate source file as they are not really
 * important for understanding this example. */ 

extern void fillMatrix(float *a, int n, int m, int offset);
extern void showMatrix(char *name, float *a, int n, int m);
extern void checkCudaError(cudaError_t status, char *error);
extern float *initHostMatrix(int n, int m);
extern float *initCudaMatrix(int n, int m);
extern void copytoCudaMatrix(float *d_a, float *h_a, int n, int m);
extern void copyfromCudaMatrix(float *h_a, float *d_a, int n, int m);
extern void freeCudaHost(float *a);
extern void freeCudaDevice(float *a);

// Just an example of a GPU device function called from the kernel :
__device__ float mult(float a, float b)
{
   return a * b;
}

__global__ void d_oddloop(float *d_A, float *d_B, float *d_C,
			int N, int blockSize)
{
   float Cs;
   int x,y,z;
   
   // Each block computes one horizontal strip of height blockSize, and each
   // thread in a block computes the product for only one single row.

#if (DEBUG >= 2)
   fprintf(stderr,"thread Index %d, block Index %d\n",blockIdx.x,threadIdx.x);
#endif
   
   y = (blockIdx.x * blockSize) + threadIdx.x; // column index

   for(x=1; x < N; x+=2) // walk over a complete row
   {
      Cs = 0; // running total
      for(z=0; z < N; z++) // walk over a complete column
      {
	 Cs += mult(d_A[y * N + z], d_B[z * N + x]);
      }
      
      // Compute block and thread offset and then store element in global memory :
      d_C[y * N + x] = Cs;
   }
}

__global__ void d_evenloop(float *d_A, float *d_B, float *d_C,
			 int N, int blockSize)
{
   float Cs;
   int x,y,z;

   // Each block computes one horizontal strip of height blockSize, and each
   // thread in a block computes the product for only one single row.

#if (DEBUG >= 2)
   fprintf(stderr,"thread Index %d, block Index %d\n",blockIdx.x,threadIdx.x);
#endif
   
   y = (blockIdx.x * blockSize) + threadIdx.x; // column index

   for(x=0; x < N; x+=2) // walk over a complete row
   {
      Cs = 0; // running total
      for(z=0; z < N; z++) // walk over a complete column
      {
	 Cs += mult(d_A[y * N + z], d_B[z * N + x]);
      }
      
      // Compute block and thread offset and then store element in global memory :
      d_C[y * N + x] = Cs;
   }
}

void matrixProdCuda(float *d_A, float *d_B, float *d_C, int N, int blockSize)
{
   cudaError_t status;
   int nBlocks;
   
   nBlocks = N/blockSize; // use strips of N * N/blockSize for each thread
   
   d_evenloop <<<nBlocks, blockSize>>> (d_A, d_B, d_C, N, blockSize);
   d_oddloop  <<<nBlocks, blockSize>>> (d_A, d_B, d_C, N, blockSize);
   
   status = cudaGetLastError();
   checkCudaError(status,"Kernel execution error !");
}

int main(int argc, char** argv)
{    
   struct timeval ti1,ti2;
   double runtime;
   int N = 4;
   float *h_A,     *h_B,     *h_C;
   float *d_A = 0, *d_B = 0, *d_C = 0;
   int blockSize;

   if(argc >=2 )
     sscanf(argv[1],"%ld",&N);

   blockSize = (N > MAXBLOCKSIZE ? MAXBLOCKSIZE : (N/4)*4);
   fprintf(stderr,"Matrix product with N = %d, blocksize = %d\n",N,blockSize);
      
   gettimeofday(&ti1,NULL);/* read starttime in t1 */

   /* Allocate host memory for the matrices */
   h_A = initHostMatrix(N,N);
   h_B = initHostMatrix(N,N);
   h_C = initHostMatrix(N,N);
   
   fillMatrix(h_A,N,N, 0);
   fillMatrix(h_B,N,N, 10);
   
   /* Allocate device memory for the matrices */
   d_A = initCudaMatrix(N,N);
   d_B = initCudaMatrix(N,N);
   d_C = initCudaMatrix(N,N);
   
   /* Initialize the device matrices with the host matrices */
   copytoCudaMatrix(d_A,h_A,N,N);
   copytoCudaMatrix(d_B,h_B,N,N);
   
   /* Clear last error */
   cudaGetLastError();
   
   /* Performs operation using Cuda kernel above */
   matrixProdCuda(d_A, d_B, d_C, N, blockSize);
   
   /* Read the result back */
   copyfromCudaMatrix(h_C,d_C,N,N);
   
   gettimeofday(&ti2,NULL); /* read endtime in t2 */
   
   // showMatrix("A",h_A,N,N);
   // showMatrix("B",h_B,N,N);

   showMatrix("C",h_C,N,N);
   
   /* Memory clean up */
   freeCudaHost(h_A);
   freeCudaHost(h_B);
   freeCudaHost(h_C);
   
   freeCudaDevice(d_A);
   freeCudaDevice(d_B);
   freeCudaDevice(d_C);
   
   runtime = (ti2.tv_sec - ti1.tv_sec ) + 1e-6*(ti2.tv_usec - ti1.tv_usec);

   fflush(stderr);
   fprintf(stderr,"\nCuda : run time = %f secs.\n",runtime);
   
   return EXIT_SUCCESS;
}
