// multiply2.cu : Simple scalar-vector product (similar to the MPI example)
// Computes blockSize elements in a single block and use N/blockSize blocks and
// of course blockSize threads per block.
//
// Kees Lemmens, Mar 2009
//

#include <stdio.h>
#include <sys/time.h>
#include <cuda.h>

// This runs on the CPU :
float multiplyOnHost(float *a, long N)
{
   long i;
   struct timeval ti1,ti2;
   float runtime;
   
   fprintf(stderr,"Now computing on CPU : \n");
   gettimeofday(&ti1,NULL);        // read starttime into t1
   
   for(i=0;i<N;i++)
     a[i] *= 3;
   
   gettimeofday(&ti2,NULL);        // read endtime into t2
   runtime = (ti2.tv_sec - ti1.tv_sec ) + 1e-6*(ti2.tv_usec - ti1.tv_usec);
   fprintf(stderr,"Run time CPU : %f microsecs.\n",runtime * 1e6);
   
   return runtime;
}

// This is the kernel that runs on the GPU :
__global__ void multiplyKernel(float *b)
{
   int i = blockIdx.x * blockDim.x + threadIdx.x;
   b[i] *= 3;

   // this only works on arch 2.x and up (but other archs could use cuPrintf)
#if __CUDA_ARCH__ >= 200  // e.g. Fermi C2050 or C2070
   fprintf(stderr,"i=%d\n",i);
#endif
}

// This part also runs on the CPU (and not on the GPU !) :
float multiplyOnDevice(float *b_h, long N, int blockSize)
{
   float *a_d;   // Pointer to device array
   struct timeval ti1,ti2;
   int nBlocks;
   float runtime;
   
   fprintf(stderr,"Now computing on GPU : \n");
   gettimeofday(&ti1,NULL);        // read starttime into t1
   
   cudaMalloc((void **) &a_d, N * sizeof(float));   // Allocate array on device
   cudaMemcpy(a_d, b_h, N * sizeof(float), cudaMemcpyHostToDevice);
   
   // Do the actual calculation on N/blockSize blocks (and N threads) inside the GPU device:
   if(blockSize > N)
     blockSize = N;
   nBlocks = N/blockSize + (N%blockSize == 0 ? 0:1);
   if(nBlocks >= 65535)
   {
      fprintf(stderr,"Maximum number of blocks exceeded : %d >= 65535 !\n", nBlocks);
      exit(1);
   }
   multiplyKernel <<< nBlocks, blockSize >>> (a_d);
   cudaThreadSynchronize(); // Make sure all threads finished before stopping clock
   
   // Retrieve result from device and store it in host array
   cudaMemcpy(b_h, a_d, sizeof(float)*N, cudaMemcpyDeviceToHost);
   
   cudaFree(a_d);

   gettimeofday(&ti2,NULL);        // read endtime into t2
   runtime = (ti2.tv_sec - ti1.tv_sec ) + 1e-6*(ti2.tv_usec - ti1.tv_usec);
   fprintf(stderr,"Run time GPU (using %d blocks and %d threads/block ) : %f microsecs.\n",
	   nBlocks, blockSize, runtime * 1e6);
   
   return runtime;
}

// main routine that executes on the host
int main(int argc, char **argv)
{
   float *a_h,*b_h;      // Pointers to host arrays
   long N = 4096L;       // Number of elements in arrays (problemsize)
   int blockSize = 256;  // Number of threads in one single block (max device dependent)
   long i;
   float ts,tp;
   
   if(argc >=2 )
     sscanf(argv[1],"%ld",&N);
   
   fprintf(stderr,"Starting with N=%d and threads/block=%d ...\n",N,blockSize);
   
   // Allocate arrays on host
   a_h = (float *)malloc(N * sizeof(float));
   b_h = (float *)malloc(N * sizeof(float));

   // Initialize host arrays 
   for (i=0; i<N; i++)
     b_h[i] = a_h[i] = (float)i;
   
   // Run sequential algorithm on CPU
   ts = multiplyOnHost(a_h, N);
   
   // Run parallel algorithm on GPU
   tp = multiplyOnDevice(b_h, N, blockSize);
   
   // check results
#if (DEBUG > 0)
   for (i=0; i<N; i++)
   {
      float f = fabs(a_h[i] - b_h[i]);
      printf("CPU: %f , GPU: %f , diff: %f\n",a_h[i],b_h[i],f);
   }
#endif
   
   printf("Ratio execution time GPU/CPU: %f\n",tp/ts);
   
   free(a_h);
   exit(0);
}

