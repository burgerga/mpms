// Simple starting example for CUDA program
// Kees Lemmens, last change Feb 2011

#include <stdio.h>
#include <cuda.h>

#include "cuPrintf.cu"

#define TEST "Hello World !!!"
#define NRTPBK  4 // size of string TEST+1 / NRBLKS
#define NRBLKS  4 // Note : NRTPBK * NRBLKS = strlen(TEST)

void checkCudaError(cudaError_t status, char *error)
{
   if (status != cudaSuccess)
   {
      fprintf (stderr, "Cuda : %s\n",error);
      exit(EXIT_FAILURE);
   }
}

__global__ void routineOnGPU(char *string_d)
{
   char string_k[] = TEST;
   int myid = blockIdx.x * blockDim.x + threadIdx.x;
   
   // Each thread copies it's own char from local to global memory :
   string_d[myid] = string_k[myid];

   cuPrintf("My character is : %c\n",string_k[myid]);
}

int main(void)
{
   cudaError_t status; // for errors on the GPU
   char *string_h;     // pointer to host   memory
   char *string_d;     // pointer to device memory
   int size = strlen(TEST) + 1;
   
   cudaPrintfInit();   // Init kernel printf debugging
   
   // allocate arrays on host and on device
   string_h = (char *)calloc(size,1); // empty string
   status = cudaMalloc((void **) &string_d, size);
   checkCudaError(status,"Malloc on GPU device failed !");
   
   // clear string_d on GPU device by copying empty string_h over it
   status = cudaMemcpy(string_d, string_h, size, cudaMemcpyHostToDevice);
   checkCudaError(status,"Sending to GPU device failed !");
   
   // Start a simple GPU kernel on NRTPBK threads and NRBLKS blocks
   routineOnGPU <<< NRBLKS, NRTPBK >>> (string_d); 
   cudaPrintfDisplay(stdout, true); // Print the cuPrintf buffer !
   
   // retrieve data from GPU device: string_d to string_h
   cudaMemcpy(string_h, string_d, size, cudaMemcpyDeviceToHost);
   checkCudaError(status,"Receiving from GPU device failed !");
   
   printf("%s\n",string_h);
   
   // cleanup memory
   cudaFree(string_d);
   free(string_h);
   
   cudaPrintfEnd(); // Exit kernel printf debugging
}
