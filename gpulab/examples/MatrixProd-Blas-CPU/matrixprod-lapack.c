#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sys/time.h>

//#include "f2c.h"
//#include "clapack.h"

typedef int integer;
typedef float real;

//#define DEBUG 2  // Show all columns
//#define DEBUG 1  // Show only last column
#define DEBUG 0

extern void sgemm_(char *a,char *b, integer *dimn, integer *dimm, integer *dimk, real *alpha,
		   real *A, integer *dimA, real *B, integer *dimB, real *beta, real *C, integer *dimC);

void simple_sgemmsqr(real *A, real *B, real *C, integer dim)
{
   real alpha = 1.0f, beta = 0.0f;
   char c = 'N';
   /*
    SUBROUTINE SGEMM(TRANSA,TRANSB,M,N,K,ALPHA,A,LDA,B,LDB,BETA,C,LDC)
    REAL ALPHA,BETA
    INTEGER K,LDA,LDB,LDC,M,N
    CHARACTER TRANSA,TRANSB
    REAL A(LDA,*),B(LDB,*),C(LDC,*)
    */
   sgemm_(&c, &c, &dim, &dim, &dim, &alpha, A, &dim, B, &dim, &beta, C, &dim);
}

real *initMatrix(int n, int m)
{
   real *ptr;
   
   ptr = (real  *) calloc(n * m, sizeof(real)); // rows x columns
   
   if (ptr == NULL)
   {
      fprintf(stderr,"Malloc for matrix failed !\n");
      exit(1);
   }
   
   return ptr;
}

// Note that we actually fill the TRANSPOSED matrix here
// as BLAS is Fortran based !!!
void fillMatrix(real *a, int n, int m, int offset)
{  int x,y;
 
   for(x=0; x<n; x++)
     for(y=0; y<m; y++)
       a[y*n + x] = (real) x+y + offset;
}

// Note that we actually show the TRANSPOSED matrix here
// // as BLAS is Fortran based !!!
void showMatrix(char *name, real *a, int n, int m)
{
#if (DEBUG > 0)
   int x,y;
   
   for(x=0; x<n; x++)
   {
# if (DEBUG < 2)
      y = m - 1;
# else
      for(y=0; y<m; y++)
# endif
	printf("%s[%02u][%02u]=%6.2f  ",name,x,y,a[y*n + x]);
      printf("\n");
   }
#endif
}

int main(int argc, char *argv[])
{
   real  *A, *B, *C;
   integer dim = 4;
   
   struct timeval ti1,ti2;
   real runtime;
   
   if(argc >=2 )
     sscanf(argv[1],"%d",(int *)&dim);
   
   fprintf(stderr,"Matrix product with dim = %d\n",(int)dim);
   
   gettimeofday(&ti1,NULL); /* read starttime in t1 */
   
   A = initMatrix(dim,dim);
   B = initMatrix(dim,dim);
   C = initMatrix(dim,dim);
   
   fillMatrix(A,dim,dim,0);
   fillMatrix(B,dim,dim,10);

   simple_sgemmsqr(A, B, C, dim);

   gettimeofday(&ti2,NULL); /* read endtime in t2 */
   
   showMatrix("A",A,dim,dim);
   showMatrix("B",B,dim,dim);
   showMatrix("C",C,dim,dim);
   
   runtime = (ti2.tv_sec - ti1.tv_sec ) + 1e-6*(ti2.tv_usec - ti1.tv_usec);
   
   fflush(stderr);
   fprintf(stderr,"\nSequential : run time = %f secs.\n",runtime);
   
   return 0;  
}

