/************************************************* 
 * 
 * Program: qsort.c
 * 
 * This program uses quicksort with a subroutine of insertion sort on
 * small subfiles of size k=10. In order to compute the constant c
 * for which this is a c n log(n) algorithm, we run the sorting
 * routine on files of N,2N,...,NUM*N random integers, computing the
 * CPU time in milliseconds.
 * 
 *************************************************/ 
 
 
#include <stdio.h> 
#include <stdlib.h>
#include <malloc.h>
#include <math.h>       /* header for rand()         */
#include <time.h>       /* header to clock CPU stats */
/* number per second returned by clock()  CLOCKS_PER_SEC */

#define NUM 500
#define N 50
/* Size of file to insertsort ranges from N,2N,3N,...,NUM*N */

int main()
{
   int i,j;
   double ms; /* milliseconds  of CPU */
   int *a;
   clock_t starttime, endtime;
   /* a[] is repeatedly initialized with i*N many random integers,
    * then sorted using insertion sort and timed, for i taking
    * the values N,2N,...,NUM*N.  */
   
   void display(int a[],int size); /* used for debugging purposes */
   void insertionsort(int a[], int first, int last);
   void quicksort(int a[], int first, int last, int k);
   /* function prototypes */
   
   
   a = (int *) calloc(N*NUM,sizeof(int));
   printf("Insertionsort statistics\n");
   printf("Filesize\t\tCPU time in ms\n");
   for (i=1;i<=NUM;++i)
   {
      for (j=0;j<i*N;++j) a[j] = rand();
      starttime = clock();
      quicksort(a,0,i*N-1,10);
      endtime = clock();
      ms = (double)(endtime - starttime)/(double)CLOCKS_PER_SEC * 100.0;
      printf("%d\t\t%3.6lf\n",i*N,ms);
   }
   exit(0);
}

void display(int a[],int size)
{
   int i;
   for (i=0;i<size;++i) printf("%d\n",a[i]);
}

void insertionsort (int a[], int first, int last) 
{ 
   int i,j,x;
   if (last <= first) return; 
   for (i = first+1; i <= last; i++) 
   { 
      x = a[i]; 
      j = i; 
      while (j > first && a[j-1] > x) 
      { 
	 a[j] = a[j-1]; 
	 j--; 
      } 
      a[j] = x; 
   } 
} 

void quicksort(int a[], int i, int j, int m)
{
   /* sort a[i] to a[j] inclusive */
   
   /* insertionsort called for files of m or less elements */
   
   int k; 
   int partition(int a[], int first, int last);
   void insertionsort(int a[], int first, int last);
   
   if ( j-i+1 <= m )
   {
      insertionsort(a,i,j);
      return;
   }
   k = partition (a, i, j);   /* k is  position of pe */ 
   quicksort(a, i, k-1, m);      /* sort left sub-array  */
   quicksort(a, k+1, j, m);      /* sort right sub-array */
} 

int partition(int a[], int low, int high) 
{
   /* partition a[low] through a[high]  */ 
   register int pe, i, j, pivot_index;
   void exchange( int b[], int first, int last);  /* function prototype */
   i = low; 
   j = high;
   pivot_index = rand() % (j - i + 1) + i;
   /* choose random element as partition pivot */
   exchange(a, pivot_index, j);
   pe = a[j];    /* move pivot to right end */ 
   while (i < j) 
   {    while (i < j && a[i] <= pe) i++; 
      while (i < j && a[j] >= pe) j--; 
      if (i < j) exchange(a, i, j); 
   } 
   if (i != high) 
     exchange(a, i, high);  /* pe to partition location */ 
   return(i);    /* return index of partition element      */ 
} 

void exchange(int b[], int i, int j) 
{ /*  array b is modified */ 
   int t; 
   t = b[j]; b[j] = b[i]; b[i] = t; 
} 
