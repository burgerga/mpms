/* Simple 1D FFT example using Fftw */
/* Kees Lemmens, Aug 2009 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/time.h>
#include <math.h>

/* Includes */
#include "fftw3.h"

/* Matrix size */
//#define N 1024
#define BATCH 1
#define DEBUG 1

//#define N 2048
//#define DEBUG 1


void ldebug(int level, char *format, ...)
{
   va_list p;
   
   if (DEBUG < level)
     return;
   va_start(p,format);
   vfprintf(stderr,format, p);
   va_end(p);
}

char *readLine(FILE *in,char *name,char *buffer,int n)
{
   if(fgets(buffer,n,in) == NULL)
   {
      fprintf(stderr,"Inputfile %s too short !!\n",name);
      exit(1);
   }
   else
     return buffer;
}

int readdim(int argc, char *argv[])
{
   ldebug(1,"readdim entered ...\n");
   
   if(argc <= 1)
   {
      printf("Please supply a dimension.\n");
      exit(1);
   }
   
   return atoi(argv[1]);
}

int fillCVector(fftw_complex *a,int n,int argc, char *argv[])
{
   FILE *in;
   char buffer[100]="";
   int i;

   ldebug(1,"fillCVector entered ...\n");
   
   if(argc <= 1)
   {
      printf("Please supply a filename.\n");
      exit(1);
   }
   
   if((in = fopen(argv[1],"r")) == NULL)
   {
      fprintf(stderr,"Open of inputfile %s failed !!\n",argv[1]);
      exit(1);
   }
   
   for(i=0;i<n;i++)
   {
      do
      {
	 readLine(in,argv[1],buffer,100);
      }
      while( strlen(buffer) < 3); // skip emtpy lines
      sscanf(buffer,"%lf",&a[i][0]);
      a[i][1] = 0.0; // imaginary part
   }
   fclose(in);
   return 0;
}

fftw_complex *initCVector(int n)
{
   fftw_complex *ptr;
   
   ldebug(1,"initCVector entered ...\n");
   
   ptr = (fftw_complex *) calloc(n, sizeof(fftw_complex));
   
   if(ptr == NULL)
   {
      fprintf(stderr,"Malloc for complex vector on host failed !\n");
      exit(1);
   }
   return ptr;
}

void showCVector(char *name, fftw_complex *a, int n)
{  int i;
   
   for(i=0; i<n; i++)
   {
//    printf("%s[%02u]=%E +i%E \n",name,i,a[i][0],a[i][1]);
//    printf("%E %E\n",a[i][0],a[i][1]);
      printf("%E\n",sqrt(a[i][0] * a[i][0]  + a[i][1] * a[i][1]));
   }
}

void scaleCVector(fftw_complex *h_a, int n)
{
   int i;
   
   for(i=0;i<n;i++)
   {
      h_a[i][0] /= (float)n;
      h_a[i][1] /= (float)n;
   }
}

/* Main */
int main(int argc, char *argv[])
{  
   struct timeval ti1,ti2;
   double runtime;
   int dim;
   fftw_plan plan;
   
   fftw_complex *h_At;
   fftw_complex *h_Af;
   
   dim = readdim(argc--,argv++);
   
   fprintf(stderr,"\n");
   
   gettimeofday(&ti1,NULL);        /* read starttime in t1 */
   
   /* Allocate host memory for the vectors */
   h_At = initCVector(dim);
   h_Af = initCVector(dim);

   plan = fftw_plan_dft_1d(dim,h_At,h_Af,FFTW_FORWARD,FFTW_ESTIMATE);
   
   fillCVector(h_At,dim,argc,argv);
   
   /* Performs operation using cufft */
   ldebug(1,"fftw_execute starting ...\n");
   fftw_execute(plan);
   ldebug(1,"fftw_execute finished ...\n");
   
   /* And scale with dim if wanted as this is not done inside FFTW : */
   scaleCVector(h_Af,dim);
   
   gettimeofday(&ti2,NULL);        /* read endtime in t2 */
   
   showCVector("F",h_Af,dim);
   
   /* Memory clean up */
   free(h_At);
   free(h_Af);
   
   fftw_destroy_plan(plan);
   
   runtime = (ti2.tv_sec - ti1.tv_sec) + 1e-6*(ti2.tv_usec-ti1.tv_usec);

   fflush(stderr);
   fprintf(stderr,"\nFftw : run time = %f secs.\n",runtime);
   
   return EXIT_SUCCESS;
}
