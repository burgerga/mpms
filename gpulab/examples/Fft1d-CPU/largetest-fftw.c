/*
 Simple test to see how the FFT routines perform by running many of them
 after each other on the same plan.

 Doesn't use any input or output, only internally generated random data.
  
 Only argument is the size of the random vector to be created.
 Kees Lemmens, Aug 2009
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/time.h>

/* Includes */
#include "fftw3.h"

/* Matrix size */
//#define N 1024
#define BATCH 1
#define DEBUG 1

//#define N 2048
//#define DEBUG 1


void ldebug(int level, char *format, ...)
{
   va_list p;
   
   if (DEBUG < level)
     return;
   va_start(p,format);
   vfprintf(stderr,format, p);
   va_end(p);
}

int readdim(int argc, char *argv[])
{
   ldebug(1,"readdim entered ...\n");
   
   if(argc <= 1)
   {
      printf("Please supply a dimension.\n");
      exit(1);
   }
   
   return atoi(argv[1]);
}

int fillCVector(fftw_complex *a,int n)
{
   int i;

   ldebug(1,"fillCVector entered ...\n");
   
   for(i=0;i<n;i++)
   {
      a[i][0] = rand()/RAND_MAX; // real part
      a[i][1] = 0.0;             // imaginary part
   }
   return 0;
}

fftw_complex *initCVector(int n)
{
   fftw_complex *ptr;
   
   ldebug(1,"initCVector entered ...\n");
   
   ptr = (fftw_complex *) calloc(n, sizeof(fftw_complex));
   
   if(ptr == NULL)
   {
      fprintf(stderr,"Malloc for complex vector failed !\n");
      exit(1);
   }
   return ptr;
}

/* Main */
int main(int argc, char *argv[])
{    
   struct timeval ti1,ti2;
   double runtime;
   int dim;
   int x;
   int nrruns=50;
   fftw_plan plan;
   
   fftw_complex *h_At;
   fftw_complex *h_Af;
   
   dim = readdim(argc--,argv++);
   
   fprintf(stderr,"\n");
   
   gettimeofday(&ti1,NULL);        /* read starttime in t1 */
   
   /* Allocate host memory for the vectors */
   h_At = initCVector(dim);
   h_Af = initCVector(dim);

   plan = fftw_plan_dft_1d(dim,h_At,h_Af,FFTW_FORWARD,FFTW_ESTIMATE);
   
   fillCVector(h_At,dim);
   
   /* Do the actual Fourier transform */
   fftw_execute(plan);

   gettimeofday(&ti2,NULL);        /* read endtime in t2 */
   
   runtime = (ti2.tv_sec - ti1.tv_sec) + 1e-6*(ti2.tv_usec-ti1.tv_usec);

   fflush(stderr);
   fprintf(stderr,"\nFftw : run time for 1 run = %f secs.\n",runtime);

   gettimeofday(&ti1,NULL);        /* read starttime in t1 */
   
   /* Performs multiple operations using cufft */
   for(x=0;x<nrruns;x++)
     fftw_execute(plan);
   
   gettimeofday(&ti2,NULL);        /* read endtime in t2 */
   
   runtime = (ti2.tv_sec - ti1.tv_sec) + 1e-6*(ti2.tv_usec-ti1.tv_usec);
   
   fflush(stderr);
   fprintf(stderr,"\nFftw : run time for %d runs = %f secs (%f secs per run).\n",
	   nrruns,runtime,runtime/nrruns);
   
   ldebug(1,"Cleaning up memory ...\n");
   free(h_At);
   free(h_Af);
   
   ldebug(1,"Destroying plan ...\n");
   fftw_destroy_plan(plan);
   
   return EXIT_SUCCESS;
}
