/* Basic sparse matrix-dense vector Multiplication example using CuSparse (Cuda 3.2)
 *
 * Performs the operation :  As * xd = yd
 *
 * - 'As' is a sparse matrix with size NxN;
 * - 'xd' is a dense vector;
 * - 'yd' is a dense vector;
 *
 * Kees Lemmens, Oct 2010
*/

#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <cusparse.h>

#include "helpers.cu" // Some less important definitions and routines

/* Finally some routines to initialize the matrix A and the sparse and 
 * dense xs and xd vectors (that will be merged later) */
void initSparseHostMatrixA(cooSpMatrix * coospmh)
{
   coospmh->RowIndex[0]=0; coospmh->ColIndex[0]=0; coospmh->Val[0]=1.0;
   coospmh->RowIndex[1]=0; coospmh->ColIndex[1]=2; coospmh->Val[1]=2.0;
   coospmh->RowIndex[2]=0; coospmh->ColIndex[2]=3; coospmh->Val[2]=3.0;
   coospmh->RowIndex[3]=1; coospmh->ColIndex[3]=1; coospmh->Val[3]=4.0;
   coospmh->RowIndex[4]=2; coospmh->ColIndex[4]=0; coospmh->Val[4]=5.0;
   coospmh->RowIndex[5]=2; coospmh->ColIndex[5]=2; coospmh->Val[5]=6.0;
   coospmh->RowIndex[6]=2; coospmh->ColIndex[6]=3; coospmh->Val[6]=7.0;
   coospmh->RowIndex[7]=3; coospmh->ColIndex[7]=1; coospmh->Val[7]=8.0;
   coospmh->RowIndex[8]=3; coospmh->ColIndex[8]=3; coospmh->Val[8]=9.0;

   // print the sparse matrix
   printf("Input data:\n");
   for (int i=0; i<coospmh->n; i++)
   {
     printf("coosmh.RowIndex[%d]=%d  ",i,coospmh->RowIndex[i]);
     printf("coosmh.ColIndex[%d]=%d  ",i,coospmh->ColIndex[i]);
     printf("coosmh.Val[%d]=%f     \n",i,coospmh->Val[i]);
  }
}

void initDenseHostVectorx(dnsVector *dnsvh)
{
   dnsvh->Val[0] = 21.0;
   dnsvh->Val[1] = 20.0;
   dnsvh->Val[2] = 15.0;
   dnsvh->Val[3] = 40.0;
   
   //print the dense vector xdh
   for (int i=0; i<dnsvh->n; i++)
   {
      printf("xdh[%d]=%f\n",i,dnsvh->Val[i]);
   }
}

int main()
{
   cudaError_t cudaStat;
   cusparseStatus_t status;
   int    i;
   
   xdh.n = ydh.n = xdd.n = ydd.n = 4;
   coosmd.n = coosmh.n = 9;

   printf("Testing example sparse matrix-vector product\n");
   
   /* create the following sparse test matrix in COO format */
   /*
    |1.0     2.0 3.0|
    |    4.0        |
    |5.0     6.0 7.0|
    |    8.0     9.0| */
   
   allocSparseHostMatrix(&coosmh);
   initSparseHostMatrixA(&coosmh);
   
   /* create dense vectors xs and xd. */
   /*
    xdh.Val = [21.0 20.0 15.0 40.0] (dense) */
   
   allocDenseHostVector(&xdh);
   initDenseHostVectorx(&xdh);
   
   /* And create the result vector y */
   allocDenseHostVector(&ydh); // empty yet
   
   /* allocate GPU memory and copy the matrix and vectors to the GPU */
   
   allocSparseDeviceMatrix(&coosmd);
   copySparseMatrixToDevice(&coosmh, &coosmd);

   allocDenseDeviceVector(&xdd);
   copyDenseVectorToDevice(&xdh, &xdd);
   
   /* And again create the result vector y on the device */
   allocDenseDeviceVector(&ydd); // empty yet
   
   /* initialize cusparse library */
   status= cusparseCreate(&handle);
   if (status != CUSPARSE_STATUS_SUCCESS) {
      cleanup("CUSPARSE Library initialization failed");
      exit(EXIT_FAILURE);
   }
   /* create and setup matrix descriptor */
   status= cusparseCreateMatDescr(&descra);
   if (status != CUSPARSE_STATUS_SUCCESS) {
      cleanup("Matrix descriptor initialization failed");
      exit(EXIT_FAILURE);
   }
   cusparseSetMatType(descra,CUSPARSE_MATRIX_TYPE_GENERAL);
   cusparseSetMatIndexBase(descra,CUSPARSE_INDEX_BASE_ZERO);
   
   /* exercise conversion routines (convert matrix from COO 2 CSR format) */
   // Note we only convert the row index here as the rest stays unmodified
   // Also note that it needs 1 extra element as defined by CSR !
   cudaStat = cudaMalloc((void**)&csrRowPtr,(xdd.n+1)*sizeof(csrRowPtr[0]));
   if (cudaStat != cudaSuccess) {
      cleanup("Device malloc failed (csrRowPtr)");
      exit( EXIT_FAILURE);
   }
   status= cusparseXcoo2csr(handle,coosmd.RowIndex,coosmd.n,xdd.n,
			    csrRowPtr,CUSPARSE_INDEX_BASE_ZERO);
   if (status != CUSPARSE_STATUS_SUCCESS) {
      cleanup("Conversion from COO to CSR format failed");
      exit(EXIT_FAILURE);
   }
   //resulting csrRowPtr should be : csrRowPtr = [0 3 4 7 9]
   
   /* Level 2 routine example (csrmv) : sparse matrix-vector product */
   // Computes yd = 1.0 * matAs * xd + 0 * whatever = matAs * xd,
   // The result is stored in the vector yd

   status= cusparseScsrmv(handle,CUSPARSE_OPERATION_NON_TRANSPOSE, xdh.n, xdh.n, 1.0,
                           descra, coosmd.Val, csrRowPtr, coosmd.ColIndex, &xdd.Val[0],
                           0.0, &ydd.Val[0]);
   if (status != CUSPARSE_STATUS_SUCCESS) {
       cleanup("Matrix x vector multiplication failed");
       return EXIT_FAILURE;
    }
   // result of cusparseScsrmv should be : yd = [ 171 80 475 520 ]
   
   /* Now copy result from device (ydd) to host (ydh) and print it */
   
   cudaMemcpy(ydh.Val, ydd.Val, (size_t)(ydh.n*sizeof(float)), cudaMemcpyDeviceToHost);
   printf("Result of matrix vector product:\n");
   for (i=0; i<ydh.n; i++)
      printf("ydh.Val[%d]=%f\n",i,ydh.Val[i]);
   
   cleanup("Example finished without problems");
   return EXIT_SUCCESS;
}

