/* Basic sparse matrix-vector routines for examples using CuSparse (Cuda 3.2)
 *
 * Kees Lemmens, Feb 2011
*/

#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <cusparse.h>

/* Some datatypes to make using sparse matrices and vectors easier */
typedef struct
{
   int   n;
   int   *RowIndex;
   int   *ColIndex;
   float *Val;
} cooSpMatrix;

typedef struct
{
   int n;
   float *Val;
} dnsVector;

typedef struct
{
   int   n;
   int   *Index;
   float *Val;
} spVector;

/* Some global static variables (this is easier for the cleanup routine) */
static cooSpMatrix coosmh, coosmd;
static spVector    xsh, xsd;
static dnsVector   xdh, xdd, ydh, ydd;
static int*        csrRowPtr=0;
static cusparseHandle_t handle=0;
static cusparseMatDescr_t descra=0;

extern void cleanup(char *message);

/* Free routines for both host and device */
void freeSparseHostMatrix(cooSpMatrix * coospmh)
{
   free(coospmh->RowIndex);
   free(coospmh->ColIndex);
   free(coospmh->Val);
}

void freeDenseHostVector(dnsVector * dnsvh)
{
   free(dnsvh->Val);
}

void freeSparseHostVector(spVector * spvh)
{
   free(spvh->Index);
   free(spvh->Val);
}

void freeSparseDeviceMatrix(cooSpMatrix * coospmd)
{
   cudaFree(coospmd->RowIndex);
   cudaFree(coospmd->ColIndex);
   cudaFree(coospmd->Val);
}

void freeDenseDeviceVector(dnsVector * dnsvd)
{
   cudaFree(dnsvd->Val);
}

void freeSparseDeviceVector(spVector * spvd)
{
   cudaFree(spvd->Index);
   cudaFree(spvd->Val);
}

/* Memory allocate routines for both host and device */
void allocSparseHostMatrix(cooSpMatrix * coospmh)
{
   int n = coospmh->n;
   coospmh->RowIndex = (int *)   malloc(n * sizeof(int));
   coospmh->ColIndex = (int *)   malloc(n * sizeof(int));
   coospmh->Val      = (float *) malloc(n * sizeof(float));

   if ((!coospmh->RowIndex) || (!coospmh->ColIndex) || (!coospmh->Val))
   {
      cleanup("Host malloc for sparse matrix failed");
      exit( EXIT_FAILURE);
   }
}

void allocDenseHostVector(dnsVector *dnsvh)
{
   int n = dnsvh->n;
   dnsvh->Val = (float *)malloc(n *sizeof(float));

   if((!dnsvh->Val))
   {
     cleanup("Host malloc for dense vector failed");
      exit(EXIT_FAILURE);
   }
}

void allocSparseHostVector(spVector *spvh)
{
   int n = spvh->n;
   spvh->Index = (int *)   malloc(n * sizeof(int));
   spvh->Val   = (float *) malloc(n * sizeof(float));
   
   if((!spvh->Index) || (!spvh->Val))
   {
      cleanup("Host malloc for sparse vector failed");
      exit(EXIT_FAILURE);
   }
}

void allocSparseDeviceMatrix(cooSpMatrix * coospmd)
{
   cudaError_t cudaStat1,cudaStat2,cudaStat3;
   int n = coospmd->n;
   
   cudaStat1 = cudaMalloc((void**)&coospmd->RowIndex,n*sizeof(int));
   cudaStat2 = cudaMalloc((void**)&coospmd->ColIndex,n*sizeof(int));
   cudaStat3 = cudaMalloc((void**)&coospmd->Val,  n*sizeof(float));

   if ((cudaStat1 != cudaSuccess) ||
       (cudaStat2 != cudaSuccess) ||
       (cudaStat3 != cudaSuccess))
   {
      cleanup("Device malloc for sparse matrix failed");
      exit( EXIT_FAILURE);
   }
}

void allocDenseDeviceVector(dnsVector *dnsvd)
{
   cudaError_t cudaStat1;
   int n = dnsvd->n;
   
   cudaStat1 = cudaMalloc((void**)&dnsvd->Val,n*sizeof(float));

   if(cudaStat1 != cudaSuccess)
   {
     cleanup("Device malloc for dense vector failed");
      exit(EXIT_FAILURE);
   }
}

void allocSparseDeviceVector(spVector *spvd)
{
   cudaError_t cudaStat1,cudaStat2;
   int n = spvd->n;
   
   cudaStat1 = cudaMalloc((void**)&spvd->Index,n*sizeof(int));
   cudaStat2 = cudaMalloc((void**)&spvd->Val,n*sizeof(float));
   
   if ((cudaStat1 != cudaSuccess) ||
       (cudaStat2 != cudaSuccess))
   {
      cleanup("Device malloc for sparse vector failed");
      exit( EXIT_FAILURE);
   }
}

/* Some routines to copy vectors and matrices from the host to the device */
void copySparseMatrixToDevice(cooSpMatrix *coospmh, cooSpMatrix * coospmd)
{
   cudaError_t cudaStat1,cudaStat2,cudaStat3;
   int n = coospmh->n;
   
   cudaStat1 = cudaMemcpy(coospmd->RowIndex, coospmh->RowIndex,
			  (size_t)(n*sizeof(int)),   cudaMemcpyHostToDevice);
   cudaStat2 = cudaMemcpy(coospmd->ColIndex, coospmh->ColIndex,
			  (size_t)(n*sizeof(int)),   cudaMemcpyHostToDevice);
   cudaStat3 = cudaMemcpy(coospmd->Val,      coospmh->Val,
			  (size_t)(n*sizeof(float)), cudaMemcpyHostToDevice);
   
   if ((cudaStat1 != cudaSuccess) ||
       (cudaStat2 != cudaSuccess) ||
       (cudaStat3 != cudaSuccess))
   {
      cleanup("Memcpy sparse matrix from Host to Device failed");
      exit(EXIT_FAILURE);
   }
}

void copyDenseVectorToDevice(dnsVector *dnsvh, dnsVector * dnsvd)
{
   cudaError_t cudaStat;
   int n = dnsvh->n;
   
   cudaStat = cudaMemcpy(dnsvd->Val, dnsvh->Val,
			 (size_t)(n*sizeof(float)),cudaMemcpyHostToDevice);
   
   if ((cudaStat != cudaSuccess))
   {
      cleanup("Memcpy dense vector from Host to Device failed");
      exit(EXIT_FAILURE);
   }
}

void copySparseVectorToDevice(spVector *spvh, spVector *spvd)
{
   cudaError_t cudaStat1,cudaStat2;
   int n = spvh->n;
   
   cudaStat1 = cudaMemcpy(spvd->Index,  spvh->Index,
			  (size_t)(n*sizeof(int)),cudaMemcpyHostToDevice);
   cudaStat2 = cudaMemcpy(spvd->Val,    spvh->Val,
			  (size_t)(n*sizeof(float)),cudaMemcpyHostToDevice);
   
   if ((cudaStat1 != cudaSuccess) ||
       (cudaStat2 != cudaSuccess))
   {
      cleanup("Memcpy sparse vector from Host to Device failed");
      exit(EXIT_FAILURE);
   }
}

/* Clear all static memory allocating variables before exiting */
void cleanup(char *message)
{
   do
   {
      printf ("%s\n", message);
      /* Host variables */
      if (ydh.Val)            freeDenseHostVector(&ydh);
      if (xdh.Val)            freeDenseHostVector(&xdh);
      if (xsh.Index)          freeSparseHostVector(&xsh);
      if (coosmh.RowIndex)    freeSparseHostMatrix(&coosmh);
      /* Device variables */
      if (xdd.Val)            freeDenseDeviceVector(&xdd);
      if (ydd.Val)            freeDenseDeviceVector(&ydd);
      if (xsd.Val)            freeSparseDeviceVector(&xsd);
      if (coosmd.Val)         freeSparseDeviceMatrix(&coosmd);
      /* Misc */
      if (csrRowPtr)          cudaFree(csrRowPtr);
      if (handle)             cusparseDestroy(handle);
      fflush (stdout);
   } while (0);
}

