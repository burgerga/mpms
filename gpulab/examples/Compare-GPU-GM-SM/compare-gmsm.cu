/* 
 * Performs a test of the latencies for computations carried out directly
 * in global device memory versus computations in shared memory inside a
 * multiprocessor, where the global memory contents is first copied to
 * shared memory before carrying out the computation.
 * 
 * Note that the shared memory space - although much faster - is very limited !
 * It should be treated as a kind of "cache"
 *
 * WARNING Jan 2011 : On newer architectures (>=1.3) the global memory is by default
 * cached on the the multiprocessor ! This implies that on these architectures the
 * speed difference between global and shared is a lot less than for older architectures !
 * 
 * Kees Lemmens, June 2009, September 2010
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "cuda.h"

#define N 4000    // Maximum amount we can approx. create in shared processor memory with 4 bytes/float

static int p=10;  // Nr of elements to sum within a loop

#define NRRUNS 5  // Nr of times to repeat the computation to avoid startup effects

void checkCudaError(cudaError_t status, char *error)
{
   if (status != cudaSuccess)
   {
      fprintf (stderr, "\nCuda : %s\n",error);
      exit(EXIT_FAILURE);
   }
}

__global__ void computeInGlobalMemory(int *d_A, int p)
{
   int i = blockIdx.x * blockDim.x + threadIdx.x;
   int j;
   
   for(j=0; j< p; j++)
     d_A[i] += d_A[N-1-j];    // Let each thread multiply a couple of elements
}

__global__ void computeInSharedMemory(int *d_A, int p)
{
   __shared__ int sm_A[N];
   int i = blockIdx.x * blockDim.x + threadIdx.x;
   int j;
   
   sm_A[i] = d_A[i];         // Copy from global device memory to shared processor memory
   __syncthreads();          // Wait until they are all finished copying

   for(j=0; j< p; j++)
     sm_A[i] += sm_A[N-1-j]; // Let each thread multiply a couple of elements
   __syncthreads();          // Wait until they are all finished multiplying
   
   d_A[i] = sm_A[i];         // And from share memory back to global device memory
   __syncthreads();          // Wait until they are all finished multiplying
}

void timeGlobalMemory(int *d_A, int nrBlocks, int blockSize)
{
   struct timeval ti1,ti2;
   double runtime;
   
   // First compute in global device memory 
   cudaThreadSynchronize(); /* Make sure all threads finished before starting clock */
   gettimeofday(&ti1,NULL); /* read starttime in t1 */
   computeInGlobalMemory <<< nrBlocks, blockSize >>> (d_A, p);
   cudaThreadSynchronize(); /* Make sure all threads finished before stopping clock */
   gettimeofday(&ti2,NULL); /* read endtime in t2 */
   
   runtime = (ti2.tv_sec - ti1.tv_sec ) + 1e-6*(ti2.tv_usec - ti1.tv_usec);
   fprintf(stderr,"Run time computing in global device memory : %f microsecs.\n",
	   runtime * 1e6);
}

void timeSharedMemory(int *d_A, int nrBlocks, int blockSize)
{
   struct timeval ti1,ti2;
   double runtime;
   
   // Then compute in shared memory on a multiprocessor :
   cudaThreadSynchronize(); /* Make sure all threads finished before starting clock */
   gettimeofday(&ti1,NULL); /* read starttime in t1 */
   computeInSharedMemory <<< nrBlocks, blockSize >>> (d_A, p);
   cudaThreadSynchronize(); /* Make sure all threads finished before stopping clock */
   gettimeofday(&ti2,NULL); /* read endtime in t2 */
   
   runtime = (ti2.tv_sec - ti1.tv_sec ) + 1e-6*(ti2.tv_usec - ti1.tv_usec);
   fprintf(stderr,"Run time computing in shared processor memory : %f microsecs.\n",
	   runtime * 1e6);
}

int main(int argc, char** argv)
{    
   cudaError_t status;
   
   int *h_A = 0;
   int *d_A = 0;
   int x,y;
   int blockSize = 256;   // Max number of threads in one single block (device dependent)
   int nrBlocks = 1;      // Or else we won't have a copy of the full vector in shared memory

   if(argc > 1)
     p = atoi(argv[1]);
   
   /* Allocate host memory for a vector of size N */
   status=cudaMallocHost((void **) &h_A,sizeof(int)*N);
   checkCudaError(status,"Error cudaMalloc on host !");
   
   /* Fill the vector with some data */
   for(x=0;x<N;x++)
     h_A[x] = x;
   
   /* Allocate device memory */
   status=cudaMalloc((void **) &d_A, sizeof(int)*N);
   checkCudaError(status,"Device memory allocation error !");
   
   blockSize = blockSize > N ? N : blockSize;
   
   // Do the actual timings a couple of times to avoid startup effects :
   for(y=0; y < NRRUNS; y++)
   {
      status =cudaMemcpy(d_A, h_A, sizeof(int)*N, cudaMemcpyHostToDevice);
      checkCudaError(status,"Error copying to device for shared mem test !");
      fprintf(stderr,"\n");
      
      timeSharedMemory(d_A, nrBlocks, blockSize);
   // Restore the contents of the device memory for the second run :
      status =cudaMemcpy(d_A, h_A, sizeof(int)*N, cudaMemcpyHostToDevice);
      checkCudaError(status,"Error copying to device for global mem test !");
      
      timeGlobalMemory(d_A, nrBlocks, blockSize);
   }
   
   status =cudaMemcpy(h_A, d_A, sizeof(int)*N, cudaMemcpyDeviceToHost);
   checkCudaError(status,"Error copying from device !");
   
   /* Just check the result for a few elements : */
   fprintf(stderr,"\n");
   for(x=0;x<10;x++)
     fprintf(stderr,"%d %d\n",x,h_A[x]);

   /* Memory clean up */
   status = cudaFreeHost(h_A);
   status = cudaFree(d_A);
   checkCudaError(status,"Device memory free error !");
   
   return EXIT_SUCCESS;
}
