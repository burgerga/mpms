/* * 
 * Performs a test of the latencies when copying data from host to device.
 * 
 * This version uses standard "malloc"s on the host, which is often slower
 * than using so-called "page-locked" memory , alloated with cudaMallocHost.
 * 
 * Note: A comparable latency problem occurs when copying from global memory
 * on the GPU device to device memory inside a single multiprocessor !
 * 
 * Kees Lemmens, Sep 2011
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

/* Includes, cuda */
#include "cuda.h"

#define N 200000

void checkCudaError(cudaError_t status, char *error)
{
   if (status != cudaSuccess)
   {
      fprintf (stderr, "\nCuda : %s\n",error);
      exit(EXIT_FAILURE);
   }
}

int main(int argc, char** argv)
{    
   struct timeval ti1,ti2;
   float runtime;
   
   float *h_A = 0;
   float *d_A = 0;
   int step = 10;
   int x,y;
   cudaError_t status;

   if(argc > 1)
     step = atoi(argv[1]);

   /* Allocate host memory */
   h_A = (float *)malloc(sizeof(float)*N);
   
   /* Fill the vector with some data */
   for(x=0;x<N;x++)
     h_A[x] = x;
   
   /* Allocate device memory */
   status = cudaMalloc((void **) &d_A, sizeof(float)*N);
   checkCudaError(status,"Error cudaMalloc on device !");
   
   // First send and receive the complete vector to and from the GPU :
   gettimeofday(&ti1,NULL);/* read starttime in t1 */

   status =cudaMemcpy(d_A, h_A, sizeof(float)*N, cudaMemcpyHostToDevice);
   checkCudaError(status,"Error copying to device !");
   status =cudaMemcpy(h_A, d_A, sizeof(float)*N, cudaMemcpyDeviceToHost);
   checkCudaError(status,"Error copying from device !");
   
   gettimeofday(&ti2,NULL); /* read endtime in t2 */
   
   runtime = (ti2.tv_sec - ti1.tv_sec ) + 1e-6*(ti2.tv_usec - ti1.tv_usec);
   fprintf(stderr,"\nRun time transfer of complete vector (%d elements): %f millisecs.\n",
	   N, runtime * 1e3);
   
   // Now send and receive every 10th element to and from the GPU :
   gettimeofday(&ti1,NULL);/* read starttime in t1 */

   for(x=0;x<N;x+=step)
   {
      status = cudaMemcpy(&d_A[x], &h_A[x], sizeof(float), cudaMemcpyHostToDevice);
      checkCudaError(status,"Error copying single element to device !");
   }
   
   // Then receive every 10th element :
   for(y=0;y<N;y+=step)
   {
      status = cudaMemcpy(&h_A[y], &d_A[y], sizeof(float), cudaMemcpyDeviceToHost);
      checkCudaError(status,"Error copying single element from device !");
   }
   
   gettimeofday(&ti2,NULL); /* read endtime in t2 */
   
   runtime = (ti2.tv_sec - ti1.tv_sec ) + 1e-6*(ti2.tv_usec - ti1.tv_usec);
   fprintf(stderr,"\nRun time transfer of every %dth element : %f millisecs.\n",
	   step, runtime * 1e3);
   
   /* Just check the result : */
   for(x=N-20;x<N;x++)
     printf("%d %f\n",x,h_A[x]);

   /* Memory clean up */
   free(h_A);
   status = cudaFree(d_A);
   checkCudaError(status,"Free error for Device memory!");
   
   return EXIT_SUCCESS;
}
