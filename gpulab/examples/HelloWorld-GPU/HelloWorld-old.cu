// Very simple starting example for CUDA program

#include <stdio.h>
#include <string.h>
#include <cuda.h>

int main(void)
{
   char *text = "Hello World !";
   char *string_h;   // pointer to host   memory
   char *string_d;   // pointer to device memory
   int textlen = strlen(text) + 1;
   
   // allocate arrays on host and on device
   string_h = (char *)malloc(textlen);
   cudaMalloc((void **) &string_d, textlen);

   // send data from host to GPU device: text to string_d 
   cudaMemcpy(string_d, text, textlen, cudaMemcpyHostToDevice);
   // retrieve data from GPU device: string_d to string_h
   cudaMemcpy(string_h, string_d, textlen, cudaMemcpyDeviceToHost);

   printf("%s\n",string_h);
   
   // cleanup memory
   cudaFree(string_d);
   free(string_h);
}
