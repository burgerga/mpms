/* Simple example of how to read the device capabilities */
/* Kees Lemmens, Nov 2008 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Includes, cuda */
#include "cuda.h"

void checkCudaError(cudaError_t status, char *error)
{
   if (status != cudaSuccess)
   {
      fprintf (stderr, "Cuda : %s\n",error);
      exit(EXIT_FAILURE);
   }
}

/* Main */
int main(int argc, char** argv)
{    
   cudaDeviceProp prop;
   int t,devcount;
   cudaError_t  status;

   status = cudaGetDeviceCount(&devcount);
   checkCudaError(status, "cudaGetDeviceCount failed");
   
   for(t=0;t < devcount; t++)
   {
      fprintf(stderr,"\nDevice %d from %d :\n\n",t+1,devcount);
      
      status = cudaGetDeviceProperties(&prop, t);
      checkCudaError(status, "cudaGetDeviceProperties failed");
      
      fprintf(stderr,"Name               : %s \n",prop.name);
      fprintf(stderr,"totalGlobalMem     : %ld MB\n",(prop.totalGlobalMem)/(1024*1024));
      fprintf(stderr,"sharedMemPerBlock  : %ld Byte\n",prop.sharedMemPerBlock);
      fprintf(stderr,"regsPerBlock       : %d \n",prop.regsPerBlock);
      fprintf(stderr,"warpSize           : %d \n",prop.warpSize);
      fprintf(stderr,"memPitch           : %ld \n",prop.memPitch);
      fprintf(stderr,"maxThreadsPerBlock : %d \n",prop.maxThreadsPerBlock);
      fprintf(stderr,"maxThreadsDim      : %d,%d,%d \n",prop.maxThreadsDim[0],prop.maxThreadsDim[1],prop.maxThreadsDim[2]);
      fprintf(stderr,"maxGridSize        : %d,%d \n",prop.maxGridSize[0],prop.maxGridSize[1]);
      fprintf(stderr,"totalConstMem      : %ld Byte\n",prop.totalConstMem);
      fprintf(stderr,"major              : %d \n",prop.major);
      fprintf(stderr,"minor              : %d \n",prop.minor);
      fprintf(stderr,"clockRate          : %d \n",prop.clockRate);
      fprintf(stderr,"textureAlignment   : %ld \n",prop.textureAlignment);
   }
   
   return EXIT_SUCCESS;
}
