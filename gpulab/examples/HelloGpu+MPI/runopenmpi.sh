mpirun -v -hostfile hostfile \
	--prefix /opt64/openmpi \
	--mca plm_rsh_agent rsh \
	--mca btl_tcp_if_exclude lo,vmnet1 \
	-wd $PWD $*

#      --mca btl_tcp_if_include eth0 \
