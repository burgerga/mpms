// Simple example of how to mix MPI code and Cuda code using both C++ and C :
// Kees Lemmens, March 2011

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include <mpi.h>

#define DEBUGLEVEL 1

// Cuda nvcc uses currently the g++ compiler for host code so these routines shouldn't 
// be declared as extern "C" but as normal functions if we link with the g++ linker :
extern int hellogpu(int procid, int vmsize);
// extern "C" int hellogpu(int procid, int vmsize);

// However, the mpi code is ordinary C-code compiled with mpicc, so it should be declared as
//  extern "C" or else the g++ linker can't link it with the cpp code from the Cuda part.
extern "C" int hellompi(int procid, int vmsize);

extern void debug(const char *format,...);

void debug(const char *format,...)
{  
#if (DEBUGLEVEL > 0)
   va_list p;
   
   va_start(p,format);
   vprintf(format, p);
   va_end(p);
#else
   ; // do nothing
#endif
}

int main(int argc, char *argv[])
{
   int procid, vmsize;
   int exitstatus;
   
   MPI_Init(&argc, &argv);
   
   MPI_Comm_rank(MPI_COMM_WORLD, &procid);  // get our process nr
   MPI_Comm_size(MPI_COMM_WORLD, &vmsize);  // get number of nodes
   
   switch(procid)
   {
    case 0 : // Master
      debug("Starting master on %d ...\n",procid);
      exitstatus = hellompi(procid, vmsize);
      break;
    default: // Slave
      debug("Starting slave on  %d ...\n",procid);
      exitstatus = hellogpu(procid, vmsize);
      break;
   }
   
   MPI_Finalize();
   exit(exitstatus);
}
