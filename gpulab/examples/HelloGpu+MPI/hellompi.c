// Simple example of how to mix MPI code and Cuda code using both C++ and C :
// Kees Lemmens, March 2011

#include <stdio.h>

extern int hellompi(int procid, int vmsize);

int hellompi(int procid, int vmsize)
{
   printf("Hello World on master process id %d with %d slaves\n",
	  procid,vmsize-1);
   
   return 0;
}
