CUDA code by Gerhard Burger for his Master Thesis "Molecular Simulations using CUDA"

The main stuff:
- code: new cuda code, both nbody and cell list.
- cuda_helper: some general cuda functions, partly ripped from the cutil library in the samples

Please run `make` in the doxygen folder to build the documentation (both latex and html)

Extra stuff:
- cuda_docs: some information about cuda, including the programming guide
- thrust_docs: some information about thrust, including some examples. more extensive documentation online.
- gpulab: slides and code example from a CUDA course I followed in Delft

Old stuff:
- old_code: old code, only nbody and lj cpu. most code is in a single file, a bit messy
