#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include <sys/time.h>

//define functor for frand
struct random_float {
	__host__ float operator()(void){
	    return ((float)rand()/RAND_MAX);
	}
};

int main(int argc, char **argv)
{
	if(argc < 2) { return 1; }
	int n = atoi(argv[1]);

	// generate 17M random numbers on the host
	thrust::host_vector<float> h_vec(n);
	thrust::device_vector<float> d_vec(n);
	thrust::generate(h_vec.begin(), h_vec.end(), random_float());

	cudaEvent_t cstart, cstop;
	float cruntime = 0.0f;
	cudaEventCreate(&cstart);
	cudaEventCreate(&cstop);
	
	cudaEventRecord(cstart, 0);
	// transfer data to the device
	//thrust::device_vector<float> d_vec = h_vec;
	thrust::copy(h_vec.begin(), h_vec.end(), d_vec.begin());
	cudaEventRecord(cstop, 0);
	cudaEventSynchronize(cstop);
	cudaEventElapsedTime(&cruntime, cstart, cstop);
	std::cout << "Run time device copy:\t" << cruntime << " ms." << std::endl;

	

	cudaEventRecord(cstart, 0);
	// sort data on the device
	float sum = thrust::reduce(d_vec.begin(), d_vec.end());
	cudaEventRecord(cstop, 0);
	cudaEventSynchronize(cstop);
	cudaEventElapsedTime(&cruntime, cstart, cstop);
	std::cout << "Run time cuda add:\t" << cruntime << " ms." << std::endl;

	printf("sum = %f\n", sum);

	struct timespec start, stop;
	float runtime;	

	float fsum = 0.0f;
	clock_gettime(CLOCK_MONOTONIC, &start);
	for(int i = 0; i < h_vec.size(); i++) {
		fsum += h_vec[i];
	}
	clock_gettime(CLOCK_MONOTONIC, &stop);
	runtime = 1e9*(stop.tv_sec - start.tv_sec ) + (stop.tv_nsec - start.tv_nsec);
	std::cout << "Run time float add:\t" << runtime*1e-6 << " ms." << std::endl;
	printf("sum = %f\n", fsum);

	double dsum = 0;
	clock_gettime(CLOCK_MONOTONIC, &start);
	for(int i = 0; i < h_vec.size(); i++) {
		dsum += (double) h_vec[i];
	}
	clock_gettime(CLOCK_MONOTONIC, &stop);
	runtime = 1e9*(stop.tv_sec - start.tv_sec ) + (stop.tv_nsec - start.tv_nsec);
	std::cout << "Run time double add:\t" << runtime*1e-6 << " ms." << std::endl;
	printf("sum = %f\n", dsum);

	return 0;
}

