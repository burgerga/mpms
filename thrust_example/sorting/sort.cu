#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>

#include <sys/time.h>

//define functor for frand
struct random_float {
	__host__ float operator()(void){
	    return ((float)rand()/RAND_MAX);
	}
};

int main(int argc, char **argv)
{
	if(argc < 2) { return 1; }
	int n = atoi(argv[1]);

	struct timespec start, stop;
	float runtime;	

	// generate 17M random numbers on the host
	thrust::host_vector<float> h_vec(n);
	thrust::device_vector<float> d_vec(n);

	clock_gettime(CLOCK_MONOTONIC, &start);
	thrust::generate(h_vec.begin(), h_vec.end(), random_float());
	clock_gettime(CLOCK_MONOTONIC, &stop);
	runtime = 1e9*(stop.tv_sec - start.tv_sec ) + (stop.tv_nsec - start.tv_nsec);
	std::cout << "Run time random number generation:\t" << runtime*1e-6 << " ms." << std::endl;

	//printf("\n");
	//for(int i = 0; i < h_vec.size(); i++) {
		//printf("%f\n",h_vec[i]);
	//}

	cudaEvent_t cstart, cstop;
	float cruntime = 0.0f;
	cudaEventCreate(&cstart);
	cudaEventCreate(&cstop);
	
	cudaEventRecord(cstart, 0);
	// transfer data to the device
	//thrust::device_vector<float> d_vec = h_vec;
	thrust::copy(h_vec.begin(), h_vec.end(), d_vec.begin());
	cudaEventRecord(cstop, 0);
	cudaEventSynchronize(cstop);
	cudaEventElapsedTime(&cruntime, cstart, cstop);
	std::cout << "Run time device copy:\t" << cruntime << " ms." << std::endl;

	

	cudaEventRecord(cstart, 0);
	// sort data on the device
	thrust::sort(d_vec.begin(), d_vec.end());
	cudaEventRecord(cstop, 0);
	cudaEventSynchronize(cstop);
	cudaEventElapsedTime(&cruntime, cstart, cstop);
	std::cout << "Run time cuda sort:\t" << cruntime << " ms." << std::endl;
	
	clock_gettime(CLOCK_MONOTONIC, &start);
	std::sort(h_vec.begin(),h_vec.end());
	clock_gettime(CLOCK_MONOTONIC, &stop);
	runtime = 1e9*(stop.tv_sec - start.tv_sec ) + (stop.tv_nsec - start.tv_nsec);
	std::cout << "Run time cpu sort:\t" << runtime*1e-6 << " ms." << std::endl;

	//printf("\n");
	//for(int i = 0; i < h_vec.size(); i++) {
		//printf("%f\n",h_vec[i]);
	//}

	cudaEventRecord(cstart, 0);
	thrust::copy(d_vec.begin(), d_vec.end(), h_vec.begin());
	cudaEventRecord(cstop, 0);
	cudaEventSynchronize(cstop);
	cudaEventElapsedTime(&cruntime, cstart, cstop);
	std::cout << "Run time copy back:\t" << cruntime << " ms." << std::endl;

	//printf("\n");
	//for(int i = 0; i < h_vec.size(); i++) {
		//printf("%f\n",h_vec[i]);
	//}

	return 0;
}

